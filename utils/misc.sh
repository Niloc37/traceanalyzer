C_BLACK='\033[0;30m'
C_DGRAY='\033[1;30m'
C_LGRAY='\033[0;37m'
C_WHITE='\033[1;37m'

C_GREEN='\033[0;32m'
C_LGREEN='\033[1;32m'
C_BLUE='\033[0;34m'
C_LBLUE='\033[1;34m'
C_CYAN='\033[0;36m'
C_LCYAN='\033[1;36m'
C_ORANGE='\033[0;33m'
C_YELLOW='\033[1;33m'
C_RED='\033[0;31m'
C_LRED='\033[1;31m'
C_PURPLE='\033[0;35m'
C_LPURPLE='\033[1;35m'

C_NC='\033[0m'

C_BOLD='\033[1m'

export TA_OUTPUT="/dev/null"

function print_error {
  if [[ $TA_OUTPUT != "/dev/stdout" && $TA_OUTPUT != "/dev/null" ]]; then
    echo -e "  [ERROR] $@" &> $TA_OUTPUT
  fi
  echo -e "  [${C_LRED}${C_BOLD}ERROR${C_NC}] $@"
}

function print_warning {
  if [[ $TA_OUTPUT != "/dev/stdout" && $TA_OUTPUT != "/dev/null" ]]; then
    echo -e "[WARNING] $@" &> $TA_OUTPUT
  fi
  echo -e "[${C_YELLOW}${C_BOLD}WARNING${C_NC}] $@"
}

function print_info {
  if [[ $TA_OUTPUT != "/dev/stdout" && $TA_OUTPUT != "/dev/null" ]]; then
    echo -e "   [INFO] $@" &> $TA_OUTPUT
  fi
  echo -e "   [${C_LBLUE}INFO${C_NC}] ${C_BOLD}$@${C_NC}"
}

function print_ok {
  if [[ $TA_OUTPUT != "/dev/stdout" && $TA_OUTPUT != "/dev/null" ]]; then
    echo -e "     [OK] $@" &> $TA_OUTPUT
  fi
  echo -e "     [${C_LGREEN}OK${C_NC}] $@"
}

function print_simple {
  if [[ $TA_OUTPUT != "/dev/stdout" && $TA_OUTPUT != "/dev/null" ]]; then
    echo "          $@" &> $TA_OUTPUT
  fi
  echo "          $@"
}

function check_last_return {
  local success=$1
  local error=$2
  if [[ $? != 0 ]] ; then
    if [[ "$error" != "" ]]; then
      print_error "$error"
    fi
    error_occured=true
  elif [[ "$success" != "" ]] ; then
    print_ok "$success"
  fi
}

