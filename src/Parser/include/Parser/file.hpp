#pragma once

#include "Core/filesystem.hpp"
#include "Core/string.hpp"
#include "Core/uniquePointer.hpp"

namespace ta::parser {

  class File {
      struct Impl;
    public:
      using LineNumber = unsigned long int;

    public:
      File();
      virtual ~File();
      File(const core::Path&);
      File(File&&) = default;
      File(const File&) = delete;
      auto operator=(File&&) -> File& = default;
      auto operator=(const File&) -> File& = delete;

    public:
      virtual auto open(const core::Path& path) -> bool;
      virtual auto nextLine() -> bool;
      virtual auto close() -> void;

      [[nodiscard]] auto isOpen() const -> bool;
      [[nodiscard]] auto currentLine() const -> core::StringView;
      [[nodiscard]] auto currentLineNumber() const -> LineNumber;

      auto stealmemory() -> core::HeapBuffer<char>;

    private:
      core::unique_ptr<Impl> _impl;
  };

}
