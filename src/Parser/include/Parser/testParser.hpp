#pragma once

namespace ta::parser {

  //------------------------------------------------------------------------

  struct TestEvent {
    core::String container;
    core::String type;
    core::String occurence;

    auto timeStamp() const { return nlc::meta::seconds<double>(0.); };
  };

  inline auto operator<(const TestEvent& lhs, const TestEvent& rhs) { return lhs.type < rhs.type; }

  //------------------------------------------------------------------------

  class TestTrace final : public trace::TraceData<TestEvent> {
      friend class TestParser;

    public:
      TestTrace() : trace::TraceData<TestEvent>("Test") {}

    public:
      auto print(core::Output& output, trace::EventDataId id) const -> void override {
        const auto& event = operator[](id);
        output("{} [{}] ({})", event.type, event.occurence, event.container);
      }


    public:
      auto addContainer(TestEvent&& event, trace::TraceBuilder<TestTrace>&) -> void;
      auto removeContainer(TestEvent&& event) -> void;

    private:
  };

  //------------------------------------------------------------------------

  class TestParser final : public Parser {
    public:
      TestParser() = default;
      ~TestParser() = default;

    public:
      auto open(const core::Path& path,
                trace::TraceStorage& storage,
                core::StringView traceName) -> trace::TraceStructureId final;
      auto parseHeader(trace::TraceStorage& storage) -> void final;
      auto parseBody(trace::TraceStorage& storage) -> void final;
      auto close(trace::TraceStorage& storage) -> void final;

      static auto name() { return core::StringView("Test"); }
      static auto accept(const core::Path& path) {
        return path.extension() == "test_trace";
      }

    public:
      std::map<core::String, trace::SequenceId> _containers;
      std::map<core::String, int> _counters;
      core::unique_ptr<trace::TraceBuilder<TestTrace>> _builder;
      parser::File _file;
  };

  //------------------------------------------------------------------------

  auto TestParser::open(const core::Path& path,
            trace::TraceStorage& storage,
            core::StringView traceName) -> trace::TraceStructureId {
    _builder = core::make_unique<trace::TraceBuilder<TestTrace>>(storage, path, traceName);

    _file.open(path);
    const auto rootId = _builder->addContainer(TestEvent { "none", "container", "root"}, trace::SequenceId::invalid());
    _containers["root"] = rootId;

    return _builder->structureId();
  }

  auto TestParser::parseHeader(trace::TraceStorage&) -> void {
    static const auto so = core::SplitOptions { "", "% ", "" };
    auto rootId = _containers["root"];

    for (const auto& token : core::Split(_file.currentLine(), so)) {
      const auto containerId = _builder->addContainer(TestEvent { "root", "container", token }, rootId);
      _containers[token] = containerId;
    }
  }

  auto TestParser::parseBody(trace::TraceStorage&) -> void {
    static const auto so = core::SplitOptions { "", " ", "" };
    while (_file.nextLine()) {
      const auto tokens = vectorFromIterable(core::Split(_file.currentLine(), so));
      Assert(tokens.size() == 3, "{}", _file.currentLine());

      auto [it, inserted] = _counters.emplace(tokens[1], 0);
      _builder->addEvent(TestEvent { tokens[0], tokens[1], core::format("{}-{}",tokens[2], it->second++) }, _containers[tokens[0]]);
    }
  }

  auto TestParser::close(trace::TraceStorage& storage) -> void {
    storage.structure(_builder->structureId()).state = trace::TraceState::Ready;
    _file.close();
    _builder.reset();
  }

  //------------------------------------------------------------------------

}
