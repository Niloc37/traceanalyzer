#pragma once

#include "Core/algorithm.hpp"
#include "Core/string.hpp"
#include "Core/uniquePointer.hpp"
#include "Core/vector.hpp"
#include "nlc/meta/id.hpp"
#include "Trace/traceStorage.hpp"

namespace ta::parser {

  //-------------------------------------------------------------------------

  struct ParserId_desr final {
    using underlying_t = unsigned int;
    static constexpr underlying_t invalid_value = static_cast<underlying_t>(-1);
    static constexpr underlying_t default_value = invalid_value;
  };
  using ParserId = nlc::meta::id<ParserId_desr>;

  //-------------------------------------------------------------------------

  class Parser {
      friend class ParserBank;
    public:
      Parser() = default;
      virtual ~Parser() = default;
      Parser(const Parser&) = delete;
      Parser(Parser&&) = default;

    public:
      [[nodiscard]] auto name() const { return _name; }

    public:
      virtual auto open(const core::Path& path,
                        trace::TraceStorage& storage,
                        core::StringView traceName) -> trace::TraceStructureId = 0;
      virtual auto parseHeader(trace::TraceStorage& storage) -> void = 0;
      virtual auto parseBody(trace::TraceStorage& storage) -> void = 0;
      virtual auto close(trace::TraceStorage& storage) -> void = 0;

    private:
      core::String _name;
  };

  //-------------------------------------------------------------------------

  class ParserFactory {
    protected:
      ParserFactory() = default;

    public:
      virtual ~ParserFactory() = default;

    public:
      [[nodiscard]] virtual auto instantiate() const -> core::unique_ptr<Parser> = 0;
      [[nodiscard]] virtual auto accept(const core::Path& path) const -> bool = 0;
      [[nodiscard]] virtual auto name() const -> core::StringView = 0;
  };

  template <typename Parser_t>
  class ParserFactorySpecialisation : public ParserFactory {
    public:
      [[nodiscard]] auto instantiate() const -> core::unique_ptr<Parser> final { return core::make_unique<Parser_t>(); }
      [[nodiscard]] auto accept(const core::Path& path) const -> bool final { return Parser_t::accept(path); }
      [[nodiscard]] auto name() const -> core::StringView final { return Parser_t::name(); }
  };

  //-------------------------------------------------------------------------

  class ParserBank final {
    public:
      ParserBank() : _nextId(0) {}

    public:
      template <typename ParserType, typename...Args>
      auto addParser(Args&&...args) -> void {
        auto parser = core::make_unique<ParserFactorySpecialisation<ParserType>>(nlc::forward<Args>(args)...);
        Assert(core::findIf(_parserFactories, [name = parser->name()] (const auto& p) { return name == p->name(); }) == _parserFactories.end(), "Parser \"{}\" already exists", parser->name());
        _parserFactories.emplace_back(nlc::move(parser));
      }

    public:
      auto createFromPath(const core::Path& path, core::StringView name) -> Parser * {
        for (const auto& factory : _parserFactories)
          if (factory->accept(path)) {
            _parsers.emplace_back(factory->instantiate());
            _parsers.back()->_name = name;
            return _parsers.back().get();
          }
        return nullptr;
      }

      auto get(core::StringView name) -> Parser * {
        for (auto& parser : _parsers)
          if (parser->_name == name)
            return parser.get();
        return nullptr;
      }

      auto list() -> core::Vector<core::StringView> {
        auto res = core::Vector<core::StringView> {};
        for (auto& parser : _parsers)
          res.emplace_back(parser->name());
        return res;
      }

    public:
      core::Vector<core::unique_ptr<ParserFactory>> _parserFactories;
      core::Vector<core::unique_ptr<Parser>> _parsers;
      ParserId::underlying_t _nextId;
  };

  //-------------------------------------------------------------------------

}
