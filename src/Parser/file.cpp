#include "Parser/file.hpp"

#include "Core/buffer.hpp"

#include <cstdio>
#include <cstring>

namespace ta::parser {

  //-------------------------------------------------------

  struct File::Impl final {
    core::HeapBuffer<char> buf;
    core::StringView line;
    LineNumber lineNumber = 0;
  };

  //-------------------------------------------------------

  File::File() : _impl(core::make_unique<Impl>()) {}

  //-------------------------------------------------------

  File::~File() = default;

  //-------------------------------------------------------

  File::File(const core::Path& path) : File() {
    open(path);
  }

  //-------------------------------------------------------

  auto File::open(const core::Path& path) -> bool {
    auto f = fopen(path, "rb");
    if (f == nullptr) {
      LOG_DEBUG("Failed to open file {}", path);
      return false;
    }
    fseek(f, 0, SEEK_END);
    auto size = ftell(f);
    fseek(f, 0, SEEK_SET);
    core::memresize<core::DiscardValues>(_impl->buf,size + 1);
    if (size != static_cast<long>(fread(_impl->buf.ptr(), sizeof(char), size, f))) {
      LOG_DEBUG("Failed to reading file {}", path);
      return false;
    }
    fclose(f);
    _impl->buf[size] = 0;

    _impl->lineNumber = 1;

    auto endOfFirstLine = strchr(_impl->buf.ptr(), '\n');
    _impl->line = core::StringView(_impl->buf.ptr(),
                                   endOfFirstLine == nullptr ? size : endOfFirstLine - _impl->buf.ptr());

    return true;
  }

  //-------------------------------------------------------

  auto File::nextLine() -> bool {
    const auto nextLineBegining = _impl->line.ptr() + _impl->line.size() + 1;
    if (nextLineBegining - _impl->buf.ptr() >= _impl->buf.size() - 1)
      return false;

    const auto nextLineEnd = strchr(nextLineBegining, '\n');
    _impl->line = core::StringView(nextLineBegining,
                                   nextLineEnd == nullptr
                                   ? _impl->buf.ptr() + _impl->buf.size() - 1 - nextLineBegining
                                   : nextLineEnd - nextLineBegining);
    ++_impl->lineNumber;
    return true;
  }

  //-------------------------------------------------------

  auto File::close() -> void {
    _impl->buf.free();
    _impl->lineNumber = 0;
  }

  auto File::isOpen() const -> bool { return _impl->buf.size() > 0; }

  auto File::currentLine() const -> core::StringView { return _impl->line; }

  auto File::currentLineNumber() const -> LineNumber { return _impl->lineNumber; }

  auto File::stealmemory() -> core::HeapBuffer<char> {
    LOG_DEBUG("Steal file memory");
    return nlc::move(_impl->buf);
  }
}
