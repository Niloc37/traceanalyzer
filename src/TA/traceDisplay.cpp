#include "traceDisplay.hpp"

#include "Trace/traceStorage.hpp"

namespace ta {

  using namespace trace;

  // ----------------------------------------------------------------------------

  namespace {

    // ----------------------------------------------------------------------------

    inline auto graph(State state) -> core::StringView {
      switch (state) {
        case State::Active: return " │";
        case State::EndLine: return "╴";
        case State::First: return ""; //"─┬"
        case State::Forward: return "──";
        case State::Inactive: return "\033[2m ·\033[22m"; // " ·"
        case State::Last: return " ╰";
        case State::Son: return " ├";
        default: return "╳╳";
      }
    }

    // ----------------------------------------------------------------------------

    constexpr auto Header_Separator = "┃";

    // ----------------------------------------------------------------------------

  }

  // States ---------------------------------------------------------------------

  auto States::push() -> void {
    const auto s = _states.size();
    if (s > 0) {
      auto& lastState = _states[s - 1];
      if (lastState == State::Active)
        lastState = State::Son;
    }
    _states.emplace_back(State::First);
  }

  auto States::pop() -> void { _states.pop_back(); }

  auto States::notifyLast() -> void {
    const auto size = _states.size();
    if (size > 0) {
      Assert(size > 0, "{} > 0", size);
      _states[size - 1] = State::Last;
    }
  }

  // TracePrinter ---------------------------------------------------------------

  auto TracePrinter::listPrinters() const -> core::Vector<core::pair<core::StringView, bool>> {
    auto res = core::Vector<core::pair<core::StringView, bool>> {};
    for (const auto& printer : _printers)
      if (printer->isVisible())
        res.emplace_back(printer->name(), printer->isActive());
    return res;
  }

  auto TracePrinter::setPrinterActive(core::StringView name, bool active) -> bool {
    for (auto& printer : _printers) {
      if (name == printer->name()) {
        printer->setActive(active);
        return true;
      }
    }
    return false;
  }

  auto TracePrinter::togglePrinter(core::StringView name) -> bool {
    for (auto& printer : _printers) {
      if (name == printer->name()) {
        printer->toggleActive();
        return true;
      }
    }
    return false;
  }

  // DisplayContext -------------------------------------------------------------

  DisplayContext::DisplayContext(TracePrinter* printer,
                                 const TraceStorage& storage,
                                 TraceStructureId structureId,
                                 int maxDepth,
                                 core::Output& output)
      : _storage(storage)
      , _structureId(structureId)
      , _output(output)
      , _maxDepth(maxDepth)
      , _printer(printer) {
    _printer->init(_storage, _structureId);
    bool isFirst = true;
    for (const auto& p : _printer->_printers) {
      if (p->isActive() && p->isVisible()) {
        if (isFirst) isFirst = false;
        else output(Header_Separator);

        output("{}", p->name());
      }
    }
    output("\n");
  }

  auto DisplayContext::printHeader(const Path& path) -> void {
    Assert(path.size() == _states.size(), "{} == {}", path.size(), _states.size());
    const auto& structure = _storage.structure(_structureId);
    const auto isHighlighted = structure.metaData().get<IsHighlighted>(path.back().id);
    if (isHighlighted) _output("\033[7m");
    bool isFirst = true;
    for (auto& printer : _printer->_printers) {
      if (printer->isActive()) {
        if (isFirst) isFirst = false;
        else output(Header_Separator);
        printer->print(_storage, _structureId, path, _output);
      }
    }
    if (isHighlighted) _output("\033[27m");
    printGraph(path);
  }

  auto DisplayContext::printData(EventDataId id) const -> void {
    const auto dataId = _storage.structure(_structureId).dataId;
    _storage.data(dataId)->print(_output, id);
  }

  auto DisplayContext::printGraph([[maybe_unused]]const Path& path) -> void {
    Assert(path.size() == _states.size(), "{} == {}", path.size(), _states.size());
    for (auto& state : _states) {
      _output(graph(state));
      switch (state) {
        case State::First:
          state = State::Active;
          break;
        case State::Forward:
          state = State::Inactive;
          break;
        case State::Last:
          state = State::Inactive;
          break;
        case State::Son:
          state = State::Active;
          break;
        default:
          break;
      }
    }
    _output(graph(State::EndLine));
  }

  auto DisplayContext::remainingDepth([[maybe_unused]] const Path& path) {
    Assert(path.size() == _states.size(), "{} == {}", path.size(), _states.size());
    auto depth = 0;
    for (const auto& state : _states)
      if (state != State::Skip)
        ++depth;
    return _maxDepth - depth;
  }


  // ----------------------------------------------------------------------------

  namespace {
    /*
    constexpr auto Bookmark_Red = "";

    constexpr auto Color_Default = "\033[39m";
    constexpr auto Color_Black = "\033[30m";
    constexpr auto Color_Blue = "\033[34m";
    constexpr auto Color_Cyan = "\033[36m";
    constexpr auto Color_Gray = "\033[90m";
    constexpr auto Color_Green = "\033[32m";
    constexpr auto Color_Magenta = "\033[35m";
    constexpr auto Color_Red = "\033[31m";
    constexpr auto Color_White = "\033[97m";
    constexpr auto Color_Yellow = "\033[33m";
    constexpr auto Color_Light_Blue = "\033[94m";
    constexpr auto Color_Light_Cyan = "\033[96m";
    constexpr auto Color_Light_Gray = "\033[37m";
    constexpr auto Color_Light_Green = "\033[92m";
    constexpr auto Color_Light_Magenta = "\033[95m";
    constexpr auto Color_Light_Red = "\033[91m";
    constexpr auto Color_Light_Yellow = "\033[93m";
    constexpr auto Color_RESET = "\003[22m";
    */

    //-----------------------------------------------------------------------------

    auto printSequence(DisplayContext&, Path&, SequenceId, size_t) -> void;

    //-----------------------------------------------------------------------------

    auto printLoopSequence(const Sequence::ConstView& sequence, Path& path, DisplayContext& context) {
      Assert(Sequence::get_type(sequence.get<Sequence::Data>()) == Sequence::Type::Loop, "{}", Sequence::get_type(sequence.get<Sequence::Data>()));
      const auto& loop = std::get<Sequence::Loop>(sequence.get<Sequence::Data>());
      context.output("Loop {} times [{}] : \n", loop.count, loop.event);
      context.states().notifyLast();
      printSequence(context, path, loop.event, 0);
    }

    //-----------------------------------------------------------------------------

    auto printContainer(const Sequence::ConstView& sequence, Path& path, DisplayContext& context) {
      Assert(Sequence::get_type(sequence.get<Sequence::Data>()) == Sequence::Type::Container, "{}", Sequence::get_type(sequence.get<Sequence::Data>()));

      if (context.remainingDepth(path) == 0)
        return;

      context.output("Container ");
      const auto& container = std::get<Sequence::Container>(sequence.get<Sequence::Data>());
      context.printData(container.createEvent);

      context.output("\n");

      if (context.remainingDepth(path) == 1)
        return;

      const auto& eventSons = container.sons;
      const auto last = eventSons.end() - 1;
      const auto begin = &*eventSons.begin();
      for (const auto& sonId : eventSons) {
        if (&sonId == last)
          context.states().notifyLast();

        Assert(sonId.isValid(), "");
        printSequence(context, path, sonId, &sonId - begin);
      }
    }

    //-----------------------------------------------------------------------------

    auto printNode(const Sequence::ConstView& sequence, Path& path, DisplayContext& context) -> void {
      Assert(Sequence::get_type(sequence.get<Sequence::Data>()) == Sequence::Type::Node, "{}", Sequence::get_type(sequence.get<Sequence::Data>()));
      context.output("Node\n");
      if (context.remainingDepth(path) != 0) {
        const auto& eventSons = Sequence::get_sons(sequence.get<Sequence::Data>());
        const auto last = eventSons.end() - 1;
        const auto begin = &*eventSons.begin();
        for (const auto& sonId : eventSons) {
          if (&sonId == last)
            context.states().notifyLast();

          Assert(sonId.isValid(), "");
          printSequence(context, path, sonId, &sonId - begin);
        }
      }
    }

    //-----------------------------------------------------------------------------

    auto printGroup(const Sequence::ConstView& sequence, Path& path, DisplayContext& context) -> void {
      const auto& group = std::get<Sequence::Group>(sequence.get<Sequence::Data>());

      if (path.size() > 0) {
        const auto index = getOccurence(context.structure(), path);
        Assert(countAllOccurences(context.structure(), path.back().id) == group.sons.size(), "");
        context.printData(group.sons[index]);
        context.output("\n");
      }
      else {
        context.output("Group\n");
        if (context.remainingDepth(path) != 0) {
          for (const auto& sonId : group.sons) {
            context.printData(sonId);
            context.output("\n");
          }
        }
      }
    }

    //-----------------------------------------------------------------------------

    auto printSequence(DisplayContext& context, Path& path, SequenceId id, size_t index) -> void {
      const auto& sequence = context.getSequence(id);
      Assert(path.size() == context.states().size(), "{} == {}", path.size(), context.states().size());

      if (path.size() > 0 && context.structure().metaData().get<IsFolded>(path.back().id))
        return;

      path.push(id, index);
      context.states().push();
      context.printHeader(path);
      switch (Sequence::get_type(sequence.get<Sequence::Data>())) {
        case Sequence::Type::Loop:
          printLoopSequence(sequence, path, context);
          break;
        case Sequence::Type::Group:
          printGroup(sequence, path, context);
          break;
        case Sequence::Type::Container:
          printContainer(sequence, path, context);
          break;
        case Sequence::Type::Node:
          printNode(sequence, path, context);
          break;
        default:
          AssertNotReached("Unknown type {}", Sequence::get_type(sequence.get<Sequence::Data>()));
      }
      path.pop();
      context.states().pop();
    }

    //-----------------------------------------------------------------------------

  } // anonymous namespace

  //-----------------------------------------------------------------------------

  auto showSequence(DisplayContext& context, SequenceId id) -> void {
    const auto& sequence = context.getSequence(id);
    auto first = true;
    for (const auto& occurence : sequence.get<SequenceOccurences>()) {
      if (first) {
        context.output("occurences : {}[{}]", occurence.id, occurence.index);
        first = false;
      }
      else
        context.output(", {}[{}]", occurence.id, occurence.index);
    }
    if (!first)
      context.output("\n");

    auto path = Path {};
    printSequence(context, path, id, 0u);
  }

  //-----------------------------------------------------------------------------

}
