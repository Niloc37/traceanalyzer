#pragma once

#include "Core/tuple.hpp"
#include "Trace/path.hpp"
#include "Trace/traceStorage.hpp"

namespace ta {

  // State ----------------------------------------------------------------------

  enum class State { Active, EndLine, First, Forward, Inactive, Last, Son, Skip };

  // States ---------------------------------------------------------------------

  class States final {
    public:
      [[nodiscard]] decltype(auto) size() const { return _states.size(); }
      [[nodiscard]] decltype(auto) begin() { return _states.begin(); }
      [[nodiscard]] decltype(auto) end() { return _states.end(); }
      auto push() -> void;
      auto pop() -> void;
      auto notifyLast() -> void;

    private:
      core::Vector<State> _states;
  };

  // DisplayContext -------------------------------------------------------------

  class Printer {
    public:
      Printer() = default;
      virtual ~Printer() = default;

    public:
      virtual auto name() const -> core::StringView = 0;
      virtual auto init(const trace::TraceStorage&, trace::TraceStructureId) -> void = 0;
      virtual auto print(const trace::TraceStorage&, trace::TraceStructureId, const trace::Path&, core::Output&) -> void = 0;

    public:
      [[nodiscard]] auto isActive() const { return _isActive; }
      auto setActive(bool active) { _isActive = active; }
      auto toggleActive() { _isActive = !_isActive; }
      auto setVisible(bool v) { _isVisible = v; }
      [[nodiscard]] auto isVisible() const { return _isVisible; }

    private:
      bool _isVisible = true;
      bool _isActive = false;
  };

  // TracePrinter ---------------------------------------------------------------

  enum PrinterVisibility { Visible, Invisible };
  enum PrinterActive { Active, Inactive };

  class TracePrinter final {
      friend class DisplayContext;
    public:
      template <PrinterActive active, PrinterVisibility visible, typename T, typename...Args>
      auto addPrinter(Args&&...args) {
        _printers.emplace_back(core::make_unique<T>(nlc::forward<Args>(args)...));
        _printers.back()->setVisible(visible == Visible);
        _printers.back()->setActive(active == Active);
      }
      auto init(const trace::TraceStorage& storage, trace::TraceStructureId id) -> void {
        for (auto& printer : _printers)
          printer->init(storage, id);
      }

    public:
      auto begin() { return _printers.begin(); }
      auto end() { return _printers.end(); }
      auto begin() const { return _printers.begin(); }
      auto end() const { return _printers.end(); }

      auto listPrinters() const -> core::Vector<core::pair<core::StringView, bool>>;
      auto setPrinterActive(core::StringView name, bool active) -> bool;
      auto togglePrinter(core::StringView name) -> bool;

    private:
      core::Vector<core::unique_ptr<Printer>> _printers;
  };

  class DisplayContext final {
    public:
      DisplayContext(TracePrinter * tracePrinter,
                     const trace::TraceStorage& storage,
                     trace::TraceStructureId structureId,
                     int maxDepth,
                     core::Output& output);

    public:
      DisplayContext() = delete;
      DisplayContext(DisplayContext&&) = delete;
      DisplayContext(const DisplayContext&) = delete;
      auto operator=(DisplayContext&&) -> DisplayContext& = delete;
      auto operator=(const DisplayContext&) -> DisplayContext& = delete;

    public:
      auto printHeader(const trace::Path& path) -> void;
      auto printData(trace::EventDataId id) const -> void;

      template <typename...T> auto output(T&& ...args) -> void { _output(nlc::forward<T>(args)...); }

      auto structure() const -> const trace::TraceStructure& { return _storage.structure(_structureId); }
      auto getSequence(trace::SequenceId id) const { return structure().view(id); }
      auto remainingDepth([[maybe_unused]] const trace::Path& path);
      decltype(auto) states() const { return (_states); }
      decltype(auto) states() { return (_states); }

    private:
      auto printGraph([[maybe_unused]]const trace::Path& path) -> void;

    private:
      const trace::TraceStorage& _storage;
      const trace::TraceStructureId _structureId;
      core::Output& _output;
      const int _maxDepth;
      States _states;
      TracePrinter * _printer;
  };


  // Display functions ----------------------------------------------------------

  auto showSequence(DisplayContext& context, trace::SequenceId id) -> void;
  auto showOccurences(DisplayContext& context, trace::SequenceId id) -> void;

  // ----------------------------------------------------------------------------

}
