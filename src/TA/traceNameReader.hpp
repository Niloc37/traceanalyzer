#pragma once

#include "REPL/command.hpp"
#include "Trace/traceStorage.hpp"

namespace ta {

  struct TraceNameAndId {
    core::String name;
    trace::TraceStructureId id;
  };

  template <trace::TraceState State>
  class TraceNameReaderBase {
    public:
      using requirements = nlc::meta::list<trace::TraceStorage>;

    public:
      TraceNameReaderBase() = default; // implicitely delete copy and move constructors

    public:
      template <typename Context> auto read(core::StringView& str, repl::Data& data, Context& context) -> bool {
        if (str == "")
          return false;

        auto&& storage = repl::get<trace::TraceStorage>(context);
        const auto infos = storage.getInfos();

        const auto token = core::extractFront(str);
        _nameAndId.name = token;
        _nameAndId.id = storage.getId(token);

        if (str == "") { // currently writing
          for (const auto& dataInfos : infos.traces)
            for (const auto& structure : dataInfos.structures)
              if (core::isPrefix(token, structure.name) && (structure.state & State) != trace::TraceState::NOTHING) {
                data.completion.completions.push_back(structure.name);
                data.completion.len = token.len();
              }

          data.completion.len = token.len();
          const auto completionsCount = data.completion.completions.size();
          if (completionsCount > 0) {
            data.hint.str = data.completion.completions.front();
            data.hint.len = data.completion.len;
            data.hint.color = completionsCount == 1 ? repl::Color::GREEN : repl::Color::GRAY;
            return validate(_nameAndId, storage);
          }
        }

        if (!validate(_nameAndId, storage)) {
          const auto index = token.begin() - data.context.begin();
          data.colors.emplace_back(repl::Data::ColorSpan { index, token.len(), repl::Color::RED });
          return false;
        }
        return true;
      }

      auto get() const -> const TraceNameAndId& { return _nameAndId; }

    protected:
      virtual auto validate(const TraceNameAndId&, const trace::TraceStorage&) -> bool = 0;

    private:
      TraceNameAndId _nameAndId;
  };

  class NewTraceNameReader final : public TraceNameReaderBase<trace::TraceState::All> {
      auto validate(const TraceNameAndId& nameAndId, const trace::TraceStorage&) -> bool final {
        return nameAndId.name != "" && nameAndId.id.isInvalid();
      }
  };

  template <trace::TraceState State>
  class ExistingTraceNameReader final : public TraceNameReaderBase<State> {
      auto validate(const TraceNameAndId& nameAndId, const trace::TraceStorage& storage) -> bool final {
        if (nameAndId.id.isValid()) {
          const auto& structure = storage.structure(nameAndId.id);
          return (structure.state & State) != trace::TraceState::NOTHING;
        }
        return false;
      }
  };

  template <trace::TraceState State>
  class ExistingTraceNameReaderWithFormat final : public TraceNameReaderBase<State> {
    public:
      auto setFormat(core::StringView format) { _format = format; }

    private:
      auto validate(const TraceNameAndId& nameAndId, const trace::TraceStorage& storage) -> bool final {
        if (!nameAndId.id.isValid()) return false;
        auto&& structureInfo = storage.getInfos(nameAndId.id);
        if ((structureInfo.state & State) == trace::TraceState::NOTHING)
          return false;
        auto&& dataInfo = storage.getInfos(structureInfo.dataId);
        return dataInfo.format == _format;
      }

    private:
      core::String _format;
  };

  template <trace::TraceState State>
  class TraceOrSequenceReader {
    public:
      using requirements = nlc::meta::list<trace::TraceStorage>;

    public:
      struct Ids {
        trace::TraceStructureId traceId;
        trace::SequenceId sequenceId;
      };

    public:
      template <typename Context> auto read(core::StringView& str, repl::Data& data, Context& context) -> bool {
        if (str == "")
          return false;

        _ids.traceId = trace::TraceStructureId::invalid();
        _ids.sequenceId = trace::SequenceId::invalid();

        static const auto options = core::SplitOptions { ".", "", "" };
        const auto& storage = repl::get<trace::TraceStorage>(context);
        const auto token = core::extractFront(str);
        const auto s = vectorFromIterable(core::Split(token, options));

        auto res = true;

        if (s.size() >= 1) {
          // Trace Id
          _ids.traceId = storage.getId(s[0]);
          if (_ids.traceId.isInvalid()) {
            const auto index = s[0].begin() - data.context.begin();
            data.colors.push_back(repl::Data::ColorSpan { index, s[0].len(), repl::Color::RED });
            res = false;
          }
          else {
            const auto& structure = storage.structure(_ids.traceId);
            if ((structure.state & State) == trace::TraceState::NOTHING) {
              const auto index = s[0].begin() - data.context.begin();
              data.colors.push_back(repl::Data::ColorSpan { index, s[0].len(), repl::Color::RED });
              res = false;
            }

            // Dot separator
            if ((s.size() == 2 && str != "") ||  // not currentlyWritting
                (s.size() >= 2 && s[1] != ".")) { // unexpected token
              const auto index = s[1].begin() - data.context.begin();
              data.colors.push_back(repl::Data::ColorSpan { index, s[1].len(), repl::Color::RED });
              res = false;
            }

            // Sequence Id
            if (s.size() >= 3 && _ids.traceId.isValid()) {
              const auto isNumber = [&]() {
                for (const auto c : s[2])
                  if (c < '0' || c > '9')
                    return false;
                return true;
              }();

              if (isNumber)
                _ids.sequenceId = trace::SequenceId(core::toInt(s[2]));

              if (!isNumber || _ids.sequenceId.value() >= structure.size()) {
                const auto index = s[2].begin() - data.context.begin();
                data.colors.push_back(repl::Data::ColorSpan { index, s[2].len(), repl::Color::RED });
                res = false;
              }
            }

            // Too much tokens
            if (s.size() > 3) {
              const auto index = s[2].begin() - data.context.begin();
              const auto size = core::StringView(data.context.end() - s[2].begin()).len();
              data.colors.push_back(repl::Data::ColorSpan { index, size, repl::Color::RED });
              res = false;
            }
          }
        }
        else {
          data.hint.str = "  traceName[.sequence] where \"sequence\" can be an id or a name.";
          data.hint.len = 0;
          data.hint.color = repl::Color::GRAY;
          res = false;
        }

        return res;
      }

      auto get() const { return _ids; }

    private:
      Ids _ids;
  };

  template <trace::TraceState State>
  class SequenceReader : public TraceOrSequenceReader<State> {
      using parent_t = TraceOrSequenceReader<State>;
    public:
      template <typename Context> auto read(core::StringView& str, repl::Data& data, Context& context) -> bool {
        if (parent_t::read(str, data, context))
          return parent_t::get().sequenceId.isValid();
        return false;
      }
  };
}
