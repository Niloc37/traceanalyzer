#pragma once

#include "traceDisplay.hpp"

#include "Trace/traceStorage.hpp"


namespace ta {

  // IdPrinter ------------------------------------------------

  class IdPrinter final : public Printer {
    public:
      auto name() const -> core::StringView final { return "id"; }
      auto init(const trace::TraceStorage&, trace::TraceStructureId) -> void final;
      auto print(const trace::TraceStorage&, trace::TraceStructureId, const trace::Path&, core::Output& output) -> void final;

    private:
      core::size_t _size;
  };

  // OccurencePrinter -----------------------------------------

  class OccurencePrinter final : public Printer {
    public:
      auto name() const -> core::StringView final { return "index"; }
      auto init(const trace::TraceStorage&, trace::TraceStructureId) -> void final;
      auto print(const trace::TraceStorage&, trace::TraceStructureId, const trace::Path&, core::Output&) -> void final;

    private:
      core::size_t _size;
  };

  // CountPrinter ---------------------------------------------
/*
  class CountPrinter final : public Printer {
    public:
      auto name() const -> core::StringView final { return "count"; }
      auto init(const trace::TraceStorage&, trace::TraceStructureId) -> void final;
      auto print(const trace::TraceStorage&, trace::TraceStructureId, const trace::Path&, core::Output&) -> void final;
    private:
      trace::TraceStructureId _structureId;
      core::String _format;
  };
*/
  // TimeStampPrinter -----------------------------------------

  class TimeStampPrinter final : public Printer {
    public:
      auto name() const -> core::StringView final { return "timestamp"; }
      auto init(const trace::TraceStorage&, trace::TraceStructureId) -> void final {}
      auto print(const trace::TraceStorage&, trace::TraceStructureId, const trace::Path&, core::Output&) -> void final;
  };

  // DurationPrinter ------------------------------------------

  class DurationPrinter final : public Printer {
    public:
      auto name() const -> core::StringView final { return "duration"; }
      auto init(const trace::TraceStorage&, trace::TraceStructureId) -> void final {}
      auto print(const trace::TraceStorage&, trace::TraceStructureId, const trace::Path&, core::Output&) -> void final;
  };

  // ContentionPrinter ----------------------------------------

  class ContentionPrinter final : public Printer {
    public:
      auto name() const -> core::StringView final { return "contention"; }
      auto init(const trace::TraceStorage&, trace::TraceStructureId) -> void final {}
      auto print(const trace::TraceStorage&, trace::TraceStructureId, const trace::Path&, core::Output&) -> void final;
  };

  // FoldingPrinter ----------------------------------------

  class FoldingPrinter final : public Printer {
    public:
      auto name() const -> core::StringView final { return "folding"; }
      auto init(const trace::TraceStorage&, trace::TraceStructureId) -> void final {}
      auto print(const trace::TraceStorage&, trace::TraceStructureId, const trace::Path&, core::Output&) -> void final;
  };

}
