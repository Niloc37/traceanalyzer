#include "traceDisplay.hpp"

#include "traceNameReader.hpp"

#include "Paje/pajeDefinitions.hpp"
#include "Paje/pajeTrace.hpp"
#include "REPL/command.hpp"

#include <set>

namespace ta {

  //-----------------------------------------------------------------------------

  using namespace repl;

  //-----------------------------------------------------------------------------

  class PrinterNameReader {
    public:
      using requirements = nlc::meta::list<TracePrinter>;

    public:
      template <typename Context_t>
      auto read(core::StringView& str, Data& data, Context_t& context) -> bool {
        if (str == "")
          return false;

        _str = core::extractFront(str);

        auto isOk = false;
        const auto& printers = repl::get<TracePrinter>(context);
        for (const auto& printer : printers.listPrinters()) {
          if (core::isPrefix(_str, printer.first))
            data.completion.completions.push_back(printer.first);
          if (_str == printer.first)
            isOk = true;
        }

        data.completion.len = _str.len();

        if (data.completion.completions.size() == 1) {
          data.hint.str = data.completion.completions.front();
          data.hint.len = data.completion.len;
          data.hint.color = Color::GREEN;
        }

        return isOk;
      }

      auto get() { return _str; }

    private:
      core::String _str;
  };

  //-----------------------------------------------------------------------------


  class ShowStructureInfo final : public repl::Command<ExistingTraceNameReader<trace::TraceState::All>> {
      using parent_t = repl::Command<ExistingTraceNameReader<trace::TraceState::All>>;
    public:
      ShowStructureInfo() : parent_t("info") {}

    private:
      auto exec(core::Output& output) -> void override {
        const auto& storage = repl::get<trace::TraceStorage>(context());
        const auto structureInfo = storage.getInfos(get<0>().id);
        const auto dataInfo = storage.getInfos(structureInfo.dataId);
        output("{}, {} sequences ({} bytes) [{}] - {} events ({} bytes)\n",
               structureInfo.name,
               structureInfo.nodeCount,
               structureInfo.memory,
               structureInfo.state,
               dataInfo.eventCount,
               dataInfo.memory);
      }

      auto description() const -> core::StringView final {
        return "Display informations about a trace";
      }
  };

  //-----------------------------------------------------------------------------

  class ShowPajeFields final : public repl::Command<ExistingTraceNameReaderWithFormat<trace::TraceState::All>> {
      using parent_t = repl::Command<ExistingTraceNameReaderWithFormat<trace::TraceState::All>>;

    public:
      ShowPajeFields() : parent_t("pajeFields") {
        reader<0>().setFormat("Paje");
      }

    private:
      auto exec(core::Output& output) -> void override {
        const auto& storage = repl::get<trace::TraceStorage>(context());
        const auto& structure = storage.structure(get<0>().id);
        const auto pData = storage.data(structure.dataId);
        const auto& data = *reinterpret_cast<const paje::Trace *>(pData);

        std::set<paje::FieldName> fieldNames;
        for (const auto& type : data.eventTypes()) {
          for (const auto field : type.second.fieldTypes) {
            fieldNames.emplace(field.name);
          }
        }

        output("Field names in trace {} are :\n", get<0>().name);
        for (const auto& name : fieldNames)
          output(" - \"{}\"\n", name);
      }

      auto description() const -> core::StringView final {
        return "List all the event fields names used in Paje trace";
      }
  };

  //-----------------------------------------------------------------------------

  class ShowCommandOptions final {
    public:
      using requirements = nlc::meta::list<>;

    public:
      struct Options {
        int recursiveDepth = -1;
        bool showContainers = false;
      };

    public:
      template <typename Context> auto read(core::StringView& str, repl::Data& data, Context&) -> bool {
        _options = Options {};
        auto res = true;

        while (true) {
          auto context = str;
          const auto token = core::extractFront(context);

          const auto end = token.end();
          auto it = token.begin();

          if (it == end || *it != '-')
            break;

          ++it;

          if (*it == 'r') {
            ++it;
            const auto count = core::StringView(it, token.end());
            const auto isInt = [&]() {
              if (count == "") return false;
              for (const auto c : count)
                if (c < '0' || c > '9')
                  return false;
              return true;
            }();

            if (_options.showContainers == false && isInt) {
              _options.recursiveDepth = core::toInt(count);
              if (_options.recursiveDepth <= 0) {
                data.colors.push_back(repl::Data::ColorSpan { token.begin() - data.context.begin(),
                                                              token.len(),
                                                              repl::Color::RED });
                res = false;
              }
            }
            else if (context != "") {
              data.colors.push_back(repl::Data::ColorSpan { token.begin() - data.context.begin(),
                                                            token.len(),
                                                            repl::Color::RED });
              res = false;
            }
          }
          else if (*it == 'c') {
            ++it;
            if (_options.recursiveDepth == -1 && it == token.end())
              _options.showContainers = true;
            else {
              data.colors.push_back(repl::Data::ColorSpan { token.begin() - data.context.begin(),
                                                            token.len(),
                                                            repl::Color::RED });
              res = false;
            }
          }
          else if (it != token.end()) {
            data.colors.push_back(repl::Data::ColorSpan { token.begin() - data.context.begin(),
                                                          token.len(),
                                                          repl::Color::RED });
            res = false;
          }

          str = context;
        }

        return res;
      }

      auto get() const -> const Options& { return _options; }

    private:
      Options _options;
  };

  class ShowCommand final : public repl::Command<ShowCommandOptions, TraceOrSequenceReader<trace::TraceState::Ready | trace::TraceState::Modified>, repl::Requirements<TracePrinter>> {
      using parent_t = repl::Command<ShowCommandOptions, TraceOrSequenceReader<trace::TraceState::Ready | trace::TraceState::Modified>, repl::Requirements<TracePrinter>>;
    public:
      ShowCommand() : parent_t("show") {}

    private:
      auto exec(core::Output& output) -> void override {
        const auto options = get<0>();
        const auto[traceId, sequenceId] = get<1>();
        Assert(traceId.isValid(), "");

        const auto& storage = repl::get<trace::TraceStorage>(context());
        auto& printer = repl::get<TracePrinter>(context());
        const auto& structure = storage.structure(traceId);

        if (options.showContainers) {
          auto context = DisplayContext(&printer, storage, traceId, options.recursiveDepth, output);
          auto path = trace::Path {};
          auto aux = [&](const auto id, auto f) -> void {
            path.push(id, 0); // index is useless here
            context.states().push();
            const auto& container = structure.get<trace::Sequence::Data>(id);
            Assert(trace::Sequence::get_type(container) == trace::Sequence::Type::Container, "");

            context.printHeader(path);
            context.printData(std::get<trace::Sequence::Container>(container).createEvent);
            output("\n");

            const auto& sons = trace::Sequence::get_sons(container);
            const auto last = sons.end() - 1;
            for (const auto& sonId : sons) {
              if (&sonId == last)
                context.states().notifyLast();

              const auto& seq = structure.get<trace::Sequence::Data>(sonId);
              if (trace::Sequence::get_type(seq) != trace::Sequence::Type::Container)
                break;

              f(sonId, f);
              path.pop();
              context.states().pop();
            }
          };

          aux(structure.rootId, aux);
        }
        else {
          auto context = DisplayContext(&printer,
                                        storage,
                                        traceId,
                                        options.recursiveDepth,
                                        output);
          showSequence(context, sequenceId.isValid() ? sequenceId : structure.rootId);
        }
      }

      auto description() const -> core::StringView final {
        return "Show a trace or a sequence, recursively or not";
      }
  };

  //-----------------------------------------------------------------------------

  template <typename Parameter>
  class ToggleSequenceMetaData final : public repl::Command<SequenceReader<trace::TraceState::ReadyOrModified>> {
      using parent_t = repl::Command<SequenceReader<trace::TraceState::ReadyOrModified>>;

    public:
      explicit ToggleSequenceMetaData(core::StringView name, core::StringView description)
          : parent_t(name)
          , _description(description) {
      }

    private:
      auto exec(core::Output&) -> void override {
        const auto structureId = get<0>().traceId;
        Assert(structureId.isValid(), "");
        auto& metaData = repl::get<trace::TraceStorage>(context()).structure(structureId).metaData();
        auto& hl = metaData.template get<Parameter>(get<0>().sequenceId);
        hl = !hl;
      }

      auto description() const -> core::StringView final {
        return _description;
      }

      core::String _description;
  };

  //-----------------------------------------------------------------------------

  template <typename Parameter, auto value>
  class ResetSequenceMetaData final : public repl::Command<ExistingTraceNameReader<trace::TraceState::ReadyOrModified>> {
      using parent_t = repl::Command<ExistingTraceNameReader<trace::TraceState::ReadyOrModified>>;

    public:
      explicit ResetSequenceMetaData(core::StringView name, core::StringView description)
          : parent_t(name)
          , _description(description) {
      }

    private:
      auto exec(core::Output&) -> void override {
        const auto structureId = get<0>().id;
        Assert(structureId.isValid(), "");
        auto& metaData = repl::get<trace::TraceStorage>(context()).structure(structureId).metaData();
        for (auto& parameter : metaData.template get<Parameter>())
          parameter = value;
      }

      auto description() const -> core::StringView final {
        return _description;
      }

      core::String _description;
  };

  //-----------------------------------------------------------------------------

  class ListTracesCommand final : public repl::Command<repl::Requirements<trace::TraceStorage>> {
      using parent_t = repl::Command<repl::Requirements<trace::TraceStorage>>;

    public:
      ListTracesCommand() : parent_t("list") {}

    private:
      auto exec(core::Output& output) -> void final {
        const auto& storage = repl::get<trace::TraceStorage>(context());
        const auto infos = storage.getInfos();
        for (const auto& data: infos.traces) {
          output(" - {} ({} kB) : {}\n", data.path, data.memory / 1000, data.format);
          for (const auto& structure : data.structures)
            output("    - {} ({} kB) [{}]\n", structure.name, structure.memory / 1000, structure.state);
        }
      }

      auto description() const -> core::StringView final {
        return "List all loaded traces";
      }
  };

  //-----------------------------------------------------------------------------

  class ListPrinters final : public repl::Command<repl::Requirements<TracePrinter>> {
      using parent_t = repl::Command<repl::Requirements<TracePrinter>>;

    public:
      ListPrinters() : parent_t("list_printers") {}

    private:
      auto exec(core::Output& output) -> void final {
        const auto& printer = repl::get<TracePrinter>(context());
        for (const auto& [name, enabled] : printer.listPrinters())
          output(" - \"{}\" [{}]\n", name, enabled ? "enabled" : "disabled");
      }

      auto description() const -> core::StringView final {
        return "Display a list of available informations displayable about traces";
      }
  };

  //-----------------------------------------------------------------------------

  class EnablePrinter final : public repl::Command<PrinterNameReader, repl::Requirements<TracePrinter>> {
      using parent_t = repl::Command<PrinterNameReader, repl::Requirements<TracePrinter>>;

    public:
      EnablePrinter() : parent_t("display") {}

    private:
      auto exec(core::Output& output) -> void final {
        auto& printer = repl::get<TracePrinter>(context());
        if (!printer.setPrinterActive(get<0>(), true))
          output("Printer \"{}\" not found", get<0>());
      }

      auto description() const -> core::StringView final {
        return "Enable a information printer";
      }
  };

  template <bool value>
  class SetActiveAllPrinters final : public repl::Command<repl::Requirements<TracePrinter>> {
      using parent_t = repl::Command<repl::Requirements<TracePrinter>>;

    public:
      SetActiveAllPrinters() : parent_t(value ? "display_all" : "hide_all") {}

    private:
      auto exec(core::Output&) -> void final {
        auto& tracePrinter = repl::get<TracePrinter>(context());
        for (auto& printer : tracePrinter)
          printer->setActive(value);
      }

      auto description() const -> core::StringView final {
        if constexpr (value)
          return "Enable all information printers";
        else
          return "Disable all information printers";
      }
  };

  //-----------------------------------------------------------------------------

  class DisablePrinter final : public repl::Command<PrinterNameReader, repl::Requirements<TracePrinter>> {
      using parent_t = repl::Command<PrinterNameReader, repl::Requirements<TracePrinter>>;

    public:
      DisablePrinter() : parent_t("hide") {}

    private:
      auto exec(core::Output& output) -> void final {
        auto& printer = repl::get<TracePrinter>(context());
        if (!printer.setPrinterActive(get<0>(), false))
          output("Printer \"{}\" not found", get<0>());
      }

      auto description() const -> core::StringView final {
        return "Disable a information printer";
      }
  };

  //-----------------------------------------------------------------------------

  // Register trace display commands ////////////////////////////////////////////

  auto registerTraceDisplayCommands(repl::Repl& repl) -> void {
    repl.add_command<ShowStructureInfo>();
    repl.add_command<ShowCommand>();
    repl.add_command<ShowPajeFields>();
    repl.add_command<ToggleSequenceMetaData<trace::IsHighlighted>>("hl", "Toggle highlight of a given sequence in a trace");
    repl.add_command<ResetSequenceMetaData<trace::IsHighlighted, false>>("nohl", "Disable highlighting for all sequences of a given trace");
    repl.add_command<ToggleSequenceMetaData<trace::IsFolded>>("fold", "Toggle foding of a fiven sequence in a trace");
    repl.add_command<ResetSequenceMetaData<trace::IsFolded, true>>("fold_all", "Fold all sequences in trace display");
    repl.add_command<ResetSequenceMetaData<trace::IsFolded, false>>("unfold_all", "Unfold all sequences in trace display");
    repl.add_command<ListTracesCommand>();
    repl.add_command<ListPrinters>();
    repl.add_command<EnablePrinter>();
    repl.add_command<SetActiveAllPrinters<true>>();
    repl.add_command<SetActiveAllPrinters<false>>();
    repl.add_command<DisablePrinter>();
  }
} // namespace ta
