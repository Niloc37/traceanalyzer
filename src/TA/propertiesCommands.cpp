#include "Core/properties.hpp"
#include "REPL/basicReaders.hpp"
#include "REPL/command.hpp"
#include "REPL/repl.hpp"

namespace ta {

  using namespace repl;

  //-----------------------------------------------------------------

  class PropertyNameReader {
    public:
      using requirements = nlc::meta::list<core::PropertyBank>;

    public:
      template <typename Context_t>
      auto read(core::StringView& str, Data& data, Context_t& context) -> bool {
        if (str == "")
          return false;

        _str = core::extractFront(str);

        const auto& propertyBank = repl::get<core::PropertyBank>(context);
        for (const auto& property : propertyBank.list()) {
          if (core::isPrefix(_str, property.name))
            data.completion.completions.push_back(property.name);
        }

        data.completion.len = _str.len();

        if (data.completion.completions.size() == 1) {
          data.hint.str = data.completion.completions.front();
          data.hint.len = data.completion.len;
          data.hint.color = Color::GREEN;
        }

        return _str.size() > 0;
      }

      auto get() { return _str; }

    private:
      core::String _str;
  };

  //-----------------------------------------------------------------

  class ListPropertiesCommand final : public Command<Requirements<core::PropertyBank>> {
      using parent_t = Command<Requirements<core::PropertyBank>>;
    public:
      ListPropertiesCommand() : parent_t("listp") {}

    private:
      auto exec(core::Output& output) -> void final {
        const auto& properties = repl::get<core::PropertyBank>(context());
        for (const auto& p : properties.list())
          output(" - {} : \"{}\" [\"{}\"]\n", p.name, p.customValue, p.defaultValue);
      }

      auto description() const -> core::StringView final {
        return "List all properties set with there default values";
      }
  };

  //-----------------------------------------------------------------

  class GetPropertyCommand final : public Command<PropertyNameReader> {
      using parent_t = Command<PropertyNameReader>;
    public:
      GetPropertyCommand() : parent_t("getp") {}

    private:
      auto exec(core::Output& output) -> void final {
        const auto& properties = repl::get<core::PropertyBank>(context());
        output("{}\n", properties.get(get<0>()));
      }

      auto description() const -> core::StringView final {
        return "Get the value of a property";
      }
  };

  //-----------------------------------------------------------------

  class SetPropertyCommand final : public Command<PropertyNameReader, StringReader> {
      using parent_t = Command<PropertyNameReader, StringReader>;
    public:
      SetPropertyCommand() : parent_t("setp") {}

    private:
      auto exec(core::Output&) -> void final {
        auto& properties = repl::get<core::PropertyBank>(context());
        properties.set(get<0>(), get<1>());
      }

      auto description() const -> core::StringView final {
        return "Set a value to a property";
      }
  };

  //-----------------------------------------------------------------

  auto registerPropertiesCommands(Repl& repl) -> void {
    repl.add_command<ListPropertiesCommand>();
    repl.add_command<GetPropertyCommand>();
    repl.add_command<SetPropertyCommand>();
  }

  //-----------------------------------------------------------------

}

