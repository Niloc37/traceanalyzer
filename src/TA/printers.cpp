#include "printers.hpp"

namespace ta {

  // IdPrinter ------------------------------------------------

  auto IdPrinter::init(const trace::TraceStorage& storage, trace::TraceStructureId id) -> void {
    _size = static_cast<int>(log10f(storage.structure(id).size())) + 1;
  }

  auto IdPrinter::print(const trace::TraceStorage&,
                        trace::TraceStructureId,
                        const trace::Path& path,
                        core::Output& output) -> void {
    output("{:>{}}", path.back().id, _size);
  }

  // OccurencePrinter -----------------------------------------

  auto OccurencePrinter::init(const trace::TraceStorage& storage, trace::TraceStructureId id) -> void {
    auto maxSize = 0u;
    for (const auto& sequence : storage.structure(id).get<trace::Sequence::Data>())
      if (trace::Sequence::get_type(sequence) == trace::Sequence::Type::Group)
        if (auto size = std::get<trace::Sequence::Group>(sequence).sons.size(); size > maxSize)
          maxSize = size;


    _size = static_cast<int>(log10f(maxSize)) + 1;
  }

  auto OccurencePrinter::print(const trace::TraceStorage& storage,
                               trace::TraceStructureId structureId,
                               const trace::Path& path,
                               core::Output& output) -> void {
    const auto parentCount = getOccurence(storage.structure(structureId), path);
    output("{:>{}}", parentCount, _size);
  }

  // CountPrinter ---------------------------------------------
/*
  auto CountPrinter::init(const trace::TraceStorage& sotrage, trace::TraceStructureId id) -> void {
    auto maxSize = 0u;
    for (const auto& sequence : storage.structure(id))
      if (sequence.type() == trace::Sequence::Type::Group)
        if (auto size = trace::as<trace::Sequence::Group>(sequence).sons.size(); size > maxSize)
          maxSize = size;

    _format = core::format("{{:>{}}}", static_cast<int>(log10f(maxSize)) + 1);
    _structureId = id;
  }

  auto CountPrinter::print(const trace::TraceStorage& storage, const trace::Path& path, core::Output& output) -> void {
    const auto parentCount = countOccurence(storage.structure(_structureId), path);
    output(_format, parentCount);
  }
*/
  // TimeStampPrinter -----------------------------------------

  auto TimeStampPrinter::print(const trace::TraceStorage& storage,
                               trace::TraceStructureId structureId,
                               const trace::Path& path,
                               core::Output& output) -> void {
    const auto& structure = storage.structure(structureId);
    const auto data = storage.data(structure.dataId);
    const auto type = trace::Sequence::get_type(structure.get<trace::Sequence::Data>(path.back().id));
    if (type == trace::Sequence::Type::Container || type == trace::Sequence::Type::Group) {
      output("{:>9}", data->timeStamp(firstEvent(structure, path)));
    }
    else
      output("         ");
  }

  // DurationPrinter ------------------------------------------

  auto DurationPrinter::print(const trace::TraceStorage& storage,
                              trace::TraceStructureId structureId,
                              const trace::Path& path,
                              core::Output& output) -> void {
    const auto& structure = storage.structure(structureId);
    if (trace::Sequence::get_type(structure.get<trace::Sequence::Data>(path.back().id)) == trace::Sequence::Type::Group)
      output("        ");
    else {
      const auto& data = storage.data(structure.dataId);
      output("{:>8}", data->timeStamp(lastEvent(structure, path)) - data->timeStamp(firstEvent(structure, path)));
    }
  }

  // ContentionPrinter ----------------------------------------

  auto ContentionPrinter::print(const trace::TraceStorage& storage,
                                trace::TraceStructureId structureId,
                                const trace::Path& path_,
                                core::Output& output) -> void {
    const auto& structure = storage.structure(structureId);
    const auto& data = storage.data(structure.dataId);

    const auto type = trace::Sequence::get_type(structure.get<trace::Sequence::Data>(path_.back().id));
    if (type == trace::Sequence::Type::Node || type == trace::Sequence::Type::Loop) {
      auto durations = core::Vector<nlc::meta::seconds<double>> {};

      auto path = path_.copy();

      auto aux = [&](auto&& f, auto index) -> void {
        auto& step = path[index];

        const auto& sequence = structure.get<trace::Sequence::Data>(step.id);

        if (index == path.size() - 1u) {
          durations.push_back(data->timeStamp(lastEvent(structure, path)) -
                              data->timeStamp(firstEvent(structure, path)));
        }
        else {
          if (trace::Sequence::get_type(sequence) == trace::Sequence::Type::Loop) {
            Assert(step.index == trace::SequenceOccurenceIndex(0), "");

            const auto& loop = std::get<trace::Sequence::Loop>(sequence);
            for (const auto sonIndex : core::Range(0u, loop.count)) {
              step.index = trace::SequenceOccurenceIndex(sonIndex);
              f(f, index + 1u);
            }

            step.index = trace::SequenceOccurenceIndex(0);
          }
          else {
            f(f, index + 1u);
          }
        }
      };

      aux(aux, 0u);

      auto min = durations.front();
      auto sum = nlc::meta::seconds<double> { 0. };

      for (const auto& duration : durations) {
        sum += duration;
        if (min > duration)
          min = duration;
      }

      const auto threadDuration = [&structure, &data] (auto path) {
        while (true) {
          Assert(path.size() > 0, "");
          const auto type = trace::Sequence::get_type(structure.get<trace::Sequence::Data>(path.back().id));
          if (type == trace::Sequence::Type::Container)
            return data->timeStamp(lastEvent(structure, path)) -
                   data->timeStamp(firstEvent(structure, path));
          path.pop();
        }
      }(path.copy());

      const auto res = (sum - min * nlc::meta::unit<double>(durations.size())) / threadDuration;
      Assert(res.v >= 0.f && res.v <= 1.f, "0.f <= {} <= 1.f", res);
      output("{: >10.4g}", static_cast<float>(res.v) * 100.f);
    }
    else
      output("          ");
  }

  // FoldingPrinter ------------------------------------------

  auto FoldingPrinter::print(const trace::TraceStorage& storage,
                              trace::TraceStructureId structureId,
                              const trace::Path& path,
                              core::Output& output) -> void {
    const auto& structure = storage.structure(structureId);
    const auto id = path.back().id;
    if (trace::Sequence::get_type(structure.get<trace::Sequence::Data>(id)) != trace::Sequence::Type::Group) {
      if (structure.metaData().get<trace::IsFolded>(id))
        output("▶ ");
      else
        output("▽ ");
    }
    else
      output("  ");
  }

  // ----------------------------------------------------------

}
