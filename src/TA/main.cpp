#include "Core/debug.hpp"
#include "Paje/pajeParser.hpp"
#include "Parser/parser.hpp"
#include "Parser/testParser.hpp"
#include "ProgramOptions.hxx"
#include "REPL/repl.hpp"
#include "traceDisplay.hpp"
#include "Trace/traceStorage.hpp"
#include "printers.hpp"

using namespace ta;

namespace ta {
  auto registerTraceManipulationCommands(repl::Repl& repl) -> void;
  auto registerTraceDisplayCommands(repl::Repl& repl) -> void;
  auto registerPropertiesCommands(repl::Repl& repl) -> void;
}

// Tests ///////////////////////////////////////////////////////////////
#include <iostream>
#include <stdexcept>
#include <cstdio>
#include <string>
#include "Parser/parser.hpp"
#include "Paje/pajeParser.hpp"
#include "Core/properties.hpp"
#include "Core/structOfArray.hpp"

std::string exec(const char* cmd) {
    char buffer[128];
    std::string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (fgets(buffer, sizeof buffer, pipe) != NULL) {
            result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

auto test() -> void {
  //std::cout << "res : " << exec("pwd") << std::endl << std::endl << exec("gnuplot 'plotTest.in'") << std::endl;
}

// MAIN ////////////////////////////////////////////////////////////////

int main(int argc, char **argv) {
  po::parser programOptions;

  programOptions["verbose"].abbreviation('v').description("Display debug informations").type(po::string)
#ifdef TA_DEBUG
                           .fallback("debug");
#else
                           .fallback("critical");
#endif

  programOptions["help"].abbreviation('?').description("print this help screen").callback([&programOptions] {
    std::cout << programOptions << std::endl;
  });

  programOptions["file"].abbreviation('f').type(po::string).multi(false).description(
      "read commands to execute from a file");
  programOptions["output"].abbreviation('o').type(po::string).multi(false).description(
      "output to a file instead of the standard output");

  if (!programOptions(argc, argv)) {
    LOG_WARNING("Failed to parse command line");
    //abort();
  }

  if (programOptions["help"].size() > 0)
    return 0;

  core::set_verbose_level(programOptions["verbose"].get().string);

  test();

  auto storage = trace::TraceStorage {};

  auto properties = core::PropertyBank {};
  properties.setDefaultAndReset("auto_factorize", "true");
  properties.setDefaultAndReset("auto_save_on_load", "true");

  auto parsers = parser::ParserBank {};
  parsers.addParser<paje::Parser>();
  parsers.addParser<parser::TestParser>();

  auto tracePrinter = TracePrinter {};
  tracePrinter.addPrinter<Active,   Visible, IdPrinter>();
  tracePrinter.addPrinter<Inactive, Visible, OccurencePrinter>();
  tracePrinter.addPrinter<Inactive, Visible, ContentionPrinter>();
  tracePrinter.addPrinter<Inactive, Visible, DurationPrinter>();
  tracePrinter.addPrinter<Inactive, Visible, TimeStampPrinter>();
  tracePrinter.addPrinter<Active, Invisible, FoldingPrinter>(); // Must be the last

  auto repl = repl::Repl { &storage, &properties, &parsers, &tracePrinter };
  auto output = core::unique_ptr<core::Output>();

  registerTraceManipulationCommands(repl);
  registerTraceDisplayCommands(repl);
  registerPropertiesCommands(repl);

  if (programOptions["output"].size() > 0)
    output = core::make_unique<core::FileOutput>(programOptions["output"].get().string.c_str());
  else
    output = repl.output();

  if (programOptions["file"].size() > 0) {
    repl.run(programOptions["file"].get().string.c_str(), *output.get());
  }
  else {
    repl.run(*output.get());
  }
}

