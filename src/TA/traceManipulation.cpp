#include "Core/properties.hpp"
#include "Paje/pajeTrace.hpp"
#include "Parser/parser.hpp"
#include "REPL/command.hpp"
#include "REPL/pathReader.hpp"
#include "Trace/trahay.hpp"

#include "traceNameReader.hpp"

namespace {

  using namespace ta;
  using namespace ta::repl;
  using namespace ta::trace;
  using namespace ta::parser;
  using namespace ta::core;

  auto factorize(TraceStorage& storage, TraceStructureId id, Output& output) -> void {
    const auto t1 = std::chrono::high_resolution_clock::now();

    const auto& name = storage.getInfos(id).name;
    output("Factorizing trace \"{}\"...", name);
    auto& structure = storage.structure(id);
    trahayTreeFactorisation(structure);
    structure.state = TraceState::Modified;

    const auto t2 = std::chrono::high_resolution_clock::now();
    const auto duration = std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1,1>>>(t2 - t1);
    output("Done ({}s)\n)", duration.count());
  }

  class OpenTrace final : public Command<TracePathReader, NewTraceNameReader, Requirements<ParserBank>> {
      using parent_t = Command<TracePathReader, NewTraceNameReader, Requirements<ParserBank>>;

    public:
      OpenTrace() : parent_t("open") {}

    private:
      auto exec(Output& output) -> void final {
        auto& parserBank = repl::get<ParserBank>(context());
        auto& storage = repl::get<TraceStorage>(context());

        // get path of the trace to load
        const auto path = get<0>();
        if (!isFile(path)) {
          output("Failed to open file \"{}\".", path);
          return;
        }

        //get name to assign to the trace
        const auto traceName = get<1>().name;

        auto parser = parserBank.createFromPath(path, traceName);
        if (parser == nullptr) {
          output("Failed to find a parser for file extension \"{}\"", path.extension());
          return;
        }

        const auto traceId = parser->open(path, storage, traceName);
        parser->parseHeader(storage);

        output("Opened trace \"{}\" in \"{}\" ({})\n", path, traceName, traceId);
      }

      auto description() const -> StringView final {
        return "Open a trace and read its header";
      }
  };

  // Load (load) traces ////////////////////////////////////////////////////////////

  class LoadTrace final : public Command<ExistingTraceNameReader<TraceState::NotLoaded>, Requirements<ParserBank, PropertyBank>> {
      using parent_t = Command<ExistingTraceNameReader<TraceState::NotLoaded>, Requirements<ParserBank, PropertyBank>>;

    public:
      LoadTrace() : parent_t("load") {}

    private:
      auto exec(Output& output) -> void final {
        const auto traceStructureId = get<0>().id;
        const auto traceStructureName = get<0>().name;

        auto& storage = repl::get<TraceStorage>(context());
        auto& properties = repl::get<PropertyBank>(context());
        const auto parser = repl::get<ParserBank>(context()).get(traceStructureName);

        const auto& traceStructure = storage.structure(traceStructureId);
        const auto traceDataId = traceStructure.dataId;
        const auto traceData = storage.data(traceDataId);

        const auto t1 = std::chrono::high_resolution_clock::now();
        // parse the trace
        Assert(parser != nullptr, "");
        output("Loading \"{}\"... ", traceData->path());
        parser->parseBody(storage);
        parser->close(storage);

        const auto t2 = std::chrono::high_resolution_clock::now();
        const auto duration = std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1,1>>>(t2 - t1);

        output("Done ({}s): \"{}\" ({})\n", duration.count(), traceStructureName, traceStructureId);

        if (properties.get("auto_save_on_load") == "true")
          storage.duplicate(traceStructureId, traceStructureName + "_save");

        if (properties.get("auto_factorize") == "true")
          factorize(storage, traceStructureId, output);
      }

      auto description() const -> StringView final {
        return "Load the content and a trace";
      }
  };

  // Duplicate (copy) traces ///////////////////////////////////////////////////////

  class DuplicateTraceCommand final : public Command<ExistingTraceNameReader<TraceState::Ready | TraceState::Modified>, NewTraceNameReader> {
      using parent_t = Command<ExistingTraceNameReader<TraceState::Ready | TraceState::Modified>, NewTraceNameReader>;
    public:
      DuplicateTraceCommand() : parent_t("copy") {}

    private:
      auto exec(Output& output) -> void final {
        auto& storage = repl::get<TraceStorage>(context());
        const auto id = storage.duplicate(get<0>().id, get<1>().name);
        output("{} was duplicated under name {} with id {}\n", storage.getInfos(get<0>().id).name, get<1>().name, id);
      }

      auto description() const -> StringView final {
        return "Duplicate the structure of a trace (doesn't duplicate the data)";
      }
  };

  // Factorize (factorize) traces //////////////////////////////////////////////////

  class TrahayFactorisationCommand final : public Command<ExistingTraceNameReader<TraceState::Ready>> {
      using parent_t = Command<ExistingTraceNameReader<TraceState::Ready>>;
    public:
      TrahayFactorisationCommand() : parent_t("factorize") {}

    private:
      auto exec(Output& output) -> void final {
        auto& storage = repl::get<TraceStorage>(context());
        const auto id = get<0>().id;
        factorize(storage, id, output);
      }

      auto description() const -> StringView final {
        return "Factorize a trace";
      }
  };

}

namespace ta {
  void registerTraceManipulationCommands(Repl& repl) {
    repl.add_command<OpenTrace>();
    repl.add_command<LoadTrace>();
    repl.add_command<DuplicateTraceCommand>();
    repl.add_command<TrahayFactorisationCommand>();
  }
}
