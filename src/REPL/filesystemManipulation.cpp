#include "REPL/repl.hpp"

#include "Core/filesystem.hpp"
#include "REPL/command.hpp"
#include "REPL/pathReader.hpp"

namespace ta::repl {

  class LSCommand final : public Command<DirectoryPathReader> {
    public:
      LSCommand() : Command<DirectoryPathReader>("ls") {}

    private:
      auto exec(core::Output& output) -> void override {
        auto entries = core::listEntries(get<0>());
        //std::sort(entries.begin(), entries.end());
        for (const auto& entry : entries)
          output("{}\n", entry);
      }

      auto description() const -> core::StringView final {
        return "List entries in current directory";
      }
  };

  class CDCommand final : public Command<DirectoryPathReader> {
    public:
      CDCommand() : Command<DirectoryPathReader>("cd") {}

    private:
      auto exec(core::Output&) -> void override {
        core::setCurrentDirectory(get<0>());
      }

      auto description() const -> core::StringView final {
        return "Set the current directory";
      }
  };

  class PWDCommand final : public Command<> {
    public:
      PWDCommand() : Command<>("pwd") {}

    private:
      auto exec(core::Output& output) -> void override {
        output("{}\n", core::currentDirectory());
      }

      auto description() const -> core::StringView final {
        return "Display the current directory";
      }
  };

  auto registerFileSystemManipulationCommand(Repl& repl) -> void {
    repl.add_command<PWDCommand>();
    repl.add_command<LSCommand>();
    repl.add_command<CDCommand>();
  }
}
