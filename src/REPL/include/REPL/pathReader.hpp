#pragma once

#include "Core/filesystem.hpp"
#include "Core/string.hpp"
#include "Core/vector.hpp"
#include "Core/tuple.hpp"
#include "REPL/repl.hpp"

namespace ta::repl {

  struct PathCompletions final {
    core::Vector <core::String> completions;
    int len = 0;
  };

  class PathReader {
    public:
      using requirements = nlc::meta::list<>;

    public:
      template <typename Context_t>
      auto read(core::StringView& str, Data& data, const Context_t&) -> bool {
        if (str == "")
          return validatePath("");

        const auto rawPath = core::extractFront(str);
        _path = rawPath;

        if (str == "") { // currently writing
          const auto [directory, basename] = [&] () {
            const auto path = core::Path(rawPath);
            if (rawPath == "")
              return core::pair<core::Path, core::String> { core::Path("./"), "" };
            if (core::backChar(rawPath) == '/')
              return core::pair<core::Path, core::String> { path, "" };
            return core::pair<core::Path, core::String> { path.parent(), path.basename().str() };
          }();

          if (directory.isValid() && core::isDirectory(directory)) {
            for (const auto& son : core::listEntries(directory)) {
              const auto path = directory/son;
              if (son.str() != "." && son.str() != ".." &&
                  core::isPrefix(basename, son.str()) &&
                  (validatePath(path) || isDirectory(path)))
                data.completion.completions.emplace_back(son.str());
            }
            data.completion.len = basename.len();

            if (data.completion.completions.size() == 0 && !validatePath(rawPath)) {
              const auto index = rawPath.begin() - data.context.begin();
              data.colors.push_back(Data::ColorSpan { index, rawPath.size(), Color::RED });
            }

            if (data.completion.completions.size() > 0) {
              data.hint.str = data.completion.completions.front();
              data.hint.len = data.completion.len;
            }

            if (data.completion.completions.size() == 1)
              data.hint.color = Color::GREEN;
            else
              data.hint.color = Color::GRAY;
          }
          else
            LOG_DEBUG("{} not a directory", directory);
        }

        if (validatePath(rawPath) || data.completion.completions.size() > 0)
          return true;

        const auto index = rawPath.begin() - data.context.begin();
        if (index < data.context.size())
          data.colors.push_back(Data::ColorSpan { index, rawPath.len(), Color::RED });
        return false;
      }

      auto get() const { return core::Path(_path); }

    protected:
      virtual auto validateForCompletion(core::StringView) const -> bool { return true; }
      virtual auto validatePath(core::StringView str) const -> bool { return core::Path(str).isValid(); }

    private:
      core::String _path;
  };

  class DirectoryPathReader final : public PathReader {
      auto validateForCompletion(core::StringView path) const -> bool final { return core::isDirectory(core::Path(path)); }
      auto validatePath(core::StringView path) const -> bool final { return core::isDirectory(core::Path(path)); }
  };

  class FilePathReader final : public PathReader {
      auto validateForCompletion(core::StringView path) const -> bool final { return core::isFile(core::Path(path)); }
      auto validatePath(core::StringView path) const -> bool final { return core::isFile(core::Path(path)); }
  };

  class NewPathReader final : public PathReader {
      auto validateForCompletion(core::StringView path) const -> bool final { return core::isFile(core::Path(path)); }
      auto validatePath(core::StringView path) const -> bool final { return core::Path(path).isValid(); }
  };

  class TracePathReader final : public PathReader {
      auto validateForCompletion(core::StringView path) const -> bool final {
        return core::isDirectory(core::Path(path)) || validatePath(path);
      }
      auto validatePath(core::StringView str) const -> bool final {
        const auto path = core::Path(str);
        if (path.isInvalid()) return false;
        const auto ext = path.extension();
        return ext == "trace" || ext == "test_trace";
      }
  };

}

