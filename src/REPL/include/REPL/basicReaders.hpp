#pragma once

#include "repl.hpp"

namespace ta::repl {

  class StringReader {
    public:
      using requirements = nlc::meta::list<>;

    public:
      template <typename Context_t>
      auto read(core::StringView& str, Data& data, const Context_t&) -> bool {
        if (str == "")
          return false;

        _str = core::extractFront(str);

        if (_str == "") {
          if (str == "") {
            data.hint.str = "  [string]";
            data.hint.color = Color::GRAY;
            data.hint.len = 0;
          }
          return false;
        }
        return true;
      }

      auto get() { return _str; }

    private:
      core::String _str;
  };

  class UIntReader {
    public:
      using requirements = nlc::meta::list<>;

    public:
      template <typename Context_t>
      auto read(core::StringView& str, Data& data, Context_t&) -> bool {
        if (str == "")
          return false;

        const auto token = core::extractFront(str);

        if (token == "") {
          data.hint.str = "  [unsigned int]";
          data.hint.color = Color::GRAY;
          data.hint.len = 0;
          return false;
        }

        const auto isValid = [&] () {
          for (const auto c : str)
            if (c < '0' || c > '9')
              return false;
          return true;
        }();

        if (!isValid) {
          const auto index = token.begin() - data.context.begin();
          data.colors.emplace_back(Data::ColorSpan { index, token.len(), Color::RED });
          return false;
        }
        return true;
      }

      auto get() const { return _value; }

    private:
      unsigned int _value;
  };
}
