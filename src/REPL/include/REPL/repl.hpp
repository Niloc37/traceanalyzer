#pragma once

#include "Core/algorithm.hpp"
#include "Core/debug.hpp"
#include "Core/filesystem.hpp"
#include "Core/output.hpp"
#include "Core/range.hpp"
#include "Core/string.hpp"
#include "Core/types.hpp"
#include "Core/uniquePointer.hpp"
#include "nlc/meta/type_value.hpp"

#include <typeinfo>

namespace ta::repl {

  // Token ///////////////////////////////////////////////////////////////////

  struct Token final {
    size_t index;
    size_t size;
  };

  inline auto operator==(const Token& lhs, const Token& rhs) -> bool {
    return lhs.index == rhs.index && lhs.size == rhs.size;
  }

  using TokenList = core::Vector<Token>;

  [[nodiscard]] auto tokenise(core::StringView) -> TokenList;

  // Input ///////////////////////////////////////////////////////////////////

  struct Input final {
    core::String str;
    TokenList tokens;
    auto operator[](size_t index) const -> core::StringView {
      Assert(index < tokens.size(), "{} < {}", index, tokens.size());
      auto token = tokens[index];
      return core::StringView { str.ptr() + token.index, token.size };
    }
    auto currentToken() const -> size_t {
      const auto tokenCount = tokens.size();
      if (tokenCount == 0u)
        return 0u;
      const auto& token = tokens.back();
      if (token.index + token.size < str.size())
        return tokenCount;
      return tokenCount - 1u;
    }
  };

  inline auto operator==(const Input& lhs, const Input& rhs) -> bool {
    return lhs.str == rhs.str;
  }

  // Command dependencies //////////////////////////////////////////////

  class Dependencies final {
    public:
      template <typename T> auto add(T * ptr) -> void {
        const auto hash = typeid(T).hash_code();
        Assert(core::findIf(_ptrs, [hash](auto& p) { return p.first == hash; }) == _ptrs.end(), "");
        _ptrs.emplace_back(hash, ptr);
      }

      template <typename T> auto get() -> T* {
        const auto hash = typeid(T).hash_code();
        for (auto& ptr : _ptrs)
          if (ptr.first == hash)
            return reinterpret_cast<T*>(ptr.second);
        AssertNotReached();
        return nullptr;
      }


    private:
      core::Vector<std::pair<core::size_t, void*>> _ptrs;
  };

  // CommandBase //////////////////////////////////////////////////////////////

  enum class Color {
    BLACK         = 0,
    RED           = 1,
    GREEN         = 2,
    BROWN         = 3,
    BLUE          = 4,
    MAGENTA       = 5,
    CYAN          = 6,
    LIGHTGRAY     = 7,
    GRAY          = 8,
    BRIGHTRED     = 9,
    BRIGHTGREEN   = 10,
    YELLOW        = 11,
    BRIGHTBLUE    = 12,
    BRIGHTMAGENTA = 13,
    BRIGHTCYAN    = 14,
    WHITE         = 15,
    NORMAL        = LIGHTGRAY,
    DEFAULT       = -1,
    ERROR         = -2
  };

  struct Data {
    core::String context;

    struct Completion {
      core::Vector<core::String> completions;
      int len = 0;
    };
    Completion completion;

    struct Hint {
      core::String str;
      Color color;
      int len = 0;
    };
    Hint hint;

    struct ColorSpan {
      size_t index;
      size_t len;

      Color color;
    };
    core::Vector<ColorSpan> colors;

    Data(core::StringView str) : context(str) {}
  };

  class CommandBase {
      friend class Repl;

    public:
      CommandBase(core::StringView name) : _data(""), _name(name) {}
      virtual ~CommandBase() = default;

    public:
      auto name() const { return _name.view(); }
      virtual auto description() const -> core::StringView = 0;

    protected:
      virtual auto exec(core::Output& output) -> void = 0;
      virtual auto update(core::StringView, Data&) -> bool = 0;
      virtual auto setDependencies(Dependencies&) -> void {}

    private:
      auto data() const -> const Data& { return _data; }
      auto isValid() const { return _isValid; }
      auto updateIFN(core::StringView str, bool force = false) {
        if (force || str != _data.context) {
          _data = Data(str);
          auto input = _data.context.view();
          core::extractFront(input);
          _isValid = update(input, _data);
        }
      }

    private:
      Data _data;
      core::String _name;
      bool _isValid;
  };

  // Repl //////////////////////////////////////////////////////////////

  class Repl final {
      struct Impl;

    private:
      Repl();

    public:
      template <typename...T>
      Repl(T*...deps) : Repl() { (_deps.add<T>(deps), ...); }
      ~Repl();
      Repl(Repl&&) = delete;
      Repl(const Repl&) = delete;
      auto operator=(Repl&&) = delete;
      auto operator=(const Repl&) = delete;

    public:
      auto run(core::Output&) -> void;
      auto run(const core::Path&, core::Output&) -> void;
      auto exit() -> void;
      auto output() -> core::unique_ptr<core::Output>;

      template <typename T, typename...Args> auto add_command(Args&&...args) {
        auto command = core::make_unique<T>(nlc::forward<Args>(args)...);
        command->setDependencies(_deps);
        register_command(nlc::move(command));
      }

    public:
      auto show_history(core::Output&) -> void;
      auto show_commands(core::Output&) -> void;

    private:
      template <typename T> auto hook_complete(const std::string& context, int& contextLen, T&) -> void;
      template <typename T> auto hook_color(const std::string& context, T& colors) -> void;
      template <typename T> auto hook_hint(const std::string& context, int& contextLen, Color& color, T&) -> void;
      auto register_command(core::unique_ptr<CommandBase> command) -> void;

      auto selectCommand(core::StringView) -> CommandBase *;
      auto runCommand(core::StringView, core::Output&) -> void;

    private:
      core::unique_ptr<Impl> _impl;
      Dependencies _deps;
  };
} // namespace ta::repl
