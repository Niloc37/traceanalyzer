#pragma once

#include "Core/span.hpp"
#include "Core/tuple.hpp"
#include "nlc/meta/basics.hpp"
#include "nlc/meta/list.hpp"
#include "REPL/basicReaders.hpp"
#include "REPL/repl.hpp"

namespace ta::repl {

  //---------------------------------------------------------

  template <typename...T> struct Requirements final {
    using requirements = nlc::meta::list<T...>;
  };

  //---------------------------------------------------------

  template <typename...T> class Context_t final {
    public:
      static constexpr auto size = sizeof...(T);
      using types = nlc::meta::list<T...>;
      using data_t = core::tuple<T *...>;

    public:
      template <typename Q, typename...Ts> friend auto get(Context_t<Ts...>&) -> Q&;

      auto init(Dependencies& deps) {
        nlc::meta::for_each<nlc::meta::range<0, size>>([&] (auto i) {
          auto ptr = deps.get<nlc::meta::get<i, types>>();
          Assert(ptr != nullptr, "");
          core::get<i>(_data) = ptr;
        });
      }

    private:
      data_t _data;
  };

  template <typename T, typename...Ts> auto get(Context_t<Ts...>& context) -> T& {
    static constexpr auto index = nlc::meta::index_of<T, typename Context_t<Ts...>::types>;
    return *core::get<index>(context._data);
  }

  //---------------------------------------------------------

  namespace impl {
    template <typename...Ts> using requirementsList = nlc::meta::remove_doublons<
        nlc::meta::concat<typename Ts::requirements...>>;

    template <typename T> struct isOnlyRequirement { static constexpr auto value = false; };
    template <typename...Ts> struct isOnlyRequirement<Requirements<Ts...>> { static constexpr auto value = true; };
    template <typename T> using isReader = nlc::meta::Value<!isOnlyRequirement<T>::value>;

    template <typename L> using readersList = nlc::meta::select<isReader, L>;
    template <typename L> using toContext = nlc::meta::apply<Context_t, L>;
    template <typename L> using toTuple = nlc::meta::apply<core::tuple, L>;
  }

  //---------------------------------------------------------

  template <typename...Arguments> class Command : public CommandBase {
      friend Repl;

    protected:
      using arguments_t = nlc::meta::list<Arguments...>;
      using requirements_t = nlc::meta::apply<impl::requirementsList, arguments_t>;
      using readers_t = impl::readersList<arguments_t>;

      using Context = impl::toContext<requirements_t>;
      using Readers = impl::toTuple<readers_t>;

      Command(core::StringView name) : CommandBase { name } {}

    protected:
      auto update(core::StringView str, Data& data) -> bool override {
        return _update<0>(str, data);
      }

      template <size_t index> decltype(auto) get() {
        static_assert(index < readers_t::size);
        return core::get<index>(_readers).get();
      }

      template <size_t index> decltype(auto) reader() {
        static_assert(index < readers_t::size);
        return core::get<index>(_readers);
      }

      auto context() -> Context& { return _context; }

    private:
      auto setDependencies(Dependencies& deps) -> void override { _context.init(deps); }

      template <int Index> auto _update(core::StringView str, Data& data) -> bool {
      if constexpr (Index < readers_t::size) {
          const auto readerRes = core::get<Index>(_readers).read(str, data, _context);
          const auto nextReaderRes = _update<Index + 1>(str, data);
          return readerRes && nextReaderRes;
        }
        else {
          if (str != "") {
            const auto token = core::extractFront(str);
            if (token != "") {
              data.hint.str = "  [Too much arguments]";
              data.hint.len = 0;
              data.hint.color = Color::RED;
              const auto index = token.begin() - data.context.begin();
              const auto size = data.context.end() - token.begin();
              data.colors.push_back(Data::ColorSpan { index, size, Color::RED });
              return false;
            }
          }
          return true;
        }
      }

    private:
      Context _context;
      Readers _readers;
  };
}
