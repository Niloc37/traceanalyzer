#include "REPL/repl.hpp"
#include "REPL/command.hpp"

#include <fstream>
#include "replxx.hxx"

namespace ta::repl {
  using Replxx = replxx::Replxx;

  extern auto registerFileSystemManipulationCommand(Repl& repl) -> void;

  // Repl /////////////////////////////////////////////////////////////////////////

  struct Repl::Impl {
    static constexpr auto history_path = "replHistory";
    Replxx rx;
    core::Vector<core::unique_ptr<CommandBase>> commands;
    bool exit = false;
  };

  // Output ///////////////////////////////////////////////////////////////////

  class Output final : public core::Output {
    public:
      explicit Output(Replxx& rx) : _rx { rx } {}

      auto operator()(core::StringView s) -> void override {
        _rx.print(core::CString(s));
        // core::output(s);
      }

    private:
      Replxx& _rx;
  };

  //--------------------------------------------------------------------

  auto Repl::output() -> core::unique_ptr<core::Output> {
    return core::make_unique<Output>(_impl->rx);
  }

  // Command registering //////////////////////////////////////////////////////////

  auto registerCommands(Repl& repl) -> void {
    registerFileSystemManipulationCommand(repl);
  }

  //--------------------------------------------------------------------

  auto Repl::register_command(core::unique_ptr<CommandBase> command) -> void {
    _impl->commands.emplace_back(nlc::move(command));
  }

  // Repl /////////////////////////////////////////////////////////////////////////

  template <typename T>
  auto getCommandCompletion(core::StringView input,
                            const core::Vector<core::unique_ptr<CommandBase>>& commands,
                            T& output,
                            int& len) {
    const auto name = core::getFront(input);
    if (name.end() == input.end()) {
      for (const auto& command : commands)
        if (core::isPrefix(name, command->name())) {
          output.emplace_back(core::CString(command->name() + " "));
        }
      len = name.size();
    }
  }

  //--------------------------------------------------------------------

  template <typename T> auto Repl::hook_complete(const std::string& input, int& len, T& res) -> void {
    static const auto options = core::SplitOptions { "", ";", "\"\"\'\'" };
    const auto s = vectorFromIterable(core::Split(input, options));

    if (s.size() > 0) {
      const auto context = s.back();

      if (const auto command = selectCommand(context); command != nullptr) {
        command->updateIFN(context);
        const auto& completions = command->data().completion;

        len = completions.len;
        for (const auto& completion : completions.completions)
          res.emplace_back(core::CString(completion));
      }
      else {
        getCommandCompletion(context, _impl->commands, res, len);
      }
    }
  }

  //--------------------------------------------------------------------

  template <typename T> auto Repl::hook_color(const std::string& input, T& colors) -> void {
    static const auto options = core::SplitOptions { "", ";", "\"\"\'\'" };

    for (const auto context : core::Split(input, options)) {
      const auto offset = context.begin() - core::StringView(input).begin();

      if (const auto command = selectCommand(context); command != nullptr) {
        command->updateIFN(context);
        for (const auto span : command->data().colors) {
          for (const auto i : core::Range(span.index, span.index + span.len))
            colors[i + offset] = static_cast<Replxx::Color>(span.color);
        }
      }
      else {
        auto completions = Replxx::completions_t {};
        auto len = 0;
        getCommandCompletion(context, _impl->commands, completions, len);
        if (completions.empty())
          for (auto& color : colors)
            color = Replxx::Color::RED;
      }
    }
  }

  //--------------------------------------------------------------------

  template <typename T> auto Repl::hook_hint(const std::string& input, int& len, Color& color, T& res) -> void {
    static const auto options = core::SplitOptions { "", ";", "\"\"\'\'" };
    const auto s = vectorFromIterable(core::Split(input, options));

    if (s.size() > 0) {
      const auto context = s.back();

      if (const auto command = selectCommand(context); command != nullptr) {
        command->updateIFN(context);
        const auto& hint = command->data().hint;
        res.emplace_back(core::CString(hint.str));
        len = hint.len;
        color = hint.color;
      }
      else {
        getCommandCompletion(context, _impl->commands, res, len);
        if (res.size() == 1)
          color = Color::GREEN;
      }
    }
  }

  //--------------------------------------------------------------------

  class ExitCommand final : public Command<Requirements<Repl>> {
      using parent_t = Command<Requirements<Repl>>;

    public:
      ExitCommand() : parent_t("exit") {}

    private:
      auto exec(core::Output&) -> void final {
        repl::get<Repl>(context()).exit();
      }

      auto description() const -> core::StringView final {
        return "Quit the program";
      }
  };

  //--------------------------------------------------------------------

  class ListCommandsCommand final : public Command<Requirements<Repl>> {
      using parent_t = Command<Requirements<Repl>>;

    public:
      ListCommandsCommand() : parent_t("commands") {}

    private:
      auto exec(core::Output& output) -> void final {
        repl::get<Repl>(context()).show_commands(output);
      }

      auto description() const -> core::StringView final {
        return "List all registered commands";
      }
  };

  //--------------------------------------------------------------------

  class ShowHistoryCommand final : public Command<Requirements<Repl>> {
      using parent_t = Command<Requirements<Repl>>;

    public:
      ShowHistoryCommand() : parent_t("history") {}

    private:
      auto exec(core::Output& output) -> void final {
        repl::get<Repl>(context()).show_history(output);
      }

      auto description() const -> core::StringView final {
        return "Show command history";
      }
  };

  //--------------------------------------------------------------------

  Repl::Repl() : _impl(core::make_unique<Impl>()) {
    _impl->rx.install_window_change_handler();
    _impl->rx.history_load(_impl->history_path);
    _impl->rx.set_max_history_size(128);
    _impl->rx.set_max_hint_rows(3);

    // completion, color and hints callbacks
    _impl->rx.set_completion_callback([this](const std::string& context, int& contextLen) {
      auto res = Replxx::completions_t {};
      hook_complete(context, contextLen, res);
      return res;
    });
    _impl->rx.set_highlighter_callback([this](const std::string& context, Replxx::colors_t& colors) {
      hook_color(context, colors);
    });
    _impl->rx.set_hint_callback([this](const std::string& context, int& contextLen, Replxx::Color& color) {
      auto res = Replxx::hints_t {};
      hook_hint(context, contextLen, reinterpret_cast<Color&>(color), res);
      return res;
    });

    // other api calls
    //rx.set_word_break_characters(separators);
    _impl->rx.set_completion_count_cutoff(128);
    _impl->rx.set_double_tab_completion(false);
    _impl->rx.set_complete_on_empty(true);
    _impl->rx.set_beep_on_ambiguous_completion(false);
    _impl->rx.set_no_color(false);

    // add dependency on itself
    _deps.add<Repl>(this);

    // commands creations
    add_command<ExitCommand>();
    add_command<ListCommandsCommand>();
    add_command<ShowHistoryCommand>();

    registerCommands(*this);
  }

  //--------------------------------------------------------------------

  auto Repl::show_history(core::Output& output) -> void {
    for (const auto i : core::Range(0, _impl->rx.history_size()))
      output("{:>4} : {}\n", i, _impl->rx.history_line(i));
  }

  //--------------------------------------------------------------------

  auto Repl::show_commands(core::Output& output) -> void {
    auto maxSize = 0u;
    for (const auto& command : _impl->commands) {
      const auto size = command->name().size();
      if (size > maxSize)
        maxSize = size;
    }
    const auto format = core::format("{}{}{}\n", "{:^", maxSize, "} : {}");
    for (const auto& command : _impl->commands)
      output(format, command->name(), command->description());
  }

  //--------------------------------------------------------------------

  Repl::~Repl() {
    _impl->rx.history_save(_impl->history_path);
  }

  //--------------------------------------------------------------------

  auto Repl::selectCommand(core::StringView str) -> CommandBase * {
    const auto input = core::getFront(str);
    for (auto& command : _impl->commands)
      if (command->name() == input)
        return command.get();
    return nullptr;
  }

  auto Repl::runCommand(core::StringView input, core::Output& output) -> void {
    static const auto options = core::SplitOptions { "", ";", "\"\"\'\'" };
    for (const auto str : core::Split(input, options)) {
      const auto command = selectCommand(str);
      if (command != nullptr) {
        command->updateIFN(str, true);
        if (command->isValid())
          command->exec(output);
        else
          output("Syntex error\n");
      }
      else
        output("Command \"{}\" not found\n", core::getFront(str));
    }
  }

  auto Repl::run(core::Output& output) -> void {
    while (!_impl->exit) {
      auto c_input = static_cast<const char *>(nullptr);

      do {
        c_input = _impl->rx.input("\x1b[1;32mta\x1b[0m> ");
      } while (c_input == nullptr && errno == EAGAIN);

      if (c_input == nullptr)
        break;

      runCommand(c_input, output);

      _impl->rx.history_add(c_input);
    }
  }

  auto Repl::run(const core::Path& path, core::Output& output) -> void {
    _impl->exit = false;
    auto file = std::ifstream { path };
    std::string str;

    while (!_impl->exit && std::getline(file, str)) {
      output("> \x1b[1;32m{}\x1b[0m\n", static_cast<const char*>(core::CString(str)));
      if (str != "")
        runCommand(str, output);
    }

    run(output);
  }

  auto Repl::exit() -> void {
    _impl->exit = true;
  }

  //--------------------------------------------------------------------

} // namespace ta::repl
