#include "Paje/pajeParser.hpp"

#include "Paje/pajeTrace.hpp"
#include "Parser/file.hpp"

#include <list>
#include <thread>


namespace ta::paje {

  //---------------------------------------------------
  // File
  //---------------------------------------------------

  auto nextLine(parser::File& file) -> bool {
    auto isValidLine = [](const auto& line) {
      for (const auto& c : line) {
        if (c == '#')
          break;
        if (c == ' ' || c == '\t')
          continue;
        return true;
      }
      return false;
    };

    do {
      if (!file.nextLine())
        return false;
    } while (!isValidLine(file.currentLine())); // omit commentaries and empty lines

    return true;
  }

  //---------------------------------------------------
  // Parser
  //---------------------------------------------------

  Parser::Parser() = default;
  Parser::~Parser() = default;

  //---------------------------------------------------

  auto parseHeaderBloc(parser::File& file, const Trace& trace) -> std::pair<PajeEventTypeId, PajeEventType> {
    static const auto splitOptions = []() {
      auto res = core::SplitOptions {};
      res.breakers = "%";
      return res;
    }();

    const auto headerTokens = vectorFromIterable(core::Split(file.currentLine(), splitOptions));
    Assert(headerTokens[0] == "%", "Not an header line : \"{}\" ({}) -> {}", file.currentLine(), file.currentLineNumber(), headerTokens.span());

    if (headerTokens.size() != 4) throw SyntaxError("Bad token number : {}", headerTokens.span());

    // the first word of the bloc must be "EventDef"
    if (headerTokens[1] != "EventDef")
      throw SyntaxError("Unexpected token {} at line {}", headerTokens[1], file.currentLineNumber());

    // get the event class
    const auto type = parseEventClass(headerTokens[2]); // TODO
    if (type == EventClass::UNKNOWN)
      throw SyntaxError("Unknown Event type \"{}\" at line {}", headerTokens[2], file.currentLineNumber());

    // get the event id (must be an int)
    for (const auto c : headerTokens[3])
      if (c < '0' || c > '9')
        throw SyntaxError("Event id \"{}\" must be an integer (line {})", headerTokens[3], file.currentLineNumber());
    const auto id = PajeEventTypeId(core::toInt(headerTokens[3]));
    if (trace.pajeEventTypeExists(id)) // Check event type id is not already declared
      throw SyntaxError("Event id \"{}\" was already declared");

    // get all the fields line by line
    auto fields = FieldTypeList {};
    while (true) {
      // Read the next line and check it is a header line
      if (!nextLine(file)) throw SyntaxError("Unexpected end of file");
      const auto tokens = vectorFromIterable(core::Split(file.currentLine(), splitOptions));

      if (tokens.size() < 1 || tokens[0] != "%")
        throw SyntaxError("Unexpected char at line {}", file.currentLineNumber());


      if (tokens.size() < 2) throw SyntaxError("Bad token number");

      // Get first word. It can be "EndEvendDef", or a FieldType name
      if (tokens[1] == "EndEventDef") break; // the bloc is finished

      if (tokens.size() != 3) throw SyntaxError("Bad token number");

      // Read the type of the field
      const auto fieldType = parseValueType(tokens[2]);
      if (fieldType == ValueType::UNKNOWN)
        throw SyntaxError("Unknown field type \"{}\" at line {}", tokens[2], file.currentLineNumber());

      // Cannot handle more than FieldListSize fields by descriptor...
      fields.emplace_back(FieldType { tokens[1], fieldType });
    }

    return std::pair { id, PajeEventType { type, nlc::move(fields) } };
  }

  //---------------------------------------------------

  struct Data {
    parser::File::LineNumber lineNumber;
    core::Vector<core::StringView> tokens;
  };

  class SplinLock {
    public:
      auto lock() { while (_locked.exchange(true)) DO_NOTHING; }
      auto unlock() { _locked.store(false); }
    private:
      std::atomic_bool _locked = false;
  };

  template <typename T> struct ListWithMtx {
    std::list<T> list;
    SplinLock mtx;
    std::atomic_bool noMoreWillBeAdded = false;
  };

  template <typename T> auto spinLockOnList(ListWithMtx<T>& l, T& value) -> bool {
    while (true) {
      auto lock = std::unique_lock(l.mtx);
      if (l.list.empty()) {
        if (l.noMoreWillBeAdded)
          return false;
      }
      else {
        value = nlc::move(l.list.front());
        l.list.pop_front();
        return true;
      }
    }
    AssertNotReached("");
  }

  template <typename T> auto pushInList(ListWithMtx<T>& l, T&& value) {
    auto lck = std::unique_lock(l.mtx);
    l.list.emplace_back(nlc::move(value));
  }

  auto splitLines(ListWithMtx<Data>& l, parser::File& file) {
    do {
      Assert(core::frontChar(file.currentLine()) != '%', "\"{}\"", file.currentLine());

      auto tokens = core::Vector<core::StringView> {};
      for (const auto t : core::Split(file.currentLine()))
        tokens.emplace_back(t);

      pushInList(l, Data { file.currentLineNumber(), nlc::move(tokens) });
    } while (nextLine(file));

    l.noMoreWillBeAdded = true;
  }

  auto parseEvent(const Data& data, paje::Trace& trace, trace::TraceBuilder<Trace>& builder) -> void {
    PajeEvent event;
    event.typeId = [str = data.tokens[0], lineNumber = data.lineNumber]() {
      auto res = 0u;
      for (const auto c : str) {
        if (c < '0' || c > '9')
          throw SyntaxError("Event id \"{}\" must be an int at line {}", str, lineNumber);
        res *= 10u;
        res += c.codepoint() - '0';
      }
      return PajeEventTypeId(res);
    }();


    if (!trace.pajeEventTypeExists(event.typeId))
      throw SyntaxError("Event type {} was not declared (line {})", event.typeId, data.lineNumber);
    const auto& eventType = trace.pajeEventType(event.typeId);

    event.fields.resize(eventType.fieldTypes.size());
    for (const auto index : core::Range(0u, eventType.fieldTypes.size())) {
      const auto& field = eventType.fieldTypes[index];
      Assert(field.name != "UNKNOWN", "");

      auto& eventField = event.fields[index];
      eventField.name = field.name;

      const auto& token = data.tokens[index + 1];
      switch (eventType.fieldTypes[index].type) {
        case ValueType::Color: {
          eventField.value = Color(0, 0, 0); // TODO
        }
          break;
        case ValueType::Date:
          eventField.value = nlc::meta::seconds<double>(core::toDouble(token));
          break;
        case ValueType::Double:
          eventField.value = core::toFloat(token);
          break;
        case ValueType::Hex:
          eventField.value = core::toInt(token, 16);
          break;
        case ValueType::Int:
          eventField.value = core::toInt(token);
          break;
        case ValueType::String:
          eventField.value = core::removeQuotes(token, "\"\"");
          break;
        case ValueType::UNKNOWN:
          AssertNotReached();
          break;
      }
    }

    switch (eventType.eventClass) {
      case EventClass::DefineContainerType: [[fallthrough]];
      case EventClass::DefineStateType: [[fallthrough]];
      case EventClass::DefineEventType: [[fallthrough]];
      case EventClass::DefineVariableType: [[fallthrough]];
      case EventClass::DefineLinkType: [[fallthrough]];
      case EventClass::DefineEntityValue:
        break;
      case EventClass::CreateContainer:
        trace.addContainer(nlc::move(event), builder);
        break;
      case EventClass::DestroyContainer:
        trace.removeContainer(nlc::move(event));
        break;
      case EventClass::NewEvent: [[fallthrough]];
      case EventClass::SetVariable: [[fallthrough]];
      case EventClass::AddVariable: [[fallthrough]];
      case EventClass::PushState: [[fallthrough]];
      case EventClass::PopState: [[fallthrough]];
      case EventClass::ResetState: [[fallthrough]];
      case EventClass::SetState: [[fallthrough]];
      case EventClass::SubVariable: {
        auto containerId = trace.getContainerId(getFieldValue(event, "Container"));
        builder.addEvent(nlc::move(event), containerId);
      }
        break;
      case EventClass::StartLink: {
        auto containerId = trace.getContainerId(getFieldValue(event, "StartContainer"));
        builder.addEvent(nlc::move(event), containerId);
      }
        break;
      case EventClass::EndLink: {
        auto containerId = trace.getContainerId(getFieldValue(event, "EndContainer"));
        builder.addEvent(nlc::move(event), containerId);
      }
        break;
      case EventClass::UNKNOWN:
        AssertNotReached("Unknown value must be threated earlier");
    }
  }

  auto parseLines(ListWithMtx<Data>& in, Trace& trace, trace::TraceBuilder<Trace>& builder) {
    auto data = Data {};
    while (spinLockOnList(in, data)) {
      parseEvent(data, trace, builder);
    }
  }

  //---------------------------------------------------

  auto Parser::open(const core::Path& path,
                    trace::TraceStorage& storage,
                    core::StringView traceName) -> trace::TraceStructureId {
    if (!_file.open(path))
      return trace::TraceStructureId::invalid();
    _builder = core::make_unique<trace::TraceBuilder<Trace>>(storage, path, traceName);
    storage.data(storage.structure(_builder->structureId()).dataId)->format = "Paje";
    return _builder->structureId();
  }

  auto Parser::parseHeader(trace::TraceStorage& storage) -> void {
    auto& trace = *static_cast<Trace*>(storage.data(_builder->dataId()));
    while (core::frontChar(_file.currentLine()) == '%') {
      auto[id, event] = parseHeaderBloc(_file, trace);
      trace.addPajeEventType(id, nlc::move(event));
      nextLine(_file);
    }
  }

  auto Parser::parseBody(trace::TraceStorage& storage) -> void {
    auto& trace = *static_cast<Trace*>(storage.data(_builder->dataId()));
    auto lineTokens = ListWithMtx<Data> {};

    // auto spliter = std::thread { [&]() { splitLines(lineTokens, _file); } };
    splitLines(lineTokens, _file);
    parseLines(lineTokens, trace, *_builder);

    //spliter.join();
    Assert(lineTokens.list.empty(), "");
  }

  auto Parser::close(trace::TraceStorage& storage) -> void {
    auto& structure = storage.structure(_builder->structureId());
    Assert(structure.state == trace::TraceState::NotLoaded, "");
    structure.state = trace::TraceState::Ready;
    auto data = storage.data<Trace>(structure.dataId);
    data->setRawTrace(_file.stealmemory());
    _file.close();
    _builder.reset();
  }

  //---------------------------------------------------

} // namespace ta::paje
