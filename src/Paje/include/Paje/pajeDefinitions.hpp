#pragma once

#include "Core/output.hpp"

#include "nlc/meta/id.hpp"
#include "nlc/meta/units.hpp"

#include <variant>

namespace ta::paje {

  //---------------------------------------------------------------------

  /// ValueType represent the different data types defined by paje standard
  enum class ValueType {
      Color,
      Date,
      Double,
      Hex,
      Int,
      String,
      UNKNOWN,
  };

  using Date = nlc::meta::seconds<double>;
  using Int = std::int32_t;
  using Double = float;
  using Hex = std::uint32_t;
  using StringView = core::StringView;

  struct Color final {
    // paje standard define color channels as double between 0 and 1 but for memory compacity we use 8bits channels
    using Channel = std::uint8_t;
    Channel r = 0, g = 0, b = 0;
    Color() = default;
    Color(const Color&) = default;
    auto operator=(const Color&) -> Color& = default;
    Color(Channel _r, Channel _g, Channel _b) : r(_r), g(_g), b(_b) {}
  };

#define OP(op) auto operator op (const Color&, const Color&) -> bool;
  OP(==) OP(!=) OP(<) OP(>) OP(<=) OP(>=)
#undef OP

  //---------------------------------------------------------------------

  /// Value is a variant handling the different value types defined by paje
  using Value = std::variant<Color, Date, Double, Hex, Int, StringView>;

  auto parseValueType(StringView str) -> ValueType;
  auto getString(ValueType) -> const char *;
  auto type_of(const Value&) -> ValueType;

  //---------------------------------------------------------------------

  /// EventClass represent the different event types defined by paje standard
  enum class EventClass : std::uint8_t {
      // Types
      DefineContainerType,  // %PajeDefineContainerType Name Type [Alias]
      DefineStateType,      // %PajeDefineStateType Name Type [Alias]
      DefineEventType,      // %PajeDefineEventType
      DefineVariableType,   // %PajeDefineVariableType
      DefineLinkType,       // %PajeDefineLinkType
      DefineEntityValue,    // %PajeDefineEntityValue
      // Container
      CreateContainer,      // %PajeCreateContainer Time Name Type Container [Alias]
      DestroyContainer,     // %PajeDestroyContainer
      // States
      PushState,            // %PajePushState
      PopState,             // %PajePopState
      ResetState,           // %PajeResetState
      SetState,             // %PajeSetState
      // Event
      NewEvent,             // %PajeNewEvent
      // Variable
      SetVariable,          // %PajeSetVariable
      AddVariable,          // %PajeAddVariable
      SubVariable,          // %PajeSubVariable
      //Link
      StartLink,            // %PajeStartLnk
      EndLink,              // %PajeEndLink
      // Other
      UNKNOWN,
  };

  auto parseEventClass(StringView str) -> EventClass;
  auto getString(EventClass) -> const char *;

  //---------------------------------------------------------------------

  struct TypeIdDesc final {
    using underlying_t = std::uint16_t;
    static constexpr underlying_t invalid_value = -1;
    static constexpr underlying_t default_value = invalid_value;
  };
  /// PajeEventTypeId identify the type of an event. It's the numerical id associating an event to and EventDef bloc
  using PajeEventTypeId = nlc::meta::id<TypeIdDesc>;

  //---------------------------------------------------------------------

  using FieldName = StringView;

  //---------------------------------------------------------------------

  /// FieldType describe a field as in paje EventDef blocs
  struct FieldType final {
    FieldName name;
    ValueType type;
  };

  /// Field describe a field in an event
  struct Field final {
    FieldName name;
    Value value;
  };

  using FieldTypeList = core::Vector<FieldType>;
  using FieldList = core::Vector<Field>;

  /// PajeEventType represent a type definition from a %EventDef paje declaration
  struct PajeEventType final {
    EventClass eventClass;
    FieldTypeList fieldTypes;
  };

  /// PajeEvent represent a event line in a paje file
  struct PajeEvent final {
    PajeEventTypeId typeId;
    FieldList fields;
    auto timeStamp() const -> Date;
  };

  auto getFieldValue(const PajeEvent&, FieldName) -> const Value&;
  auto hasFieldValue(const PajeEvent&, FieldName) -> bool;

  //---------------------------------------------------------------------

  auto operator==(const PajeEvent&, const PajeEvent&) -> bool;
  auto operator==(const PajeEvent&, const PajeEvent&) -> bool;
  auto operator!=(const PajeEvent&, const PajeEvent&) -> bool;

  auto operator<(const PajeEvent&, const PajeEvent&) -> bool;

  //---------------------------------------------------------------------

} // namespace ta::paje

#define DEFINE_PAJE_ENUM_OUTPUT(name)                                                               \
template <> struct fmt::formatter<ta::paje::name> : public formatter<string_view> {                 \
  template <typename FormatContext> auto format(const ta::paje::name& value, FormatContext& ctx) {  \
    return formatter<string_view>::format(ta::paje::getString(value), ctx);                         \
  }                                                                                                 \
};

DEFINE_PAJE_ENUM_OUTPUT(ValueType)
DEFINE_PAJE_ENUM_OUTPUT(EventClass)

#undef DEFINE_PAJE_ENUM_OUTPUT

template <> struct fmt::formatter<ta::paje::Color> : public formatter<string_view> {
  template <typename FormatContext> auto format(const ta::paje::Color& color, FormatContext& ctx) {
    return formatter<string_view>::format(fmt::format("#{X}{X}{X}", color.r, color.g, color.b), ctx);
  }
};

template <> struct fmt::formatter<ta::paje::Value> : public formatter<string_view> {
  template <typename FormatContext> auto format(const ta::paje::Value& value, FormatContext& ctx) {
    return std::visit([&](const auto& v) { return formatter<string_view>::format(fmt::format("{}", v), ctx); }, value);
  }
};

template <> struct fmt::formatter<ta::paje::FieldType> : public formatter<string_view> {
  template <typename FormatContext> auto format(const ta::paje::FieldType& fieldType, FormatContext& ctx) {
    return formatter<string_view>::format(fmt::format("\"{}\" : \"{}\"", fieldType.name, fieldType.type), ctx);
  }
};

template <> struct fmt::formatter<ta::paje::PajeEvent> : public formatter<string_view> {
  template <typename FormatContext> auto format(const ta::paje::PajeEvent& event, FormatContext& ctx) {
    auto output = ta::core::StringOutput {};
    output("{}(", event.typeId);
    auto first = true;
    for (const auto& field : event.fields) {
      if (field.name == "UNKNOWN") break;
      if (first) first = false;
      else output(", ");
      output("{}:\"{}\"", field.name, field.value);
    }
    output(")");
    return format_to(ctx.out(), "{}", output.str());
  }
};
