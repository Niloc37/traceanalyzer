#pragma once

#include "nlc/meta/id.hpp"

#include "pajeDefinitions.hpp"
#include "pajeParser.hpp"

#include "Trace/trace.hpp"
#include "Trace/treeBuilder.hpp"

#include <map>

namespace ta::paje {

  //---------------------------------------------------------------

  class Trace final : public trace::TraceData<PajeEvent> {
    public:
      Trace() : trace::TraceData<PajeEvent>("Paje") {}

    public:
      auto print(core::Output&, trace::EventDataId) const -> void override;

    public:
      auto addPajeEventType(PajeEventTypeId, PajeEventType&&) -> void;

    public:
      [[nodiscard]] auto pajeEventTypeExists(PajeEventTypeId id) const -> bool;
      [[nodiscard]] auto pajeEventType(PajeEventTypeId id) const -> const PajeEventType&;

    public:
      auto addContainer(PajeEvent&& event, trace::TraceBuilder<Trace>&) -> void;
      auto removeContainer(PajeEvent&& event) -> void;

    public:
      [[nodiscard]] auto getContainerId(const paje::Value&) const -> trace::SequenceId;
      [[nodiscard]] decltype(auto) eventTypes() const { return (_eventTypes); }

      auto setRawTrace(core::HeapBuffer<char>&& rawTrace) {
        LOG_DEBUG("Relocate raw trace");
        core::memmove(_rawTrace, rawTrace);
      }

    private:
      std::map<PajeEventTypeId, PajeEventType> _eventTypes;
      std::map<paje::Value, trace::SequenceId> _containersByName;
      std::map<paje::Value, trace::SequenceId> _containersByAlias;
      core::HeapBuffer<char> _rawTrace;
  };

  //---------------------------------------------------------------

}
