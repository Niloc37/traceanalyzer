#pragma once

#include "Core/debug.hpp"
#include "Parser/file.hpp"
#include "Parser/parser.hpp"
#include "Trace/traceStorage.hpp"
#include "Trace/treeBuilder.hpp"

namespace ta::core { class Path; }
namespace ta::paje {

  //---------------------------------------------------

  class Trace;

  //---------------------------------------------------

  struct SyntaxError {
    template <typename...Args> SyntaxError(Args&& ...args) {
      LOG_ERROR(nlc::forward<Args>(args)...);
      Assert(false, "");
    }
  };

  //---------------------------------------------------

  class Parser : public parser::Parser {
    public:
      Parser();
      ~Parser();

    public:
      auto open(const core::Path& path,
                trace::TraceStorage& storage,
                core::StringView traceName) -> trace::TraceStructureId final;
      auto parseHeader(trace::TraceStorage& storage) -> void final;
      auto parseBody(trace::TraceStorage& storage) -> void final;
      auto close(trace::TraceStorage& storage) -> void final;

      static auto name() { return core::StringView("Paje"); }
      static auto accept(const core::Path& path) {
        return path.extension() == "trace";
      }

    public:
      core::unique_ptr<trace::TraceBuilder<Trace>> _builder;
      parser::File _file;
  };

  //---------------------------------------------------

}
