#include "Paje/pajeDefinitions.hpp"

#include "Core/debug.hpp"
#include "Core/compare.hpp"
#include "Core/range.hpp"

#include <iomanip>
#include <map>

namespace ta::paje {

  //---------------------------------------------------------------------

  namespace {
    constexpr auto compareColor(const Color& lhs, const Color& rhs) {
      return core::compare(lhs.r, rhs.r, lhs.g, rhs.g, lhs.b, rhs.b);
    }
  } // anonymous namespace

  auto operator==(const Color& lhs, const Color& rhs) -> bool { return compareColor(lhs, rhs) == core::CompareResult::Equal; }
  auto operator!=(const Color& lhs, const Color& rhs) -> bool { return compareColor(lhs, rhs) != core::CompareResult::Equal; }
  auto operator>(const Color& lhs, const Color& rhs) -> bool { return compareColor(lhs, rhs) == core::CompareResult::More; }
  auto operator>=(const Color& lhs, const Color& rhs) -> bool { return compareColor(lhs, rhs) != core::CompareResult::Less; }
  auto operator<(const Color& lhs, const Color& rhs) -> bool { return compareColor(lhs, rhs) == core::CompareResult::Less; }
  auto operator<=(const Color& lhs, const Color& rhs) -> bool { return compareColor(lhs, rhs) != core::CompareResult::More; }

  auto operator>>(std::istream& is, Color& color) -> std::istream& {
    std::string _buffer;
    is >> std::quoted(_buffer);
    std::stringstream stream { _buffer };
    float r, g, b;
    stream >> r >> g >> b;
    color.r = static_cast<Color::Channel>(255.f * r);
    color.g = static_cast<Color::Channel>(255.f * g);
    color.b = static_cast<Color::Channel>(255.f * b);
    return is;
  }

  auto operator>>(std::istream& is, Date& date) -> std::istream& {
    Date::underlying buffer;
    is >> buffer;
    date = Date { buffer };
    return is;
  }

//---------------------------------------------------------------------

  auto parseEventClass(StringView str) -> EventClass {
    static const auto stringToEventClass = std::map<StringView, EventClass> {
#define IMPL(name) { "Paje" #name, EventClass::name }
        IMPL(DefineContainerType),
        IMPL(DefineStateType),
        IMPL(DefineEventType),
        IMPL(DefineVariableType),
        IMPL(DefineLinkType),
        IMPL(DefineEntityValue),
        IMPL(CreateContainer),
        IMPL(DestroyContainer),
        IMPL(PushState),
        IMPL(PopState),
        IMPL(ResetState),
        IMPL(SetState),
        IMPL(NewEvent),
        IMPL(SetVariable),
        IMPL(AddVariable),
        IMPL(SubVariable),
        IMPL(StartLink),
        IMPL(EndLink)
#undef IMPL
    };

    const auto it = stringToEventClass.find(str);
    if (it == stringToEventClass.end())
      return EventClass::UNKNOWN;
    return it->second;
  }

  auto getString(EventClass v) -> const char * {
    switch (v) {
#define IMPL(name) case EventClass::name: return #name
      IMPL(DefineContainerType);
      IMPL(DefineStateType);
      IMPL(DefineEventType);
      IMPL(DefineVariableType);
      IMPL(DefineLinkType);
      IMPL(DefineEntityValue);
      IMPL(CreateContainer);
      IMPL(DestroyContainer);
      IMPL(PushState);
      IMPL(PopState);
      IMPL(ResetState);
      IMPL(SetState);
      IMPL(NewEvent);
      IMPL(SetVariable);
      IMPL(AddVariable);
      IMPL(SubVariable);
      IMPL(StartLink);
      IMPL(EndLink);
#undef IMPL
      default:
        return "UNKNOWN";
    }
  }

//---------------------------------------------------------------------

  auto parseValueType(StringView str) -> ValueType {
    const auto stringtoValueType = std::map<StringView, ValueType> {
        { "color",  ValueType::Color },
        { "date",   ValueType::Date },
        { "double", ValueType::Double },
        { "hex",    ValueType::Hex },
        { "int",    ValueType::Int },
        { "string", ValueType::String },
    };

    const auto it = stringtoValueType.find(str);
    if (it == stringtoValueType.end())
      return ValueType::UNKNOWN;
    return it->second;
  }

  auto getString(ValueType v) -> const char * {
    switch (v) {
#define IMPL(name) case ValueType::name: return #name
      IMPL(Color);
      IMPL(Date);
      IMPL(Double);
      IMPL(Hex);
      IMPL(Int);
      IMPL(String);
#undef IMPL
      default:
        return "UNKNOWN";
    }
  }

  auto type_of(const Value& v) -> ValueType {
    return std::visit(nlc::meta::overloaded {
                          [](const Color&) { return ValueType::Color; },
                          [](const Date&) { return ValueType::Date; },
                          [](const Double&) { return ValueType::Double; },
                          [](const Hex&) { return ValueType::Hex; },
                          [](const Int&) { return ValueType::Int; },
                          [](const StringView&) { return ValueType::String; },
                          [](const auto&) { return ValueType::UNKNOWN; }},
                      v);
  }

  //---------------------------------------------------------------------

  auto PajeEvent::timeStamp() const -> Date {
    const auto& value = getFieldValue(*this, "Time");
    Assert(type_of(value) == ValueType::Date, "Value::Type::{} == ValueType::Date", type_of(value));
    return std::get<Date>(value);
  }

  //---------------------------------------------------------------------

  auto getFieldValue(const PajeEvent& event, FieldName name) -> const Value& {
    for (const auto& field : event.fields) {
      if (field.name == name)
        return field.value;
    }
    AssertNotReached("Field {} is absent of event", name);
    return event.fields[0].value;
  }

  auto hasFieldValue(const PajeEvent& event, FieldName name) -> bool {
    for (const auto& field : event.fields) {
      if (field.name == name)
        return true;
    }
    return false;
  }

//---------------------------------------------------------------------

  auto operator==(const PajeEvent& lhs, const PajeEvent& rhs) -> bool {
    return !(lhs != rhs);
  }

  auto operator!=(const PajeEvent& lhs, const PajeEvent& rhs) -> bool {
    return lhs < rhs || rhs < lhs;
  }

  auto operator<(const PajeEvent& lhs, const PajeEvent& rhs) -> bool {
    if (lhs.typeId != rhs.typeId)
      return lhs.typeId < rhs.typeId;

    Assert(lhs.fields.size() == rhs.fields.size(), "");

    for (const auto i : core::Range(0u, lhs.fields.size())) {
      if (lhs.fields[i].name != rhs.fields[i].name)
        return lhs.fields[i].name < rhs.fields[i].name;

      if (const auto name = lhs.fields[i].name;
          name == "Alias" ||
          name == "Container" ||
          // name == "EndContainer" ||
          name == "Footprint" ||
          name == "GFlop" ||
          name == "Iteration" ||
          name == "JobId" ||
          name == "Key" ||
          // name == "Name" ||
          // name == "Params" ||
          name == "Size" ||
          // name == "StartContainer" ||
          name == "Subiteration" ||
          name == "SubmitOrder" ||
          name == "Tag" ||
          name == "Time" ||
          // name == "Type" ||
          // name == "Value" ||
          name == "X" ||
          name == "Y")
        continue;

      if (lhs.fields[i].value != rhs.fields[i].value)
        return lhs.fields[i].value < rhs.fields[i].value;
    }

    return false;
  }

//---------------------------------------------------------------------

} // namespace ta::paje

