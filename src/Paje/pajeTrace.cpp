#include "Paje/pajeTrace.hpp"

namespace ta::paje {

  //---------------------------------------------------
  // Trace
  //---------------------------------------------------

  auto Trace::print(core::Output& output, trace::EventDataId id) const -> void {
    const auto& event = operator[](id);
    output("{}(", pajeEventType(event.typeId).eventClass);
    auto first = true;
    for (const auto& field : event.fields) {
      if (field.name == "UNKNOWN") break;
      if (first) first = false;
      else output(", ");
      output("{}:\"{}\"", field.name, field.value);
    }
    output(")");
  }

  //---------------------------------------------------

  auto Trace::addPajeEventType(PajeEventTypeId id, PajeEventType&& type) -> void {
    if (pajeEventTypeExists(id)) {
      LOG_ERROR("Event type {} already exists", id);
      throw std::exception {};
    }
    _eventTypes[id] = nlc::move(type);
  }

  //---------------------------------------------------

  auto Trace::pajeEventTypeExists(PajeEventTypeId id) const -> bool {
    const auto it = _eventTypes.find(id);
    return it != _eventTypes.end();
  }

  //---------------------------------------------------

  auto Trace::pajeEventType(PajeEventTypeId id) const -> const PajeEventType& {
    Assert(pajeEventTypeExists(id), "");
    return _eventTypes.at(id);
  }

  //---------------------------------------------------

  auto Trace::getContainerId(const Value& value) const -> trace::SequenceId {
    auto aliasIt = _containersByAlias.find(value);
    if (aliasIt != _containersByAlias.end())
      return aliasIt->second;

    auto nameIt = _containersByName.find(value);
    if (nameIt != _containersByName.end())
      return nameIt->second;

    return trace::SequenceId::invalid();
  }

  //---------------------------------------------------

  auto Trace::addContainer(PajeEvent&& event, trace::TraceBuilder<Trace>& builder) -> void {
    const auto fatherName = getFieldValue(event, "Container");
    const auto fatherId = getContainerId(fatherName);

    const auto& containerName = getFieldValue(event, "Name");
    const auto containerHasAlias = hasFieldValue(event, "Alias");
    const auto containerAlias = containerHasAlias ? getFieldValue(event, "Alias") : StringView();

    const auto containerId = builder.addContainer(nlc::move(event), fatherId);

    // Assert(_containersByName.find(containerName) == _containersByName.end(), "Container {} already exists", containerName); // TODO
    _containersByName[containerName] = containerId;

    if (containerHasAlias) {
      // Assert(_containersByAlias.find(alias) == _containersByAlias.end(), "Container {} already exists", alias); // TODO
      _containersByAlias[containerAlias] = containerId;
    }
  }

  //---------------------------------------------------

  auto Trace::removeContainer(ta::paje::PajeEvent&& event) -> void {
    const auto& name = getFieldValue(event, "Name");
    _containersByAlias.erase(name);
    _containersByAlias.erase(name);
  }

  //---------------------------------------------------

}
