cmake_minimum_required(VERSION 3.7)

add_library(TraceAnalyser-trace STATIC
  include/Trace/path.hpp
  include/Trace/sequence.hpp
  include/Trace/trace.hpp
  include/Trace/traceStorage.hpp
  include/Trace/trahay.hpp
  include/Trace/treeBuilder.hpp
  path.cpp
  sequence.cpp
  traceStorage.cpp
  trahay.cpp
  treeBuilder.cpp
)

target_include_directories(TraceAnalyser-trace
  PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
)

target_compile_options(TraceAnalyser-trace
  PRIVATE
    -Wall -Wextra -Wpedantic
    -Werror
)

target_link_libraries(TraceAnalyser-trace
  PUBLIC
    TraceAnalyser-core
  PRIVATE
    pthread
)

