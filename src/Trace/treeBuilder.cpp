#include "Trace/treeBuilder.hpp"

namespace ta::trace {

  auto addSequenceSon(TraceStructure& structure, SequenceId fatherId, SequenceId sonId) -> void {
    auto& fatherSons = Sequence::get_sons(structure.get<Sequence::Data>(fatherId));
    structure.get<SequenceOccurences>(sonId).insert(fatherId, SequenceOccurenceIndex(fatherSons.size()));
    fatherSons.push_back(sonId);
  }

  auto makeGroup(TraceStructure& structure, SequenceId eventId) -> SequenceId {
    const auto groupId = structure.emplace_back(Sequence::Group {});
    auto& groupOccurences = structure.get<SequenceOccurences>(groupId);
    auto& groupSons = Sequence::get_sons(structure.get<Sequence::Data>(groupId));
    auto& sequenceOccurences = structure.get<SequenceOccurences>(eventId);

    swap(groupOccurences, sequenceOccurences);

    Assert(sequenceOccurences.empty(), "");
    Assert(groupSons.empty(), "");

    sequenceOccurences.insert(groupId, SequenceOccurenceIndex(0));
    groupSons.push_back(eventId);

    for (const auto&[fatherId, index] : groupOccurences) {
      Sequence::get_sons(structure.get<Sequence::Data>(fatherId)).replace(index, groupId);
    }

    return groupId;
  }

} // namespace ta::trace
