#include "Trace/path.hpp"

namespace ta::trace {

  auto Path::push(SequenceId id, unsigned int index) -> void {
    const auto s = size();
    if (s > 0) {
      _data[s - 1].index = SequenceOccurenceIndex(index);
    }
    _data.emplace_back(Step { id, SequenceOccurenceIndex::invalid() });
  }

  auto Path::pop() -> void {
    _data.pop_back();
  }

  auto Path::copy() const -> Path {
    Path res;
    res._data.reserve(_data.size());
    for (const auto& s : _data)
      res._data.emplace_back_already_reserved(s);
    return res;
  }

  auto countAllOccurences(const trace::TraceStructure& structure, trace::SequenceId id) -> size_t {
    auto impl = [&](auto f, auto id, auto count) {
      // if root reached
      if (id == structure.rootId) {
        return count;
      }

      auto res = 0u;
      for (const auto occurence : structure.get<SequenceOccurences>(id)) {
        const auto& fatherData = structure.get<Sequence::Data>(occurence.id);
        if (Sequence::get_type(fatherData) == trace::Sequence::Type::Loop)
          res += f(f, occurence.id, std::get<trace::Sequence::Loop>(fatherData).count * count);
        else
          res += f(f, occurence.id, count);
      }
      return res;
    };
    return impl(impl, id, 1u);
  }

  auto getOccurence(const trace::TraceStructure& structure, const Path& path) -> size_t {
    auto revPath = core::Vector<trace::SequenceOccurence> {};
    revPath.push_back({ path.back().id, path.back().index });
    auto res = 0u;

    auto analysePath = [&structure, &revPath, &path]() -> size_t {
      enum CMP { LOWER, EQUAL }; // GREATER returns
      auto cmp = EQUAL;
      auto locRes = 0u;

      const auto size = revPath.size();
      for (const auto i : core::Range(0u, size - 1)) {
        const auto ri = size - 1 - i;

        switch (cmp) {
          case EQUAL: {
            Assert(path[i].id == revPath[ri].id, "");

            if (path[i].index < revPath[ri].index)
              return 0u;

            if (path[i].index > revPath[ri].index) {
              cmp = LOWER;
              locRes = 1;
            }

            const auto& seq = structure.get<Sequence::Data>(path[i].id);
            if (Sequence::get_type(seq) == trace::Sequence::Type::Loop) {
              const auto iter = std::get<trace::Sequence::Loop>(seq).count;
              Assert(revPath[ri].index.value() < iter, "{}[{}] < {} ({})", revPath.span(), ri, iter, path.span());

              if (cmp == LOWER)
                locRes *= iter - 1;
              else
                locRes *= revPath[ri].index.value();
            }
            break;
          }
          case LOWER: {
            const auto& seq = structure.get<Sequence::Data>(revPath[ri].id);
            if (Sequence::get_type(seq) == trace::Sequence::Type::Loop)
              locRes *= std::get<trace::Sequence::Loop>(seq).count;

            break;
          }
        }
      }

      return locRes;
    };

    auto explore = [&analysePath, &res, &revPath, &structure](auto self) -> void {
      if (revPath.back().id == structure.rootId) {
        res += analysePath();
      }
      else {
        for (const auto& occurence : structure.get<SequenceOccurences>(revPath.back().id)) {
          revPath.push_back({ occurence.id, occurence.index });
          self(self);
          revPath.pop_back();
        }
      }
    };
    explore(explore);

    return res;
  }

  auto firstEvent(const trace::TraceStructure& structure, const Path& path) -> EventDataId {
    auto tmpPath = path.copy();

    while (true) {
      const auto sequenceId = tmpPath.back().id;
      const auto& sequence = structure.get<Sequence::Data>(sequenceId);
      switch (Sequence::get_type(sequence)) {
        case trace::Sequence::Type::Group: {
          const auto index = getOccurence(structure, tmpPath);
          const auto& groupSons = std::get<trace::Sequence::Group>(sequence).sons;
          return groupSons[index];
        }
        case trace::Sequence::Type::Container:
          return std::get<trace::Sequence::Container>(sequence).createEvent;
        case trace::Sequence::Type::Node:
          tmpPath.push(std::get<trace::Sequence::Node>(sequence).sons.front(), 0);
          break;
        case trace::Sequence::Type::Loop:
          tmpPath.push(std::get<trace::Sequence::Loop>(sequence).event, 0);
          break;
        default:
          AssertNotReached("Unknown type {}", Sequence::get_type(sequence));
      }
    };
  }

  auto lastEvent(const trace::TraceStructure& structure, const Path& path) -> EventDataId {
    auto tmpPath = path.copy();

    while (true) {
      const auto sequenceId = tmpPath.back().id;
      const auto& sequence = structure.get<Sequence::Data>(sequenceId);
      switch (Sequence::get_type(sequence)) {
        case trace::Sequence::Type::Group: {
          const auto index = getOccurence(structure, tmpPath);
          const auto& groupSons = std::get<trace::Sequence::Group>(sequence).sons;
          return groupSons[index];
        }
        case trace::Sequence::Type::Container: {
          const auto& sons = std::get<trace::Sequence::Container>(sequence).sons;
          tmpPath.push(sons.back(), sons.size() - 1);
          break;
        }
        case trace::Sequence::Type::Node: {
          const auto& sons = std::get<trace::Sequence::Node>(sequence).sons;
          tmpPath.push(sons.back(), sons.size() - 1);
          break;
        }
        case trace::Sequence::Type::Loop: {
          const auto& loop = std::get<trace::Sequence::Loop>(sequence);
          tmpPath.push(loop.event, loop.count - 1);
          break;
        }
        default:
          AssertNotReached("Unknown type {}", Sequence::get_type(sequence));
      }
    }
  }
}
