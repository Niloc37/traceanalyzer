#pragma once

#include "Core/debug.hpp"
#include "Core/outputHelper.hpp"
#include "Core/span.hpp"
#include "Core/vector.hpp"

#include "Core/uninitializedVector.hpp"
#include "Core/uniquePointer.hpp"
#include "nlc/meta/basics.hpp"
#include "nlc/meta/id.hpp"
#include "nlc/meta/type_value.hpp"

#include <variant>
#include <Core/structOfArray.hpp>

namespace ta::trace {

  class TraceStructure;
  class GenericTraceData;

  // SequenceId ////////////////////////////////////////////////////

  struct SequenceId_desr final {
    using underlying_t = unsigned int;
    static constexpr underlying_t invalid_value = static_cast<underlying_t>(-1);
    static constexpr underlying_t default_value = invalid_value;
  };
  using SequenceId = nlc::meta::id<SequenceId_desr>;

  // DataSequenceId ////////////////////////////////////////////////

  struct EventDataId_decr final {
    using underlying_t = unsigned int;
    //static constexpr underlying_t invalid_value = static_cast<underlying_t>(-1);
    //static constexpr underlying_t default_value = invalid_value;
  };
  using EventDataId = nlc::meta::id<EventDataId_decr>;

  // SequenceOccurenceIndex ////////////////////////////////////////

  struct SequenceOccurenceIndex_decr final {
    using underlying_t = unsigned int;
    static constexpr underlying_t invalid_value = static_cast<underlying_t>(-1);
    //static constexpr underlying_t default_value = invalid_value;
    static constexpr auto has_increment = true;
  };
  using SequenceOccurenceIndex = nlc::meta::id<SequenceOccurenceIndex_decr>;

  // Occurences /////////////////////////////////////////////////

  class SequenceOccurence final {
    public:
      SequenceOccurence(SequenceId _id, SequenceOccurenceIndex _index) : id(_id), index(_index) {}
      SequenceOccurence() : id(SequenceId::invalid()), index(SequenceOccurenceIndex::invalid()) {}
    public:
      SequenceId id;
      SequenceOccurenceIndex index;
  };

  [[nodiscard]] inline auto operator<(const SequenceOccurence& lhs, const SequenceOccurence& rhs) -> bool {
    if (lhs.id == rhs.id)
      return lhs.index < rhs.index;
    return lhs.id < rhs.id;
  }

  [[nodiscard]] inline auto operator==(const SequenceOccurence& lhs, const SequenceOccurence& rhs) -> bool {
    return lhs.id == rhs.id && lhs.index == rhs.index;
  }

  // Occurences /////////////////////////////////////////////////

  class SequenceOccurences final {
    public:
      SequenceOccurences() = default;
      SequenceOccurences(SequenceOccurences&&) = default;
      auto operator=(SequenceOccurences&&) -> SequenceOccurences& = default;

    public:
      auto remove(core::span<const SequenceOccurence>) -> void;
      auto remove(SequenceId, core::span<const SequenceOccurenceIndex>) -> void;
      auto insert(core::span<const SequenceOccurence>) -> void;
      auto insert(SequenceId, core::span<const SequenceOccurenceIndex>) -> void;
      auto replace(SequenceId,
                   core::span<const SequenceOccurenceIndex>,
                   SequenceId,
                   core::span<const SequenceOccurenceIndex>) -> void;
      auto replace(core::span<const SequenceOccurence>, SequenceId, core::span<const SequenceOccurenceIndex>) -> void;

    public:
      [[nodiscard]] auto find(SequenceId, SequenceOccurenceIndex) const -> const SequenceOccurence*;
      [[nodiscard]] auto find(SequenceOccurence) const -> const SequenceOccurence*;

      [[nodiscard]] auto contains(SequenceId, core::span<const SequenceOccurenceIndex>) const -> bool;
      [[nodiscard]] auto contains(core::span<const SequenceOccurence>) const -> bool;
      [[nodiscard]] auto doesntContain(SequenceId, core::span<const SequenceOccurenceIndex>) const -> bool;
      [[nodiscard]] auto doesntContain(core::span<const SequenceOccurence>) const -> bool;

      [[nodiscard]] auto size() const { return _data.size(); }
      [[nodiscard]] auto empty() const { return _data.size() == 0u; }

      [[nodiscard]] auto begin() const { return _data.begin(); }
      [[nodiscard]] auto end() const { return _data.end(); }
      [[nodiscard]] auto span() const { return _data.span(); }
      [[nodiscard]] operator core::span<const SequenceOccurence>() const { return span(); }

      auto shrink_to_fit() -> void { _data.shrink_to_fit(); }

      auto swap(SequenceOccurences& o) {
        _data.swap(o._data);
      }
    private:
      core::UninitializedVector<SequenceOccurence, 2> _data;
  };

  inline auto swap(SequenceOccurences& lhs, SequenceOccurences& rhs) { lhs.swap(rhs); }

  // GoupSons //////////////////////////////////////////////////////

  class GroupSons final {
    public:
      auto begin() const { return _data.begin(); }
      auto end() const { return _data.begin(); }
      decltype(auto) size() const { return _data.size(); }

      auto push_back(EventDataId id) { _data.push_back(id); }
      decltype(auto) operator[](size_t index) const {
        Assert(index < size(), "{} < {}", index, size());
        return _data[index];
      }

      auto sortOnContainer(GenericTraceData&) -> void;

    private:
      core::Vector<EventDataId> _data;
  };

  // SequenceSons //////////////////////////////////////////////////

  auto removeMarkedSons(TraceStructure&, SequenceId) -> void;

  class SequenceSons final {
      using Container = core::Vector<SequenceId, 2>;

      friend auto removeMarkedSons(TraceStructure&, SequenceId) -> void;

    public:
      [[nodiscard]] auto begin() const { return _data.begin(); }
      [[nodiscard]] auto end() const { return _data.end(); }

    public:
      [[nodiscard]] decltype(auto) size() const { return _data.size(); }
      [[nodiscard]] decltype(auto) empty() const { return _data.empty(); }
      auto reserve(size_t size) { _data.reserve(size); }

      template <typename...T> auto push_back(SequenceId id) { _data.push_back((id)); }

    public:
      [[nodiscard]] decltype (auto) front() const { return _data.front(); }
      [[nodiscard]] decltype (auto) back() const { return _data.back(); }
      [[nodiscard]] auto operator[](SequenceOccurenceIndex id) const -> SequenceId {
        Assert(id.value() < _data.size(), "{} < {}", id, _data.size());
        return _data[id.value()];
      }

    public:
      auto replace(core::span<const SequenceOccurenceIndex> indexes, SequenceId id) -> void;
      auto replace(core::span<const SequenceOccurence> occurences, SequenceId id) -> void;
      auto markForRemove(core::span<const SequenceOccurenceIndex> indexes) -> void;
      auto markForRemove(core::span<const SequenceOccurence> occurences) -> void;
      auto removeMarked(TraceStructure&);

      [[nodiscard]] auto span() const { return _data.span(); }

    private:
      Container _data;
  };

  // Sequence //////////////////////////////////////////////////////


  struct Sequence final {
    public:
      enum class Type { Container, Group, Loop, Node, UNKNOWN };

    public:
      struct Container final {
        Container(EventDataId id) : createEvent(id), sons() {}
        EventDataId createEvent;
        SequenceSons sons;
      };
      struct Group final { GroupSons sons; };
      struct Loop final { SequenceId event; SequenceId::underlying_t count; };
      struct Node final { SequenceSons sons; };
      using Data = std::variant<Container, Group, Loop, Node>; // HAS to be ordered as Type enum

      using Storage = core::StructOfArray<nlc::meta::list<nlc::meta::pair<Sequence::Data, Sequence::Data>,
                                                          nlc::meta::pair<SequenceOccurences,  SequenceOccurences>>,
                                          SequenceId>;
      using View = Storage::view;
      using ConstView = Storage::const_view;

      [[nodiscard]] static auto get_type(const Data& data) { return static_cast<Type>(data.index()); }
      [[nodiscard]] static auto get_sons(const Data& data) -> const SequenceSons& {
        [[maybe_unused]] const auto type = get_type(data);
        Assert(type == Type::Container || type == Type::Node, "{}", type);
        if (type == Type::Container)
          return std::get<Container>(data).sons;
        return std::get<Node>(data).sons;
      }
      [[nodiscard]] static auto get_sons(Data& data) -> SequenceSons& {
        [[maybe_unused]] const auto type = get_type(data);
        Assert(type == Type::Container || type == Type::Node, "{}", type);
        if (type == Type::Container)
          return std::get<Container>(data).sons;
        return std::get<Node>(data).sons;
      }
  };


// ---------------------------------------------------------------

#ifdef TA_DEBUG
  auto checkSons(const TraceStructure& trace, SequenceId id) -> void;
  auto checkParents(const TraceStructure& trace, SequenceId id) -> void;
#endif

} // namespace ta::trace

// ---------------------------------------------------------------

template <> struct fmt::formatter<ta::trace::Sequence::Type> {
  using Type = ta::trace::Sequence::Type;

  template <typename ParseContext> constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

  template <typename FormatContext> auto format(const Type& type, FormatContext& ctx) {
    switch (type) {
      case Type::Container: return format_to(ctx.out(), "Container");
      case Type::Group: return format_to(ctx.out(), "Group");
      case Type::Loop: return format_to(ctx.out(), "Loop");
      case Type::Node: return format_to(ctx.out(), "Node");
      case Type::UNKNOWN: return format_to(ctx.out(), "UNKNOWN");
    }
    Assert(false, "");
    return format_to(ctx.out(), "LOG ERROR");
  }
};

template <> struct fmt::formatter<ta::trace::SequenceOccurence> : formatter<string_view> {
  template <typename FormatContext> auto format(const ta::trace::SequenceOccurence& o, FormatContext& ctx) {
    return formatter<string_view>::format(string_view(fmt::format("{}[{}]", o.id, o.index)), ctx);
  }
};
