#pragma once

#include "Core/filesystem.hpp"
#include "Core/string.hpp"
#include "nlc/meta/units.hpp"
#include "Trace/trace.hpp"

#include <map>

namespace ta::trace {

  using Bytes = nlc::meta::bytes<unsigned long int>;

  struct TraceStorageInfo {
    struct Structure final {
      TraceDataId dataId;
      size_t nodeCount;
      core::String name;
      Bytes memory = Bytes { 0u };
      TraceState state;
    };

    struct Data final {
      size_t eventCount;
      core::Path path;
      core::String format;
      Bytes memory = Bytes { 0u };
      core::Vector<Structure> structures;
    };

    core::Vector<Data> traces;
    size_t structuresCount;
    Bytes memory = Bytes { 0u };
  };

  class TraceStorage final {
    public:
      TraceStorage() = default;
      TraceStorage(TraceStorage&&) = delete;
      TraceStorage(const TraceStorage&) = delete;
      auto operator=(TraceStorage&&) -> TraceStorage = delete;
      auto operator=(const TraceStorage&) -> TraceStorage = delete;

    public:
      template <typename TraceData_t>
      [[nodiscard]] auto create(const core::Path& path,
                                core::String name) -> TraceStructureId {
        const auto dataId = _data.emplace_back(core::make_unique<TraceData_t>());
        const auto structureId = _structures.emplace_back();

        auto& data = _data[dataId];
        data->structureIds().emplace_back(structureId);
        data->setPath(path);

        auto& structure = _structures[structureId];
        structure.dataId = dataId;
        structure.rootId = SequenceId::invalid();
        structure.name = name;

        Assert(structureId.isValid(), "");
        Assert(structureId.value() < _structures.size(), "");
        return structureId;
      }
      [[nodiscard]] auto getId(core::StringView name) const -> TraceStructureId;
      [[nodiscard]] auto rename(core::StringView oldName, core::StringView newName) -> bool;
      auto duplicate(TraceStructureId id, core::StringView newName) -> TraceStructureId;
      auto duplicate(core::StringView name, core::StringView newName) -> TraceStructureId;

    public:
      [[nodiscard]] auto structure(TraceStructureId id) -> TraceStructure& { return _structures[id]; }
      [[nodiscard]] auto structure(TraceStructureId id) const -> const TraceStructure& { return _structures[id]; }
      template <typename TraceData_t = nlc::meta::Default> [[nodiscard]] auto data(TraceDataId id) const {
        if constexpr (nlc::meta::is_same<TraceData_t, nlc::meta::Default>) {
          return _data[id].get();
        }
        else {
          const auto pData = static_cast<const TraceData_t *>(_data[id].get());
          Assert(pData == dynamic_cast<const TraceData_t *>(_data[id].get()), "");
          return pData;
        }
      }
      template <typename TraceData_t = nlc::meta::Default> [[nodiscard]] auto data(TraceDataId id) {
        if constexpr (nlc::meta::is_same<TraceData_t, nlc::meta::Default>) {
          return _data[id].get();
        }
        else {
          const auto pData = static_cast<TraceData_t *>(_data[id].get());
          Assert(pData == dynamic_cast<TraceData_t *>(_data[id].get()), "");
          return pData;
        }
      }

    public:
      [[nodiscard]] auto getInfos(TraceStructureId) const -> TraceStorageInfo::Structure;
      [[nodiscard]] auto getInfos(TraceDataId) const -> TraceStorageInfo::Data;
      [[nodiscard]] auto getInfos() const -> TraceStorageInfo;

    private:
      // structure
      core::Vector<TraceStructure, 0u, TraceStructureId> _structures;

      // data
      core::Vector<core::unique_ptr<GenericTraceData>, 0u, TraceDataId> _data;
  };
}
