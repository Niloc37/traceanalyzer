#pragma once

#include "Trace/traceStorage.hpp"

namespace ta::trace {

  struct Step {
    trace::SequenceId id;
    trace::SequenceOccurenceIndex index;
  };

  class Path final {
    public:
      decltype(auto) operator[](core::size_t index) const { return _data.operator[](index); }
      decltype(auto) operator[](core::size_t index) { return _data.operator[](index); }
      decltype(auto) size() const { return _data.size(); }
      decltype(auto) back() const { return _data.back(); }
      decltype(auto) begin() const { return _data.begin(); }
      decltype(auto) end() const { return _data.end(); }
      decltype(auto) span() const { return _data.span(); }

    public:
      auto push(trace::SequenceId id, unsigned int index) -> void;
      auto pop() -> void;
      auto copy() const -> Path;

    private:
      core::Vector<Step> _data;
  };

  auto getOccurence(const trace::TraceStructure& structure, const Path& path) -> core::size_t;
  auto countAllOccurences(const trace::TraceStructure& structure, trace::SequenceId id) -> core::size_t;
  auto firstEvent(const trace::TraceStructure& structure, const Path& path) -> EventDataId;
  auto lastEvent(const trace::TraceStructure& structure, const Path& path) -> EventDataId;

}

template <> struct fmt::formatter<ta::trace::Step> : formatter<string_view> {
  template <typename FormatContext> auto format(const ta::trace::Step& s, FormatContext& ctx) {
    return formatter<string_view>::format(fmt::format("({}, {})", s.id, s.index), ctx);
  }
};
