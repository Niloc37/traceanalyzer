#pragma once

#include "sequence.hpp"
#include "trace.hpp"

namespace ta::trace {
  auto naiveTreeFactorization(TraceStructure& trace) -> void;
  auto trahayTreeFactorisation(TraceStructure& trace) -> void;
} // namespace ta::trace
