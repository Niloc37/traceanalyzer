#pragma once

#include "Core/debug.hpp"
#include "Core/output.hpp"
#include "Core/uniquePointer.hpp"
#include "Trace/sequence.hpp"

namespace ta::trace {

  //-------------------------------------------

  struct TraceDataId_desr final {
    using underlying_t = unsigned int;
    static constexpr underlying_t invalid_value = static_cast<underlying_t>(-1);
    static constexpr underlying_t default_value = invalid_value;
  };
  using TraceDataId = nlc::meta::id<TraceDataId_desr>;

  //-------------------------------------------

  struct TraceStructureId_descr final {
    using underlying_t = unsigned int;
    static constexpr underlying_t invalid_value = static_cast<underlying_t>(-1);
    static constexpr underlying_t default_value = invalid_value;
  };
  using TraceStructureId = nlc::meta::id<TraceStructureId_descr>;

  //-------------------------------------------

  enum class TraceState : core::uint8 {
      NOTHING         = 0b0000'0000,
      NotLoaded       = 0b0000'0001,
      Ready           = 0b0000'0010,
      Modified        = 0b0000'0100,
      ReadyOrModified = 0b0000'0110,
      All             = 0b1111'1111,
  };
  constexpr inline auto operator|(TraceState lhs, TraceState rhs) { return TraceState(static_cast<core::uint8>(lhs) | static_cast<core::uint8>(rhs)); }
  constexpr inline auto operator&(TraceState lhs, TraceState rhs) { return TraceState(static_cast<core::uint8>(lhs) & static_cast<core::uint8>(rhs)); }

  //-------------------------------------------

  class GenericTraceData {
    protected:
      explicit GenericTraceData(const core::Path& path) : _path(path) {}

    public:
      virtual ~GenericTraceData() = default;

    public:
      virtual auto print(core::Output&, EventDataId) const -> void = 0;
      [[nodiscard]] virtual auto size() const -> size_t = 0;
      [[nodiscard]] virtual auto memory() const -> nlc::meta::bytes<unsigned long> {
        return _structureIds.memory() + _path.memory();
      }

    public:
      virtual auto timeStamp(trace::EventDataId) const -> nlc::meta::seconds<double> = 0;
      virtual auto container(trace::EventDataId id) const -> trace::SequenceId = 0;

    public:
      auto setPath(const core::Path& path) { _path = path; }
      [[nodiscard]] auto path() const -> const core::Path& { return _path; }
      [[nodiscard]] auto structureIds() -> core::Vector<TraceStructureId>& { return _structureIds; }
      [[nodiscard]] auto structureIds() const { return _structureIds.span(); }

      core::String format;

    private:
      core::Vector<TraceStructureId> _structureIds;
      core::Path _path;
  };

  template <typename EventData_t> class TraceData : public GenericTraceData {
    public:
      using EventData = EventData_t;

    public:
      explicit TraceData(const core::Path& path) : GenericTraceData(path) {}

    public:
      [[nodiscard]] auto size() const -> size_t override { return _raw.size(); }
      [[nodiscard]] auto memory() const -> nlc::meta::bytes<unsigned long> override {
        return GenericTraceData::memory() + _raw.memory() + _timeStamps.memory();
      }

    public:
      [[nodiscard]] decltype(auto) operator[](trace::EventDataId id) const { return _raw[id]; }
      [[nodiscard]] auto timeStamp(trace::EventDataId id) const -> nlc::meta::seconds<double> override { return _timeStamps[id]; }
      [[nodiscard]] auto container(trace::EventDataId id) const -> trace::SequenceId override { return _containers[id]; }

    public:
      auto emplace_back(trace::SequenceId container, EventData&& data) -> EventDataId {
        [[maybe_unused]] const auto timeStampId = _timeStamps.emplace_back(data.timeStamp());
        [[maybe_unused]] const auto containerId = _containers.emplace_back(container);
        const auto dataId = _raw.emplace_back(nlc::move(data));
        Assert(timeStampId == dataId, "{} == {}", timeStampId, dataId);
        Assert(containerId == dataId, "{} == {}", containerId, dataId);
        return dataId;
      }

    private:
      core::Vector<EventData, 0u, EventDataId> _raw;
      core::Vector<nlc::meta::seconds<double>, 0u, EventDataId> _timeStamps;
      core::Vector<trace::SequenceId, 0u, EventDataId> _containers;
  };

  //-------------------------------------------

  enum class IsHighlighted {};
  enum class IsFolded {};

  using TraceMetaData = core::StructOfArray<nlc::meta::list<nlc::meta::pair<IsHighlighted, bool>,
                                                            nlc::meta::pair<IsFolded, bool>>,
                                            SequenceId>;

  //-------------------------------------------

  class TraceStructure final {
    public:
      template <typename...T>
      decltype(auto) emplace_back(T...args) {
        _metaData.emplace_back();
        return _sequences.emplace_back(nlc::forward<T>(args)...);
      }

      template <typename Key> decltype(auto) get() {
        return _sequences.get<Key>();
      }
      template <typename Key> decltype(auto) get() const {
        return _sequences.get<Key>();
      }

      template <typename Key> decltype(auto) get(SequenceId id) {
        return _sequences.get<Key>(id);
      }
      template <typename Key> decltype(auto) get(SequenceId id) const {
        return _sequences.get<Key>(id);
      }

      decltype(auto) view(SequenceId id) const { return _sequences[id]; }
      decltype(auto) view(SequenceId id) { return _sequences[id]; }

      decltype(auto) size() const { return _sequences.size(); }
      decltype(auto) memory() const { return _sequences.memory(); }
      decltype(auto) metaData() { return (_metaData); }
      decltype(auto) metaData() const { return (_metaData); }

    public:
      TraceDataId dataId = TraceDataId::invalid();
      SequenceId rootId = SequenceId::invalid();
      core::String name;
      TraceState state = TraceState::NotLoaded;

    private:
      Sequence::Storage _sequences;
      TraceMetaData _metaData;
  };

  //-------------------------------------------

} // namespace ta::trace

template <> struct fmt::formatter<ta::trace::TraceState> : public formatter<string_view> {                 \
  template <typename FormatContext> auto format(const ta::trace::TraceState& value, FormatContext& ctx) {  \
    switch (value) {
      case ta::trace::TraceState::NotLoaded: return formatter<string_view>::format("NotLoaded", ctx);
      case ta::trace::TraceState::Ready: return formatter<string_view>::format("Ready", ctx);
      case ta::trace::TraceState::Modified: return formatter<string_view>::format("Modified", ctx);
      default: return formatter<string_view>::format("OTHER", ctx);
    }
  }                                                                                                 \
};
