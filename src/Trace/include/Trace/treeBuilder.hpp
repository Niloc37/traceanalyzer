#pragma once

#include "Trace/sequence.hpp"
#include "Trace/traceStorage.hpp"
#include "Core/string.hpp"

#include <map>
#include <algorithm>

namespace ta::trace {

  auto addSequenceSon(TraceStructure& storage, SequenceId fatherId, SequenceId sonId) -> void;
  auto makeGroup(TraceStructure& storage, SequenceId eventId) -> SequenceId;

  template <typename TraceData_t> class TraceBuilder final {
      using EventData = typename TraceData_t::EventData;

    private:
      struct EventDataProxy final {
        public:
          using underlying_t = EventData;
        public:
          EventDataProxy(const TraceStructure& structure, const TraceData<EventData>& data, EventDataId dataId)
              : _structure { structure }, _data { data }, _dataId { dataId } {
          }
        public:
          auto data() const -> const EventData& { return _data[_dataId]; }
          auto operator<(const EventDataProxy& rhs) const { return data() < rhs.data(); }
        private:
          const TraceStructure& _structure;
          const TraceData<EventData>& _data;
          const EventDataId _dataId;
      };

    public:
      auto addContainer(EventData&& event, SequenceId parentId) -> SequenceId {
        auto& structure = _storage.structure(_structureId);
        const auto pData = _storage.data<TraceData_t>(_dataId);

        const auto dataId = pData->emplace_back(parentId, nlc::move(event));
        const auto containerId = structure.emplace_back(Sequence::Container { dataId });

        if (parentId.isInvalid()) {
          Assert(structure.rootId.isInvalid(), "");
          structure.rootId = containerId;
        }
        else
          addSequenceSon(structure, parentId, containerId);

        return containerId;
      }

    public:
      auto addEvent(EventData&& event, SequenceId containerId) {
        const auto pData = _storage.data<TraceData_t>(_dataId);
        auto& structure = _storage.structure(_structureId);

        const auto dataId = pData->emplace_back(containerId, nlc::move(event));
        const auto eventDataProxy = EventDataProxy { structure, *pData, dataId };

        // Search if equivalent leaf exists
        auto [it, inserted] = _equivClasses.try_emplace(eventDataProxy, SequenceId::invalid());

        // if no equivalent leaf was existing, create new group
        if (inserted) // Il n'y avait pas d'autre élément du même type
          it->second = structure.emplace_back(Sequence::Group {});

        // add the new leaf to the group
        Assert(Sequence::get_type(structure.template get<Sequence::Data>(it->second)) == Sequence::Type::Group, "");
        std::get<Sequence::Group>(structure.template get<Sequence::Data>(it->second)).sons.push_back(dataId);
        addSequenceSon(structure, containerId, it->second);
      }

      //-------------------------------------------

    public:
      auto structureId() const { return _structureId; }
      auto dataId() const { return _dataId; }
      auto rootId() const { return _storage.structure(_structureId).rootId; }

      //-------------------------------------------

      TraceBuilder(TraceStorage& storage, const core::Path& path, core::StringView name)
          : _storage { storage } {
        _structureId = _storage.create<TraceData_t>(path, name);
        _dataId = _storage.structure(_structureId).dataId;
      }

      ~TraceBuilder() {
        auto& structure = _storage.structure(_structureId);
        auto& data = *_storage.data(structure.dataId);
        auto& sequences = structure.template get<Sequence::Data>();
        for (auto& sequence : sequences) {
          if (Sequence::get_type(sequence) == trace::Sequence::Type::Group) {
            std::get<Sequence::Group>(sequence).sons.sortOnContainer(data);
          }
        }

      }

      //-------------------------------------------

      decltype(auto) storage() const { return (_storage); }

      //-------------------------------------------

    private:
      TraceStorage& _storage;
      TraceDataId _dataId;
      TraceStructureId _structureId;
      std::map<EventDataProxy, SequenceId> _equivClasses;
  };
} // namespace ta::trace
