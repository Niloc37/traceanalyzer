#include "Trace/sequence.hpp"

#include "Trace/traceStorage.hpp"

namespace ta::trace {

  // ---------------------------------------------------------------

#ifdef TA_DEBUG
  auto checkSons(const TraceStructure& trace, SequenceId id) -> void {
    const auto& event = trace.get<Sequence::Data>(id);
    const auto type = Sequence::get_type(event);
    switch (type) {
      case Sequence::Type::Node:
        [[fallthrough]];
      case Sequence::Type::Container: {
        const auto& parentSons = Sequence::get_sons(trace.get<Sequence::Data>(id));
        for (const auto i : core::Range(0u, parentSons.size())) {
          const auto index = SequenceOccurenceIndex(i);
          const auto sonId = parentSons[index];
          Assert(sonId.isInvalid() || trace.get<SequenceOccurences>(sonId).contains(id, index),
                 "{}[{}] ({}) -- {}",
                 id,
                 i,
                 sonId,
                 trace.get<SequenceOccurences>(sonId).span());
        }
      }
        break;
      case Sequence::Type::Group:
        break;
      case Sequence::Type::Loop: {
        const auto& loop = std::get<Sequence::Loop>(event);
        Assert(loop.count >= 2, "");
        Assert(loop.event.isValid(), "");
      }
        break;
      case Sequence::Type::UNKNOWN:
        AssertNotReached("");
        break;
    }
  }

  // ---------------------------------------------------------------

  auto checkParents(const TraceStructure& trace, SequenceId id) -> void {
    for (const auto& occurence : trace.get<SequenceOccurences>(id)) {
      const auto& seq = trace.get<Sequence::Data>(occurence.id);
      switch (Sequence::get_type(seq)) {
        case Sequence::Type::Container:
          [[fallthrough]];
        case Sequence::Type::Node:
          Assert(Sequence::get_sons(seq)[occurence.index] == id, "");
          break;
        case Sequence::Type::Group:
          AssertNotReached("Father of {} can't be a group", id);
          break;
        case Sequence::Type::Loop:
          Assert(std::get<Sequence::Loop>(seq).event == id,
                 "{}[{}] is parent of {} but as value of {}",
                 occurence.id,
                 occurence.index,
                 id,
                 std::get<Sequence::Loop>(seq).event);
          break;
        case Sequence::Type::UNKNOWN:
          AssertNotReached("");
      }
    }
  }
#endif

  // ---------------------------------------------------------------

  template <typename T> auto auxFind(core::span<T> data, const SequenceOccurence& occurence) {
    if (data.size() == 0)
      return data.begin();

    const auto p = data.begin() + (data.end() - data.begin()) / 2;
    if (*p < occurence)
      return auxFind(core::span(p + 1, data.end()), occurence);
    return auxFind(core::span(data.begin(), p), occurence);
  }

  // ---------------------------------------------------------------

  auto auxContains(core::span<const SequenceOccurence> data, const SequenceOccurence& occurence) {
    const auto it = auxFind(data, occurence);
    return it != data.end() && *it == occurence;
  }

  // ---------------------------------------------------------------

  template <typename T, typename F>
  auto auxRemove(core::span<SequenceOccurence> data, core::span<T> rem, F& get) -> size_t {
    if (rem.begin() == rem.end())
      return data.size();

    auto remIt = rem.begin();
    auto readIt = auxFind(data, get(remIt));
    auto writeIt = readIt;

    while (remIt != rem.end()) {
      Assert(readIt != data.end(), "rem must be a subset of data : \ndata : {}\nrem : {}", data, rem);
      if (*readIt < get(remIt)) {
        *writeIt = *readIt;
        ++writeIt;
        ++readIt;
      }
      else {
        Assert(get(remIt) == *readIt, "rem must be a subset of data : \ndata : {}\nrem : {}", data, rem);
        ++remIt;
        ++readIt;
      }
    }

    while (readIt != data.end()) {
      *writeIt = *readIt;
      ++writeIt;
      ++readIt;
    }

    return writeIt - data.begin();
  }

  // ---------------------------------------------------------------

  auto SequenceOccurences::remove(SequenceId id, core::span<const SequenceOccurenceIndex> indexes) -> void {
    auto get = [id](auto it) { return SequenceOccurence(id, *it); };
    const auto size = auxRemove(_data, indexes, get);
    _data.resize(size);
  }

  // ---------------------------------------------------------------

  auto SequenceOccurences::remove(core::span<const SequenceOccurence> occurences) -> void {
    auto get = [](auto it) -> const SequenceOccurence& { return *it; };
    const auto size = auxRemove(_data, occurences, get);
    _data.resize(size);
  }

  // ---------------------------------------------------------------

#if 0
  template <typename T, typename F> [[nodiscard]] auto auxInsert(core::span<SequenceOccurence> data,
                                                                 SequenceOccurence* const end,
                                                                 core::span<T> ins, F& get) -> size_t {
    if (ins.size() == 1) {
      *end = get(ins.begin());
      if (data.size() == 0u || data.back() < *end)
        return 1u;
    }

    auto w = data.begin();
    auto p = end;
    auto e = p;
    auto i = ins.begin();
    Assert(data.size() == (p - data.begin()) + ins.size(),
           "{} == {} + {}",
           data.size(),
           p - data.begin(),
           ins.size());

    // We assume :
    //  * [data.begin(), p[ and ins are sorted
    //  * [p, data.end()[ is free usable memory
    // At each iteration we assume and certify
    //  * [data.begin(), w[ and [p, e[ are sorted
    //  * all elements of [data.begin(), w[ are smaller than all elements of [p, e[ and [i, ins.end()[
    //  * w <= p
    while (p == e) {
      if (get(i) < *w) {
        *e = *w;
        *w = get(i);
        ++e; ++w; ++i;
      }
      else if (*w < get(i))
        ++w;
      else
        ++i;
    }

    while (i != end) {
      if (get(i) < *p) {
        *e = *w;
        *w = get(i);
        ++e; ++w; ++i;
      }
      else if (*p < get(i)) {
        *e = *w;
        *w = *p;
        ++e; ++w; ++p;
      }
      else
        ++i;
    }

    while (p != e) {
      *w = *p;
      ++w; ++p;
    }
  }

#else

  auto auxInsert(core::span<SequenceOccurence> data, SequenceOccurence *p) -> size_t {
    if (data.end() - p == 1) {
      if (data.size() == 1 || *(p - 1) < *p)
        return data.size();
      else if (*(p - 1) == *p)
        return data.size() - 1;
    }

    std::inplace_merge(data.begin(), p, data.end());
    auto w = data.begin();
    for (auto r = w + 1; w != data.end(); ++w, ++r) {
      if (r != data.end() && *w == *r) {
        for (++w, ++r; r != data.end(); ++r) {
          if (!(*r == *w)) {
            *w = *r;
            ++w;
          }
        }
        return w - data.begin();
      }
    }
    return w - data.begin();
  }

#endif

  auto SequenceOccurences::insert(core::span<const SequenceOccurence> occurences) -> void {
    const auto oldSize = _data.size();
    _data.resize(oldSize + occurences.size());

    const auto p = _data.begin() + oldSize;
    std::copy(occurences.begin(), occurences.end(), p);

    _data.resize(auxInsert(_data, p));
  }

  // ---------------------------------------------------------------

  auto SequenceOccurences::insert(SequenceId id, core::span<const SequenceOccurenceIndex> indexes) -> void {
    const auto oldSize = _data.size();
    _data.resize(oldSize + indexes.size());

    const auto p = _data.begin() + oldSize;
    for (const auto& index : core::Range(0u, indexes.size()))
      p[index] = SequenceOccurence(id, indexes[index]);

    _data.resize(auxInsert(_data, p));
  }

  // ---------------------------------------------------------------

  template <typename T1, typename F1, typename T2, typename F2>
  [[nodiscard]] auto replaceImpl(core::span<SequenceOccurence> write,
                                 core::span<const SequenceOccurence> read,
                                 core::span<const T1> rem,
                                 F1& getRem,
                                 core::span<const T2> ins,
                                 F2& getIns) -> size_t {
    auto writeIt = write.begin();
    auto readIt = read.begin();
    auto remIt = rem.begin();
    auto insIt = ins.begin();

    // Requisite :
    //  1) rem is a subset of read
    //  2) no elements are common between ins and rem
    // At each loop we assume :
    //  3) (readIt != read.end()) || (insIt != ins.end())
    //  4) readIt != read.end() => remIt == rem.end() || *readIt <= *remIt
    Assert(write.size() == read.size() + ins.size() - rem.size(),
           "{} == {} + {} - {}",
           write.size(),
           read.size(),
           ins.size(),
           rem.size());

    enum { READ, REM, INS, SKIP_INS };

    auto nextAction = [&]() {
      if (readIt == read.end()) {
        Assert(remIt == rem.end(), "rem must be a subset of read"); // (1)
        Assert(insIt != ins.end(), "Broken loop invariant"); // (4)
        return INS;
      }

      if (remIt == rem.end()) {
        if (insIt == ins.end() || *readIt < getIns(insIt))
          return READ;
        if (getIns(insIt) < *readIt)
          return SKIP_INS;
        return INS;
      }

      if (insIt != ins.end() && getIns(insIt) < *readIt)
        return INS;

      Assert(!(getRem(remIt) < *readIt), "rem must be a subset of read");
      if (*readIt < getRem(remIt))
        return READ;
      return REM;
    };


    while (readIt != read.end() || insIt != ins.end()) {
      switch (nextAction()) {
        case READ:
          *writeIt = *readIt;
          ++writeIt;
          ++readIt;
          break;
        case REM:
          ++remIt;
          ++readIt;
          break;
        case INS:
          *writeIt = getIns(insIt);
          ++writeIt;
          ++insIt;
          break;
        case SKIP_INS:
          ++insIt;
      }
    }

    return writeIt - write.begin();
  }

  // ---------------------------------------------------------------

  auto SequenceOccurences::replace(SequenceId remId,
                                   core::span<const SequenceOccurenceIndex> remIndexes,
                                   SequenceId insId,
                                   core::span<const SequenceOccurenceIndex> insIndexes) -> void {
    auto tmp = nlc::move(_data);
    _data.resize(tmp.size() + insIndexes.size() - remIndexes.size());
    auto getRem = [remId](auto it) { return SequenceOccurence(remId, *it); };
    auto getIns = [insId](auto it) { return SequenceOccurence(insId, *it); };
    const auto size = replaceImpl(_data, tmp, remIndexes, getRem, insIndexes, getIns);
    _data.resize(size);
  }

  auto SequenceOccurences::replace(core::span<const SequenceOccurence> rem,
                                   SequenceId insId,
                                   core::span<const SequenceOccurenceIndex> insIndexes) -> void {
    auto tmp = nlc::move(_data);
    _data.resize(tmp.size() + insIndexes.size() - rem.size());
    auto getRem = [](auto it) -> const SequenceOccurence& { return *it; };
    auto getIns = [insId](auto it) { return SequenceOccurence(insId, *it); };
    const auto size = replaceImpl(_data, tmp, rem, getRem, insIndexes, getIns);
    _data.resize(size);
  }

  // ---------------------------------------------------------------

  auto SequenceOccurences::find(SequenceId id, SequenceOccurenceIndex index) const -> const SequenceOccurence * {
    return find(SequenceOccurence(id, index));
  }

  auto SequenceOccurences::find(SequenceOccurence occurence) const -> const SequenceOccurence * {
    const auto it = auxFind(_data.span(), occurence);
    if (*it == occurence)
      return it;
    return end();
  }

  // ---------------------------------------------------------------

  auto SequenceOccurences::contains(SequenceId id, core::span<const SequenceOccurenceIndex> indexes) const -> bool {
    for (const auto index : indexes)
      if (!auxContains(_data, SequenceOccurence(id, index)))
        return false;
    return true;
  }

  // ---------------------------------------------------------------

  auto SequenceOccurences::contains(core::span<const SequenceOccurence> occurences) const -> bool {
    for (const auto& occurence : occurences)
      if (!auxContains(_data, occurence))
        return false;
    return true;
  }

  // ---------------------------------------------------------------

  auto
  SequenceOccurences::doesntContain(SequenceId id, core::span<const SequenceOccurenceIndex> indexes) const -> bool {
    for (const auto index : indexes)
      if (auxContains(_data, SequenceOccurence(id, index)))
        return false;
    return true;
  }

  // ---------------------------------------------------------------

  auto SequenceOccurences::doesntContain(core::span<const SequenceOccurence> occurences) const -> bool {
    for (const auto& occurence : occurences)
      if (auxContains(_data, occurence))
        return false;
    return true;
  }

  // GroupSons /////////////////////////////////////////////////////

  auto GroupSons::sortOnContainer(GenericTraceData& data) -> void {
    std::stable_sort(_data.begin(), _data.end(), [&data] (auto lhs, auto rhs) {
      return data.container(lhs) < data.container(rhs);
    });
  }

  // SequenceSons //////////////////////////////////////////////////

  auto SequenceSons::replace(core::span<const SequenceOccurenceIndex> indexes, SequenceId id) -> void {
    Assert(id.isValid(), "");
    for (const auto index : indexes)
      _data[index.value()] = id;
  }

  // ---------------------------------------------------------------

  auto SequenceSons::replace(core::span<const SequenceOccurence> occurences, SequenceId id) -> void {
    Assert(id.isValid(), "");
    for (const auto occurence : occurences)
      _data[occurence.index.value()] = id;
  }

  // ---------------------------------------------------------------

  auto SequenceSons::markForRemove(core::span<const SequenceOccurenceIndex> indexes) -> void {
    for (const auto index : indexes)
      _data[index.value()] = SequenceId::invalid();
  }

  // ---------------------------------------------------------------

  auto SequenceSons::markForRemove(core::span<const SequenceOccurence> occurences) -> void {
    for (const auto occurence : occurences)
      _data[occurence.index.value()] = SequenceId::invalid();
  }

  // ---------------------------------------------------------------

  auto removeMarkedSons(TraceStructure& trace, SequenceId id) -> void {
    auto& sons = Sequence::get_sons(trace.get<Sequence::Data>(id))._data;

    const auto begin = sons.begin();
    const auto end = sons.end();

    struct ModificationsOnSons {
      core::Vector<SequenceOccurenceIndex> remove;
      core::Vector<SequenceOccurenceIndex> insert;
    };
    auto sonSons = std::map<SequenceId, ModificationsOnSons> {};

    // search for first invalid
    auto nextToWrite = begin;
    while (nextToWrite != end && nextToWrite->isValid())
      ++nextToWrite;

    if (nextToWrite == end)
      return; // nothing to do

    // search next valid to copy
    auto nextToRead = nextToWrite + 1;

    while (true) {
      while (nextToRead != end && !nextToRead->isValid())
        ++nextToRead;

      if (nextToRead == end)
        break;

      auto& onSon = sonSons[*nextToRead];
      onSon.remove.push_back(SequenceOccurenceIndex(nextToRead - begin));
      onSon.insert.push_back(SequenceOccurenceIndex(nextToWrite - begin));

      *nextToWrite = *nextToRead;
      ++nextToRead;
      ++nextToWrite;
    }

#if 1
    for (auto w = nextToWrite; w != end; ++w)
      *w = SequenceId::invalid();
#endif

    for (const auto& modifications : sonSons) {
      auto& occurences = trace.get<SequenceOccurences>(modifications.first);
      occurences.remove(id, modifications.second.remove);
      occurences.insert(id, modifications.second.insert);
    }

    sons.resize(nextToWrite - begin);
  }

} // namespace ta::trace
