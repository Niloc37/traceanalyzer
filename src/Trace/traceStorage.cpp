#include "Trace/traceStorage.hpp"

namespace ta::trace {

  //---------------------------------------------------------------


  //---------------------------------------------------------------

  auto TraceStorage::getId(core::StringView name) const -> TraceStructureId {
    for (const auto& structure : _structures)
      if (structure.name == name)
        return _structures.indexOf(&structure);
    return TraceStructureId::invalid();
  }

  //---------------------------------------------------------------

  auto TraceStorage::duplicate(TraceStructureId id, core::StringView newName) -> TraceStructureId {
    /*
    if (id.isInvalid())
      return TraceStructureId::invalid();

    const auto structureId = _structures.emplace_back();

    auto& oldStructure = _structures[id];
    const auto dataId = oldStructure.dataId;

    _data[dataId]->structureIds().emplace_back(structureId);

    auto& structure = _structures[structureId];

    structure = oldStructure;
    structure.name = newName;
    return structureId;
    return structureId;
   */

    core::do_nothing(id, newName);
    LOG_ERROR("No longuer trace duplication for the moment");
    return TraceStructureId::invalid();
  }

  //---------------------------------------------------------------

  auto TraceStorage::duplicate(core::StringView name, core::StringView newName) -> TraceStructureId {
    const auto id = getId(name);
    return duplicate(id, newName);
  }

  //---------------------------------------------------------------

  auto TraceStorage::rename(core::StringView oldName, core::StringView newName) -> bool {
    const auto id = getId(oldName);
    if (id.isValid()) {
      auto& structure = _structures[id];
      structure.name = newName;
      return true;
    }
    return false;
  }

  //---------------------------------------------------------------

  auto TraceStorage::getInfos(TraceStructureId id) const -> TraceStorageInfo::Structure {
    auto res = TraceStorageInfo::Structure {};
    const auto& trace = structure(id);

    res.dataId = trace.dataId;
    res.nodeCount = trace.size();
    res.name = trace.name;
    res.memory = trace.memory();
    res.state = trace.state;

    return res;
  }

  //---------------------------------------------------------------

  auto TraceStorage::getInfos(TraceDataId id) const -> TraceStorageInfo::Data {
    auto res = TraceStorageInfo::Data {};
    const auto& trace = data(id);

    res.eventCount = trace->size();
    res.memory = trace->memory();
    res.path = trace->path();
    res.format = trace->format;

    for (const auto& structureId : trace->structureIds()) {
      res.structures.emplace_back(getInfos(structureId));
      res.memory += res.structures.back().memory;
    }

    return res;
  }

  //---------------------------------------------------------------

  auto TraceStorage::getInfos() const -> TraceStorageInfo {
    auto res = TraceStorageInfo {};

    res.structuresCount = 0;
    res.memory = _structures.memory() + _data.memory();

    for (const auto& dataId : _data.indexes()) {
      res.traces.emplace_back(getInfos(dataId));
      res.structuresCount += res.traces.back().structures.size();
      res.memory += res.traces.back().memory;
    }

    return res;
  }

  //---------------------------------------------------------------

}
