#include "Trace/trahay.hpp"

#include <unordered_map>
#include <unordered_set>

//---------------------------------------------------

namespace std {
  template <typename Descr, typename Self> struct hash<nlc::meta::id<Descr, Self>> {
    using argument_t = nlc::meta::id<Descr, Self>;
    auto operator()(const argument_t& id) const noexcept {
      return std::hash<typename argument_t::underlying_t> {}(id.value());
    }
  };

  static_assert(sizeof(ta::trace::SequenceId) == 4);
  template <> struct hash<std::pair<ta::trace::SequenceId, size_t>> {
    using argument_t = std::pair<ta::trace::SequenceId, size_t>;
    auto operator()(const argument_t& p) const noexcept {
      auto x = (static_cast<size_t>(p.first.value()) << 32) ^ static_cast<size_t>(p.second);
      x ^= x >> 30;
      x *= 0xbf58476d1ce4e5b9;
      x ^= x >> 27;
      x *= 0x94d049bb133111eb;
      x ^= x >> 31;
      return x;
    }
  };

  static_assert(sizeof(ta::trace::SequenceId) == 4);
  template <> struct hash<std::pair<ta::trace::SequenceId, ta::trace::SequenceId>> {
    using argument_t = std::pair<ta::trace::SequenceId, ta::trace::SequenceId>;
    auto operator()(const argument_t& p) const noexcept {
      auto x = (static_cast<size_t>(p.first.value()) << 32) ^ static_cast<size_t>(p.second.value());
      x ^= x >> 30;
      x *= 0xbf58476d1ce4e5b9;
      x ^= x >> 27;
      x *= 0x94d049bb133111eb;
      x ^= x >> 31;
      return x;
    }
  };
}

//---------------------------------------------------

namespace ta::trace {

  //---------------------------------------------------

  using underlying_t = SequenceId::underlying_t;

  enum RemoveMarkedSonPolicy { REMOVE_MARKED_SONS, DONT_REMOVE_MARKED_SONS };

  //---------------------------------------------------

  class Progression {
    public:
      using clock = std::chrono::high_resolution_clock;
      using time_point = std::chrono::time_point<clock>;
      using duration = std::chrono::duration<double, std::ratio<1, 1>>;
    public:
      Progression(core::StringView prefix, double max, double interval = 1., double begin = -1.)
          : _prefix(prefix), _max(max), _current(0.), _interval(interval), _next(begin < 0. ? interval : begin), _start(
          clock::now()) {
      }
      auto notify() {
        ++_current;
        const auto d = std::chrono::duration_cast<duration>(clock::now() - _start).count();

        if (d > _next) {
          _next += _interval;
          LOG_DEBUG("{}{} / {} : {}%", _prefix, _current, _max, 100. * _current / _max);
        }
      }

    private:
      core::String _prefix;
      const double _max;
      double _current;
      const double _interval;
      double _next;
      const time_point _start;
  };

  //---------------------------------------------------

  namespace {

    //---------------------------------------------------

    struct Cache final {
      Cache() = default;
      Cache(Cache&&) = delete;
      Cache(const Cache&) = delete;
      std::unordered_map<SequenceId, std::unordered_map<underlying_t, SequenceId>> loops;
      core::Vector<SequenceId> leafContainers;
    };

    //---------------------------------------------------

    auto getLoop(Cache& cache, TraceStructure& storage, SequenceId id, underlying_t count) -> SequenceId {
      auto& loopId = cache.loops[id][count];

      if (loopId.isInvalid())
        loopId = storage.emplace_back(Sequence::Loop { id, count });

      LOG_DEBUG("New loop of {} times {} -> {}", count, id, loopId);

      return loopId;
    }

    //---------------------------------------------------

    struct PairOccurences {
      // std::pair<SequenceId, SequenceId> ids;
      core::Vector<SequenceOccurenceIndex, 4> occurences1;
      core::Vector<SequenceOccurenceIndex, 4> occurences2;
    };

    auto searchMoreFrequentDuoInContainer(const TraceStructure& trace,
                                          SequenceId containerId) -> std::unordered_map<std::pair<SequenceId, SequenceId>, PairOccurences> {
      auto pairs = std::unordered_map<std::pair<SequenceId, SequenceId>, PairOccurences> {};

      const auto& containerSons = Sequence::get_sons(trace.get<Sequence::Data>(containerId));
      const auto begin = containerSons.begin();
      const auto end = containerSons.end();

      auto first = containerSons.begin();
      Assert(first->isValid(), "");

      while (true) {
        auto second = first + 1;
        if (second == end)
          break;

        Assert(second->isValid(), "");
        Assert(first != second, "({}, {}) / {}", first - begin, second - begin, containerSons.size());

        if (*first != *second) {
          Assert(first->isValid() && second->isValid(), "");

          auto& occurences = pairs[{ *first, *second }];
          // occurences.ids = { *first, *second };
          occurences.occurences1.emplace_back(SequenceOccurenceIndex(first - begin));
          occurences.occurences2.emplace_back(SequenceOccurenceIndex(second - begin));
        }

        first = second;
      }

      return pairs;
    }

    //---------------------------------------------------

    struct PairOccurencesList {
      std::pair<SequenceId, SequenceId> ids;
      core::Vector<std::pair<SequenceId, PairOccurences>> containers;
      size_t count;
      PairOccurencesList(std::pair<SequenceId, SequenceId> _ids,
                         core::Vector<std::pair<SequenceId, PairOccurences>>&& _containers,
                         size_t _count)
          : ids(_ids)
          , containers(nlc::move(_containers))
          , count(_count) {
      }
    };

    auto searchMoreFrequentDuo(TraceStructure& trace, const Cache& cache) -> core::Vector<PairOccurencesList> {
      auto pairs = std::unordered_map<std::pair<SequenceId, SequenceId>, core::Vector<std::pair<SequenceId, PairOccurences>>> {};

      // merge all data
      for (const auto containerId : cache.leafContainers) {
        for (auto& temp : searchMoreFrequentDuoInContainer(trace, containerId)) {
          pairs[temp.first].emplace_back(containerId, nlc::move(temp.second));
        }
      }

      // Count
      core::Vector<std::pair<size_t, decltype(pairs)::iterator>> counters;
      for (auto it = pairs.begin(); it != pairs.end(); ++it) {
        size_t count = 0u;
        for (const auto& occurences : it->second)
          count += occurences.second.occurences1.size();
        counters.emplace_back(count, it);
      }

      // sort
      std::sort(counters.begin(),
                counters.end(),
                [](const auto& lhs, const auto& rhs) { return lhs.first > rhs.first; });

      // Select more frenquents without conflits
      std::unordered_set<SequenceId> alreadySeenIds;
      core::Vector<PairOccurencesList> res;
      res.reserve(counters.size());

      for (auto& pair : counters) {
        if (!(alreadySeenIds.emplace(pair.second->first.first).second &&
              alreadySeenIds.emplace(pair.second->first.second).second) ||
            pair.first < 2)
          continue;

        res.emplace_back(pair.second->first, nlc::move(pair.second->second), pair.first);
      }

      return res;
    }

    //---------------------------------------------------

    template <RemoveMarkedSonPolicy removeMarkedSonPolicy>
    auto extendsNode(TraceStructure& storage, SequenceId eventId) -> bool {
      auto nextId = SequenceId::invalid();
      core::Vector<SequenceOccurence> nextIdOccurencesToDelete;

      // Searching next ids
      for (const auto&[fatherId, occurenceIndex] : storage.get<SequenceOccurences>(eventId)) {
        const auto& seq = storage.get<Sequence::Data>(fatherId);
        const auto type = Sequence::get_type(seq);
        if (type != Sequence::Type::Node && type != Sequence::Type::Container)
          continue;
        const auto& fatherSons = Sequence::get_sons(seq);

        auto nextIndex = occurenceIndex + 1;
        if (nextIndex.value() >= fatherSons.size())
          return false;

        const auto id = fatherSons[nextIndex];

        if (nextId.isInvalid())
          nextId = id;
        else if (id != nextId)
          return false;

        Assert(fatherSons[nextIndex] == nextId, "");

        nextIdOccurencesToDelete.emplace_back(fatherId, nextIndex);
      }

      if (nextId.isInvalid())
        return false;

      LOG_DEBUG("-- Extends {} with {}", eventId, nextId);

      auto& nextIdOccurences = storage.get<SequenceOccurences>(nextId);

      [[maybe_unused]]auto parents = std::unordered_set<SequenceId> {};
      [[maybe_unused]]auto lastParent = SequenceId::invalid();
      for (const auto& occurence : nextIdOccurencesToDelete) {
        if constexpr (removeMarkedSonPolicy == REMOVE_MARKED_SONS) {
          if (lastParent != occurence.id) {
            parents.insert(occurence.id);
            lastParent = occurence.id;
          }
        }
        // Erasing old links in fathers
        auto& fatherSons = Sequence::get_sons(storage.get<Sequence::Data>(occurence.id));
        Assert(fatherSons[occurence.index] == nextId, "{} == {}", fatherSons[occurence.index], nextId);
        fatherSons.markForRemove(occurence.index);
      }


      auto& eventSons = Sequence::get_sons(storage.get<Sequence::Data>(eventId));

      // Erasing old link in son and adding link to new node
      // Adding node to nextIdSequence occurences
      nextIdOccurences.replace(core::make_span(nextIdOccurencesToDelete), eventId, SequenceOccurenceIndex(eventSons.size()));

      // Adding nextId to node sons
      eventSons.push_back(nextId);

      if constexpr (removeMarkedSonPolicy == REMOVE_MARKED_SONS) {
        for (const auto parent : parents)
          removeMarkedSons(storage, parent);
      }
      return true;
    }

    //---------------------------------------------------

    template <RemoveMarkedSonPolicy removeMarkedSonPolicy>
    auto factoriseLoops(TraceStructure& storage, Cache& cache, SequenceId rootId) -> void {
      auto loops = std::unordered_map<std::pair<SequenceId, size_t>, core::Vector<SequenceOccurenceIndex>> {};

      // Search loops (Scoped in order to avoid reference invalidation
      {
        auto& rootSons = Sequence::get_sons(storage.get<Sequence::Data>(rootId));
        const auto end = rootSons.end();
        auto it = rootSons.begin();
        Assert(it != end, "");
        Assert(it->isValid(), "");

        auto id = *it;
        auto index = 0u;
        auto buffer = core::Vector<SequenceOccurenceIndex> {};

        while (true) {
          if (it == end || (it->isValid() && *it != id)) {
            const auto endIndex = it - rootSons.begin();

            const auto size = endIndex - index;
            if (size > 1) {
              buffer.clear();
              buffer.reserve(size);
              for (const auto i : core::Range(0u, size)) {
                auto occurenceIndex = SequenceOccurenceIndex(index + i);
                if (rootSons[occurenceIndex].isValid())
                  buffer.emplace_back(occurenceIndex);
              }

              const auto realSize = buffer.size();
              if (realSize > 1) {
                rootSons.markForRemove(buffer);
                storage.get<SequenceOccurences>(id).remove(rootId, buffer);

                loops[{ id, realSize }].push_back(buffer.front());
              }
            }

            if (it == end)
              break;

            id = *it;
            index = endIndex;
          }
          ++it;
        }
      }

      // Get or create loops (anticipated, avoid reference invalidation);
      for (const auto&[loop, occurences] : loops) {
        const auto loopId = getLoop(cache, storage, loop.first, loop.second); // This line can invalidate references

        storage.get<SequenceOccurences>(loop.first).insert(loopId, SequenceOccurenceIndex(0));

        for (const auto& occurenceIndex : occurences) {
          storage.get<SequenceOccurences>(loopId).insert(rootId, occurenceIndex);
          Sequence::get_sons(storage.get<Sequence::Data>(rootId)).replace(occurenceIndex, loopId);
        }
      }

      if constexpr (removeMarkedSonPolicy == REMOVE_MARKED_SONS) {
        if (!loops.empty())
          removeMarkedSons(storage, rootId);
      }
    }

    //---------------------------------------------------

    auto factoriseMoreFrequentDuo(TraceStructure& trace, Cache& cache) -> bool {
      const auto& pairs = searchMoreFrequentDuo(trace, cache);

      if (pairs.empty())
        return false;

      LOG_DEBUG(" ** {} frequent pairs found with between {} and {} occurences",
                pairs.size(), pairs.front().count, pairs.back().count);


#ifdef TA_PARANO
      for (const auto& pair : pairs) {
        checkParents(trace, pair.ids.first);
        checkParents(trace, pair.ids.second);
        for (const auto& container : pair.containers) {
          checkSons(trace, container.first);
          Assert(trace[pair.ids.first].occurences().contains(container.first, container.second.occurences1), "");
          Assert(trace[pair.ids.second].occurences().contains(container.first, container.second.occurences2), "");
        }
      }
#endif

      for (const auto& pair : pairs) {
        // Must be done first in order to avoid reference invalidation
        const auto newNodeId = trace.emplace_back(Sequence::Node {});
        decltype(auto) newNodeSons = Sequence::get_sons(trace.get<Sequence::Data>(newNodeId));

#ifdef TA_PARANO
        LOG_DEBUG("-- More frequent duo is {}-{} with {} occurences -> create node {}",
                  pair.ids.first,
                  pair.ids.second,
                  pair.count,
                  newNodeId);
#endif

        newNodeSons.push_back(pair.ids.first);
        newNodeSons.push_back(pair.ids.second);

        for (const auto& container : pair.containers) {
#ifdef TA_PARANO
          checkSons(trace, container.first);
#endif
          Assert(container.second.occurences1.size() == container.second.occurences2.size(), "");
          trace.get<SequenceOccurences>(newNodeId).insert(container.first, container.second.occurences1);

          auto& containerSons = Sequence::get_sons(trace.get<Sequence::Data>(container.first));
          containerSons.replace(container.second.occurences1, newNodeId);
          containerSons.markForRemove(container.second.occurences2);

#ifdef TA_PARANO
          Assert(e1.occurences().contains(container.first, container.second.occurences1), "\n*** {}\n*** {}{}", e1.occurences().span(), container.first, container.second.occurences1.span());
          Assert(e2.occurences().contains(container.first, container.second.occurences2), "\n*** {}\n*** {}{}", e2.occurences().span(), container.first, container.second.occurences2.span());
#endif
          trace.get<SequenceOccurences>(pair.ids.first).replace(container.first, container.second.occurences1, newNodeId, SequenceOccurenceIndex(0));
          trace.get<SequenceOccurences>(pair.ids.second).replace(container.first, container.second.occurences2, newNodeId, SequenceOccurenceIndex(1));

#ifdef TA_PARANO
          checkSons(trace, container.first);
          checkParents(trace, pair.ids.first);
          checkParents(trace, pair.ids.second);
#endif
        }

        // Try to factorise
        while (extendsNode<DONT_REMOVE_MARKED_SONS>(trace, newNodeId))
          DO_NOTHING;

        for (const auto& containerId : cache.leafContainers) {
          factoriseLoops<DONT_REMOVE_MARKED_SONS>(trace, cache, containerId);
        }
      }

      for (const auto& containerId : cache.leafContainers)
        removeMarkedSons(trace, containerId);

      return true;
    }

    //---------------------------------------------------

    auto findContainers(const TraceStructure& structure, Cache& cache) {
      Assert(cache.leafContainers.empty(), "");

      auto aux = [&](const auto id, auto f) -> void {
        const auto& container = structure.get<Sequence::Data>(id);
        Assert(Sequence::get_type(container) == trace::Sequence::Type::Container, "");

        for (const auto son : Sequence::get_sons(container)) {
          const auto& event = structure.get<Sequence::Data>(son);
          if (Sequence::get_type(event) != trace::Sequence::Type::Container) {
            cache.leafContainers.push_back(id);
            return;
          }
          f(son, f);
        }
      };

      aux(structure.rootId, aux);
    }
  } // anonymous

  //---------------------------------------------------

  auto naiveTreeFactorisation(TraceStructure& trace) -> void {
    auto cache = Cache {};
    findContainers(trace, cache);

    auto progression = Progression("Initial search of loops : container ", cache.leafContainers.size(), 1, 0);
    for (const auto id : cache.leafContainers) {
      factoriseLoops<REMOVE_MARKED_SONS>(trace, cache, id);
      progression.notify();
    }

    while (factoriseMoreFrequentDuo(trace, cache))
      DO_NOTHING;
  }

  //---------------------------------------------------

  auto trahayTreeFactorisation(TraceStructure& trace) -> void {
    using Index = SequenceOccurenceIndex;

    auto cache = Cache {};
    findContainers(trace, cache);

    auto occurences1 = core::Vector<SequenceOccurenceIndex> {};
    auto occurences2 = core::Vector<SequenceOccurenceIndex> {};

    for (const auto containerId : cache.leafContainers) {
      auto index = 1u;
      while (index < Sequence::get_sons(trace.get<Sequence::Data>(containerId)).size()) {
        // Search factorisable duos
        occurences1.clear();
        occurences2.clear();
        const auto id1 = Sequence::get_sons(trace.get<Sequence::Data>(containerId))[Index(index - 1)];
        const auto id2 = Sequence::get_sons(trace.get<Sequence::Data>(containerId))[Index(index)];

        if (id1 == id2) { // Looking for loops
          { // Scope for reference life duration protection
            auto& containerSons = Sequence::get_sons(trace.get<Sequence::Data>(containerId)); // Will be invalidated in case of node creation
            occurences1.push_back(Index(index - 1));
            auto i = index;
            for (; i < containerSons.size(); ++i) {
              Assert(containerSons[Index(i)].isValid(), "");
              if (containerSons[Index(i)] != id1)
                break;
              containerSons.markForRemove(Index(i));
              occurences1.push_back(Index(i));
            }
            Assert(i >= containerSons.size() || containerSons[Index(i)] != id1, "{} != {}", containerSons[Index(i)], id1);
          }

          const auto loopId = getLoop(cache, trace, id1, occurences1.size());
          Sequence::get_sons(trace.get<Sequence::Data>(containerId)).replace(Index(index - 1), loopId);
          trace.get<SequenceOccurences>(loopId).insert(containerId, Index(index - 1));

          trace.get<SequenceOccurences>(id1).replace(containerId, occurences1, loopId, Index(0));
          removeMarkedSons(trace, containerId);
          Assert(index >= Sequence::get_sons(trace.get<Sequence::Data>(containerId)).size() ||
                     Sequence::get_sons(trace.get<Sequence::Data>(containerId))[Index(index)] != id1,
                 "{} >= {} || {} != {}",
                 index,
                 Sequence::get_sons(trace.get<Sequence::Data>(containerId)).size(),
                 Sequence::get_sons(trace.get<Sequence::Data>(containerId))[Index(index)],
                 id1);
        }
        else { // Looking for new Node
          { // Scope for reference life duration protection
            const auto& containerSons = Sequence::get_sons(trace.get<Sequence::Data>(containerId)); // Will be invalidated in case of node creation
            occurences1.push_back(Index(index - 1));
            occurences2.push_back(Index(index));
            for (const auto i : core::Range(index + 1, containerSons.size())) {
              if (containerSons[Index(i - 1)] == id1 && containerSons[Index(i)] == id2) {
                occurences1.push_back(Index(i - 1));
                occurences2.push_back(Index(i));
              }
            }
          }

          // if no one was found, try next duo
          if (occurences1.size() < 2) {
            ++index;
            //LOG_DEBUG("Miss {}, {}", id1, id2);
          }
          else {
            // Found factorisable duo, create new Sequence
            const auto newNodeId = trace.emplace_back(Sequence::Node {});
            LOG_DEBUG("New node for pair {}, {} with {} occurences -> {}", id1, id2, occurences1.size(), newNodeId);
            auto&& newNodeSons = Sequence::get_sons(trace.get<Sequence::Data>(newNodeId));

            newNodeSons.push_back(id1);
            newNodeSons.push_back(id2);
            trace.get<SequenceOccurences>(newNodeId).insert(containerId, occurences1);

            Sequence::get_sons(trace.get<Sequence::Data>(containerId)).replace(occurences1, newNodeId);
            Sequence::get_sons(trace.get<Sequence::Data>(containerId)).markForRemove(occurences2);

            trace.get<SequenceOccurences>(id1).replace(containerId, occurences1, newNodeId, Index(0));
            trace.get<SequenceOccurences>(id2).replace(containerId, occurences2, newNodeId, Index(1));

            const auto& containerSons = Sequence::get_sons(trace.get<Sequence::Data>(containerId)); // Will be invalidated in case of node creation

            // Try to extend the sequence
            while ([&]() -> bool {
              occurences1.swap(occurences2);
              const auto lastId = newNodeSons[Index(newNodeSons.size() - 1)];

              auto nextId = SequenceId::invalid();
              for (const auto i : core::Range(0u, occurences1.size())) {
                const auto nextIndex = occurences1[i] + 1;

                if (nextIndex.value() >= containerSons.size() || nextId == lastId)
                  return false;

                const auto possibleNextId = containerSons[nextIndex];
                if (!nextId.isValid())
                  nextId = possibleNextId;
                else if (nextId != possibleNextId)
                  return false;

                occurences2[i] = nextIndex;
              }

              //LOG_DEBUG("Extends with {}", nextId);
              trace.get<SequenceOccurences>(nextId).replace(containerId, occurences2, newNodeId, Index(newNodeSons.size()));
              newNodeSons.push_back(nextId);
              Sequence::get_sons(trace.get<Sequence::Data>(containerId)).markForRemove(occurences2);

              return true;
            }());

            factoriseLoops<REMOVE_MARKED_SONS>(trace, cache, containerId);
            removeMarkedSons(trace, containerId);
          }
        }
      }
    }
  }

  //---------------------------------------------------

} // namespace ta::trace

