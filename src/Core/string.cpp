#include "Core/string.hpp"

#include <charconv>

namespace ta::core {


  auto strSize(const char *str) {
    size_t size = 0;
    while ('\0' != str[size])
      ++size;
    return size;
  }

  int utf8ncmp(const char *src1, const char *src2, size_t n) {
    auto s1 = reinterpret_cast<const unsigned char *>(src1);
    auto s2 = reinterpret_cast<const unsigned char *>(src2);
    while (0 != n--) {
      if (*s1 < *s2) return -1;
      else if (*s1 > *s2) return 1;
      ++s1; ++s2;
    }
    return 0;
  }

  template <typename Ptr>
  auto utf8codepoint(Ptr s, Ptr* next) -> int{
    Ptr buf;
    if (next == nullptr)
      next = &buf;

    if (0xf0 == (0xf8 & s[0])) { // 4 byte utf8 codepoint
      *next += 4;
      return ((0x07 & s[0]) << 18) | ((0x3f & s[1]) << 12) | ((0x3f & s[2]) << 6) | (0x3f & s[3]);
    }
    if (0xe0 == (0xf0 & s[0])) { // 3 byte utf8 codepoint
      *next += 3;
      return ((0x0f & s[0]) << 12) | ((0x3f & s[1]) << 6) | (0x3f & s[2]);
    }
    if (0xc0 == (0xe0 & s[0])) { // 2 byte utf8 codepoint
      *next += 2;
      return ((0x1f & s[0]) << 6) | (0x3f & s[1]);
    }
    // 1 byte utf8 codepoint otherwise
    *next += 1;
    return s[0];
  }

  auto isValidUtf8(const char *s, const char *end) -> bool {
    while (s < end) {
      if (0xf0 == (0xf8 & *s)) {
        // ensure each of the 3 following bytes in this 4-byte
        // utf8 codepoint began with 0b10xxxxxx
        if ((0x80 != (0xc0 & s[1])) || (0x80 != (0xc0 & s[2])) || (0x80 != (0xc0 & s[3])))
          return false;

        // ensure that our utf8 codepoint ended after 4 bytes
        if (0x80 == (0xc0 & s[4]))
          return false;

        // ensure that the top 5 bits of this 4-byte utf8
        // codepoint were not 0, as then we could have used
        // one of the smaller encodings
        if ((0 == (0x07 & s[0])) && (0 == (0x30 & s[1])))
          return false;

        // 4-byte utf8 code point (began with 0b11110xxx)
        s += 4;
      }
      else if (0xe0 == (0xf0 & *s)) {
        // ensure each of the 2 following bytes in this 3-byte
        // utf8 codepoint began with 0b10xxxxxx
        if ((0x80 != (0xc0 & s[1])) || (0x80 != (0xc0 & s[2])))
          return false;

        // ensure that our utf8 codepoint ended after 3 bytes
        if (0x80 == (0xc0 & s[3]))
          return false;

        // ensure that the top 5 bits of this 3-byte utf8
        // codepoint were not 0, as then we could have used
        // one of the smaller encodings
        if ((0 == (0x0f & s[0])) && (0 == (0x20 & s[1])))
          return false;

        // 3-byte utf8 code point (began with 0b1110xxxx)
        s += 3;
      }
      else if (0xc0 == (0xe0 & *s)) {
        // ensure the 1 following byte in this 2-byte
        // utf8 codepoint began with 0b10xxxxxx
        if (0x80 != (0xc0 & s[1]))
          return false;

        // ensure that our utf8 codepoint ended after 2 bytes
        if (0x80 == (0xc0 & s[2]))
          return false;

        // ensure that the top 4 bits of this 2-byte utf8
        // codepoint were not 0, as then we could have used
        // one of the smaller encodings
        if (0 == (0x1e & s[0]))
          return false;

        // 2-byte utf8 code point (began with 0b110xxxxx)
        s += 2;
      }
      else if (0x00 == (0x80 & *s)) {
        // 1-byte ascii (began with 0b0xxxxxxx)
        s += 1;
      }
      else {
        // we have an invalid 0b1xxxxxxx utf8 code point entry
        return false;
      }
    }

    return s == end;
  }

  // Char /////////////////////////////////////////////////////

  auto Char::_catToString(char *str) const -> char * {
    char *s = (char *)str;

    if (0 == ((int)0xffffff80 & _data)) {
      // 1-byte/7-bit ascii (0b0xxxxxxx)
      s[0] = (char)_data;
      s += 1;
    }
    else if (0 == ((int)0xfffff800 & _data)) {
      // 2-byte/11-bit utf8 code point (0b110xxxxx 0b10xxxxxx)
      s[0] = 0xc0 | (char)(_data >> 6);
      s[1] = 0x80 | (char)(_data & 0x3f);
      s += 2;
    }
    else if (0 == ((int)0xffff0000 & _data)) {
      // 3-byte/16-bit utf8 code point (0b1110xxxx 0b10xxxxxx 0b10xxxxxx)
      s[0] = 0xe0 | (char)(_data >> 12);
      s[1] = 0x80 | (char)((_data >> 6) & 0x3f);
      s[2] = 0x80 | (char)(_data & 0x3f);
      s += 3;
    }
    else { // if (0 == ((int)0xffe00000 & _data)) {
      // 4-byte/21-bit utf8 code point (0b11110xxx 0b10xxxxxx 0b10xxxxxx 0b10xxxxxx)
      s[0] = 0xf0 | (char)(_data >> 18);
      s[1] = 0x80 | (char)((_data >> 12) & 0x3f);
      s[2] = 0x80 | (char)((_data >> 6) & 0x3f);
      s[3] = 0x80 | (char)(_data & 0x3f);
      s += 4;
    }

    return s;
  }

  auto Char::size() const -> size_t {
    if (0 == ((int)0xffffff80 & _data))
      return 1;
    else if (0 == ((int)0xfffff800 & _data))
      return 2;
    else if (0 == ((int)0xffff0000 & _data))
      return 3;
    else // if (0 == ((int)0xffe00000 & chr)) {
      return 4;
  }

  // String iterator //////////////////////////////////////////

  auto StringIterator::operator++() -> StringIterator& {
    utf8codepoint(_ptr, &_ptr);
    return *this;
  }

  auto StringIterator::operator++(int) -> StringIterator {
    auto res = *this;
    operator++();
    return res;
  }

  auto StringIterator::operator*() -> Char {
    return Char { utf8codepoint(_ptr, (const char**)nullptr) };
  }

  auto StringIterator::operator!=(const StringIterator& it) -> bool {
    return _ptr != it._ptr;
  }

  auto StringIterator::operator==(const StringIterator& it) -> bool {
    return _ptr == it._ptr;
  }

  // StringView ///////////////////////////////////////////////

  StringView::StringView(const char *str, size_t size) : _size(size), _data(str) {
    Assert(isValidUtf8(str, str + size), "");
  }

  auto utf8nlen(const char * str, size_t n) -> size_t {
    const auto end = str + n;
    auto res = 0u;

    while (str != end) {
      Assert(str < end, "");
      ++res;
      utf8codepoint(str, &str);
    }

    return res;
  }

  StringView::StringView(const char *str) : StringView(str, strSize(str)) {}

  auto StringView::operator=(const char *str) -> StringView& {
    _size = strSize(str);
    Assert(isValidUtf8(str, str + _size), "");
    _data = str;
    return *this;
  }

  auto StringView::len() const -> size_t {
    return utf8nlen(_data, _size);
  }

  // Parsing //////////////////////////////////////////////////

  auto find(StringView str, Char c) -> StringIterator {
    auto it = str.begin();
    const auto end = str.end();

    while (it != end && *it != c)
      ++it;

    return it;
  }

  //-------------------------------------------------------------------

  auto findLast(StringView str, Char c) -> StringIterator {
    const auto end = str.end();
    auto res = end;

    for (auto it = str.begin(); it != end; ++it)
      if (*it == c)
        res = it;

    return res;
  }

  //-------------------------------------------------------------------

  auto contains(StringView str, Char c) -> bool {
    return find(str, c) != str.end();
  }

  //-------------------------------------------------------------------

  auto extractFront(StringView& str, SplitOptions options) -> StringView {
    Assert(options.grouper.len() % 2 == 0, "{}", options.grouper);
    Assert(options.grouper.len() == options.grouper.size(),
           "Only one byte utf8 char are supported as grouper for the moment : {}",
           options.grouper);

    auto it = str.begin();
    const auto end = str.end();

    // skip ignored
    while (it != end && contains(options.ignored, *it))
      ++it;

    // if end reached, nothing to extract
    if (it == end) {
      str = StringView(it, end);
      return str;
    }

    const auto front = it;

    // check if the token is a group token (as a quoted string)
    auto grouperIt = find(options.grouper, *it);
    if (grouperIt != options.grouper.end()) {
      const auto grouperIndex = grouperIt - options.grouper.begin();
      // Search the end of the group
      if (grouperIndex % 2 == 0) {
        ++grouperIt;
        ++it;

        while (it != end && *it != *grouperIt)
          ++it;

        // if closer found, end immediatly after it, else end at end of str
        if (it != end)
          ++it;

        str = StringView(it, end);
        return StringView(front, it);
      }

      // if the grouper was a closer, act as if it was a breaker
      it = front;
      ++it;
      str = StringView(it, end);
      return StringView(front, it);
    }

    // if breaker, extract only one character
    if (contains(options.breakers, *it)) {
      ++it;
      str = StringView(it, end);
      return StringView(front, it);
    }

    // read to next breaker or ignored, or to end
    while (it != end &&
           !contains(options.ignored, *it) &&
           !contains(options.breakers, *it) &&
           !contains(options.grouper, *it))
      ++it;

    str = StringView(it, end);
    return StringView(front, it);
  }

  //-------------------------------------------------------------------

  auto getFront(StringView str, SplitOptions options) -> StringView {
    return extractFront(str, options);
  }

  //-------------------------------------------------------------------

  auto frontChar(StringView str) -> Char {
    Assert(str.size() > 0, "{}", str);
    return *str.begin();
  }

  //-------------------------------------------------------------------

  auto backChar(StringView str) -> Char {
    Assert(str.size() > 0, "{}", str);
    auto it = str.begin();
    auto last = StringIterator(nullptr);
    do {
      last = it;
      ++it;
    } while (it != str.end());

    return *last;
  }

  //-------------------------------------------------------------------

  auto popFrontChar(StringView str) -> StringView {
    Assert(str.size() > 0, "{}", str);
    return StringView(++str.begin(), str.end());
  }

  //-------------------------------------------------------------------

  auto removeQuotes(StringView str, StringView quotes) -> StringView {
    Assert(quotes.len() % 2 == 0, "{}", quotes);

    auto it = str.begin();
    const auto end = str.end();

    if (it == end)
      return str;

    auto quotesIt = find(quotes, *it);
    if (quotesIt == quotes.end() || (quotesIt - quotes.begin()) % 2 != 0)
      return str;

    auto last = it++;
    const auto newFirst = it;

    if (it == end)
      return str;

    while (it != end) {
      ++last;
      ++it;
    }

    ++quotesIt;
    if (*quotesIt == *last)
      return StringView(newFirst, last);

    return str;
  }

  //-------------------------------------------------------------------

  auto toInt(StringView str, int base) -> int {
    const auto end = str.ptr() + str.size();
    auto res = static_cast<int>(0);
    [[maybe_unused]] auto [p, ec] = std::from_chars(str.ptr(), end, res, base);
    Assert(p == end, "");
    Assert(ec == std::errc(), "");
    return res;
  }

  //-------------------------------------------------------------------

  auto toFloat(StringView str) -> float { // std::from_chars for floats not implemented yet
    return std::strtof(CString(str), nullptr);
  }

  //-------------------------------------------------------------------

  auto toDouble(StringView str) -> double { // std::from_chars for doubles not implemented yet
    return std::strtod(CString(str), nullptr);
  }

  // String ///////////////////////////////////////////////////

  String::String() : _data(0) {}

  //-----------------------------------------------------------

  String::String(const StringView& str) : _data(str.size()) {
    core::memcpy(_data.ptr(), str.ptr(), str.size());
  }

  //-----------------------------------------------------------

  auto String::operator=(const StringView& str) -> String& {
    memresize<DiscardValues>(_data, str.size());
    core::memcpy(_data.ptr(), str.ptr(), str.size());
    return *this;
  }

  //-----------------------------------------------------------

  auto String::len() const -> size_t {
    return utf8nlen(ptr(), size());
  }

  // Comparisons //////////////////////////////////////////////

  auto compare(const StringView& lhs, const StringView& rhs) -> int {
    const auto s = std::min(lhs.size(), rhs.size());
    const auto res = utf8ncmp(lhs.data().ptr(), rhs.data().ptr(), s);
    if (res == 0)
      return static_cast<int>(lhs.size()) - static_cast<int>(rhs.size());
    return res;
  }

  auto operator==(const StringView& lhs, const StringView& rhs) -> bool {
    if (lhs.size() != rhs.size())
      return false;

    if (lhs.ptr() == rhs.ptr())
      return true;

    return utf8ncmp(lhs.data().ptr(), rhs.data().ptr(), lhs.size()) == 0;
  }

  auto operator<(const StringView& lhs, const StringView& rhs) -> bool {
    if (lhs.ptr() == rhs.ptr() && lhs.size() == rhs.size())
      return false;

    return compare(lhs, rhs) < 0;
  }

  auto operator>(const StringView& lhs, const StringView& rhs) -> bool {
    if (lhs.ptr() == rhs.ptr() && lhs.size() == rhs.size())
      return false;

    return compare(lhs, rhs) > 0;
  }

  auto isPrefix(const StringView& prefix, const StringView& str) -> bool {
    if (prefix.size() > str.size())
      return false;
    return utf8ncmp(prefix.data().ptr(), str.data().ptr(), prefix.size()) == 0;
  }

  //-----------------------------------------------------------

  auto operator+(StringView lhs, StringView rhs) -> String {
    auto buffer = HeapBuffer<char>(lhs.size() + rhs.size());

    core::memcpy(buffer.ptr(), lhs.ptr(), lhs.size());
    core::memcpy(buffer.ptr() + lhs.size(), rhs.ptr(), rhs.size());

    return String(StringView(buffer.ptr(), buffer.size()));
  }

  //-----------------------------------------------------------

}
