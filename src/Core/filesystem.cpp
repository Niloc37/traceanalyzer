#include "Core/filesystem.hpp"

#include "Core/debug.hpp"
#include "Core/string.hpp"

#include <sys/types.h>
#include <sys/stat.h>

#include <cstdlib>
#include <dirent.h>
#include <unistd.h>
#include <string>

namespace ta::core {

  constexpr auto invalidPath = "";

  // Path implementation ////////////////////////////////////////////////////

  struct Path::Impl final {
    std::string currentPath;
    Impl(const char * str) : currentPath { str } {}
  };

  //-------------------------------------------------------------------------

  Path::Path() : _impl(core::make_unique<Impl>(invalidPath)) {}

  //-------------------------------------------------------------------------

  Path::~Path() = default;

  //-------------------------------------------------------------------------

  Path::Path(Path&& rhs) { std::swap(_impl, rhs._impl); }

  //-------------------------------------------------------------------------

  Path::Path(const Path& rhs) : _impl(core::make_unique<Impl>(*rhs._impl)) {}

  //-------------------------------------------------------------------------

  auto Path::operator=(Path&& rhs) -> Path& {
    std::swap(_impl, rhs._impl);
    return *this;
  }

  //-------------------------------------------------------------------------

  auto Path::operator=(const Path& rhs) -> Path& {
    _impl->currentPath = rhs._impl->currentPath;
    return *this;
  }

  //-------------------------------------------------------------------------

  auto Path::operator=(const char *rhs) -> Path& {
    _impl = core::make_unique<Impl>(rhs);
    _simplify();
    return *this;
  }

  //-------------------------------------------------------------------------

  Path::Path(const char *path, size_t size) : Path() {
    if (size > 0)
      _impl->currentPath = std::string_view { path, size };
    else
      _impl->currentPath = path;
    _simplify();
  }

  //-------------------------------------------------------------------------

  Path::Path(const StringView& str) : Path(str.data().ptr(), str.size()) {}

  //-------------------------------------------------------------------------

  auto Path::operator=(const StringView& str) -> Path& {
    _impl->currentPath = std::string_view { str.ptr(), str.size() };
    _simplify();
    return *this;
  }

  //-------------------------------------------------------------------------

  auto Path::operator==(const Path& rhs) const -> bool { return _impl->currentPath == rhs._impl->currentPath; }

  //-------------------------------------------------------------------------

  auto Path::operator!=(const Path& rhs) const -> bool { return _impl->currentPath != rhs._impl->currentPath; }

  //-------------------------------------------------------------------------

  auto Path::c_str() const -> const char * {
    return _impl->currentPath.c_str();
  }

  //-------------------------------------------------------------------------

  auto Path::memory() const -> nlc::meta::bytes<unsigned long> {
    return nlc::meta::bytes<unsigned long>(_impl->currentPath.capacity() + sizeof(Impl));
  }

  //-------------------------------------------------------------------------

  auto Path::operator/(const Path& rhs) const -> Path {
    return operator/(rhs.c_str());
  }

  //-------------------------------------------------------------------------

  auto Path::operator/(const char *rhs) const -> Path {
    auto res = *this;
    if (!res._impl->currentPath.empty())
      res._impl->currentPath += "/";
    res._impl->currentPath += rhs;
    res._simplify();
    return res;
  }

  //-------------------------------------------------------------------------

  auto Path::invalid() -> Path {
    return Path {};
  }

  //-------------------------------------------------------------------------

  auto Path::isValid() const -> bool {
    return _impl->currentPath != invalidPath;
  }

  //-------------------------------------------------------------------------

  auto Path::isInvalid() const -> bool {
    return _impl->currentPath == invalidPath;
  }

  //-------------------------------------------------------------------------

  auto Path::parent() const -> Path {
    Assert(isValid(), "");
    return *this / "..";
  }

  //-------------------------------------------------------------------------

  auto Path::basename() const -> Path {
    Assert(isValid(), "");
    return Path { _basename() };
  }

  //-------------------------------------------------------------------------

  auto Path::extension() const -> StringView {
    Assert(isValid(), "");
    const auto basename = _basename();
    auto extBegin = findLast(basename, '.');
    if (extBegin == basename.end())
      return "";
    auto first = extBegin;
    ++first;
    return StringView(first, basename.end());
  }

  //-------------------------------------------------------------------------

  auto Path::_basename() const -> StringView {
    Assert(isValid(), "");
    const auto res = std::strrchr(c_str(), '/');
    return res == nullptr ? c_str() : res + 1;
  }

  //-------------------------------------------------------------------------

  auto Path::_simplify() -> void {
    const auto isAbsolute = _impl->currentPath.front() == '/';

    auto tokens = [] (std::string_view str) {
      const auto end = str.end();
      auto it = str.begin();
      auto res = Vector<std::string_view> {};

      auto firstCharOfToken = end;

      while (it != end) {
        if (*it == '/') {
          if (firstCharOfToken != end)
            res.emplace_back(&firstCharOfToken[0], it - firstCharOfToken);

          firstCharOfToken = end;
        }
        else if (firstCharOfToken == end) {
          firstCharOfToken = it;
        }
        ++it;
      }

      if (firstCharOfToken != end)
        res.emplace_back(&firstCharOfToken[0], it - firstCharOfToken);

      return res;
    }(_impl->currentPath);

    auto directories = decltype(tokens) {};

    for (auto token : tokens) {
      if (token == "..") {
        if (directories.empty()) {
          if (!isAbsolute)
            directories.emplace_back("..");
        }
        else {
          if (directories.back() != "..")
            directories.pop_back();
          else
            directories.emplace_back("..");
        }
      }
      else if (token != ".")
        directories.emplace_back(token);
    }

    auto res = std::string {};

    for (const auto& directory : directories) {
      if (!res.empty() || isAbsolute) res += '/';
      res += directory;
    }

    if (res.empty()) res = isAbsolute ? "/" : ".";

    _impl->currentPath = res;
  }

  // Free functions /////////////////////////////////////////////////////////

  auto currentDirectory() -> Path {
    char buf[PATH_MAX];
    if (getcwd(buf, sizeof(buf)) != nullptr)
      return Path { buf };
    return Path {};
  }

  //-------------------------------------------------------------------------

  auto setCurrentDirectory(const Path& path) -> void {
    Assert(path.isValid(), "");
    if (chdir(path.c_str()) != 0)
      LOG_ERROR("Failed to set current path to \"{}\"", path); // TODO check errno
  }

  //-------------------------------------------------------------------------

  auto searchParent(const Path& path, const char *_name) -> Path {
    Assert(path.isValid(), "");
    const auto name = Path { _name };
    auto res =  path;
    while (true) {
      const auto basename = res.basename();
      if (basename == name)
        return res;
      if (res == Path { "/" })
        return Path::invalid();
      res = res.parent();
    }
  }

  //-------------------------------------------------------------------------

  auto isFile(const Path& path) -> bool {
    Assert(path.isValid(), "");
    struct stat sb;
    return stat(path.c_str(), &sb) == 0 && S_ISREG(sb.st_mode);
  }

  //-------------------------------------------------------------------------

  auto isDirectory(const Path& path) -> bool {
    Assert(path.isValid(), "");
    struct stat sb;
    return stat(path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode);
  }

  //-------------------------------------------------------------------------

  auto listEntries(const Path& path) -> Vector<Path> {
    Assert(path.isValid(), "");
    Assert(isDirectory(path), "{}", path);

    const auto dirp = opendir(path.c_str());
    Assert(dirp != nullptr, ""); // TODO check errno

    auto res = Vector<Path> {};

    while (true) {
      const auto dp = readdir(dirp);
      if (dp == nullptr)
        break;
      res.emplace_back(dp->d_name);
    }

    closedir(dirp);
    return res;
  }

  //-------------------------------------------------------------------------

} // namespace ta::core
