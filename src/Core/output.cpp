#include "Core/output.hpp"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string_view>

#include "Core/debug.hpp"

namespace ta::core {

  auto output(StringView s) -> void { std::cout << std::string_view(s.ptr(), s.size()); }

  // StringOutput ///////////////////////////////////////////////////////

  struct StringOutput::Impl {
    std::stringstream ss;
  };

  //--------------------------------------------------------------------

  StringOutput::StringOutput() : _impl { core::make_unique<Impl>() } {}
  StringOutput::~StringOutput() = default;

  //--------------------------------------------------------------------

  auto StringOutput::operator()(StringView s) -> void { _impl->ss << std::string_view(s.ptr(), s.size()); }

  //--------------------------------------------------------------------

  auto StringOutput::str() const -> std::string {
    return _impl->ss.str();
  }

  // FileOutput /////////////////////////////////////////////////////////

  struct FileOutput::Impl {
    std::ofstream stream;

    Impl(const core::Path& path) {
      Assert(path.isValid(), "");
      stream.open(path.c_str());
      Assert(stream.is_open(), "");
    }
  };

  //--------------------------------------------------------------------

  FileOutput::FileOutput(const core::Path& path) : _impl { core::make_unique<Impl>(path) } {}
  FileOutput::~FileOutput() = default;

  //--------------------------------------------------------------------

  auto FileOutput::operator()(StringView s) -> void { _impl->stream << std::string_view(s.ptr(), s.size()); }

}
