#pragma once

#include "Core/debug.hpp"
#include "nlc/meta/basics.hpp"
#include "nlc/meta/traits.hpp"

#include <sstream>

namespace ta::core {

  template <typename T>
  class span final {
    public:
      using size_t = unsigned long int;
      using iterator = T*;
      using const_iterator = const T*;

    public:
      span(T* ptr, size_t size) : _begin(ptr) , _end(ptr + size) {}
      span(T* begin, T* end) : _begin(begin) , _end(end) {}
      span(T& ref) : _begin(&ref), _end(&ref + 1) {}
      span() = delete;
      span(span&&) = default;
      span(const span&) = default;
      auto operator=(span&&) -> span& = default;
      auto operator=(const span&) -> span& = default;

      [[nodiscard]] operator span<const T> () const { return span<const T> { _begin, _end }; }

    public:
      [[nodiscard]] auto begin() { return _begin; }
      [[nodiscard]] auto begin() const { return _begin; }
      [[nodiscard]] auto end() { return _end; }
      [[nodiscard]] auto end() const { return _end; }

    public:
      [[nodiscard]] auto operator[](size_t index) -> T& {
        Assert(index < size(), "Out of bound");
        return _begin[index];
      }
      [[nodiscard]] auto operator[](size_t index) const -> const T& {
        Assert(index < size(), "Out of bound");
        return _begin[index];
      }
      [[nodiscard]] auto front() -> T& { Assert(size() > 0u, ""); return _begin[0u]; }
      [[nodiscard]] auto front() const -> const T& { Assert(size() > 0u, ""); return _begin[0u]; }
      [[nodiscard]] auto back() -> T& { Assert(size() > 0u, ""); return _begin[size() - 1u]; }
      [[nodiscard]] auto back() const -> const T& { Assert(size() > 0u, ""); return _begin[size() - 1u]; }

    public:
      [[nodiscard]] auto size() const { return static_cast<size_t>(_end - _begin); }
      [[nodiscard]] auto ptr() -> T* { return _begin; }
      [[nodiscard]] auto ptr() const -> const T* { return _begin; }

    private:
      T* _begin;
      T* _end;
  };
  template <typename It> span(It b, It) -> span<nlc::meta::rm_ref<decltype(*b)>>;

  template <typename T> inline auto make_span(T& container) {
    if constexpr (nlc::meta::is_array<T>)
      return span(container + 0, container + nlc::meta::array_len<T>);
    else
      return span<nlc::meta::rm_ref<decltype(*container.begin())>>(&*container.begin(), &*container.end());
  }

}

template <typename T> struct fmt::formatter<ta::core::span<T>> : formatter<string_view> {
  template <typename FormatContext> auto format(const ta::core::span<T>& v, FormatContext& ctx) {
    if (v.size() == 0u)
      return formatter<string_view>::format("[]", ctx);
    auto ss = std::stringstream {};
    auto it = v.begin();
    const auto end = v.end();
    ss << fmt::format("[{}", *it);
    for (++it; it != end; ++it)
      ss << fmt::format(", {}", *it);
    ss << " ]";
    return formatter<string_view>::format(string_view(ss.str()), ctx);
  }
};
