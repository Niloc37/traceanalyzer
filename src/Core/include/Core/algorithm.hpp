#pragma once

#include "Core/span.hpp"

namespace ta::core {

  //--------------------------------------------------------------------

  template <typename T> auto isSorted(span <T> data) -> bool {
    if (data.size() < 2)
      return true;

    for (auto it1 = data.begin(), it2 = it1 + 1; it2 != data.end(); ++it1, ++it2)
      if (*it2 < *it1)
        return false;
    return true;
  }

  //--------------------------------------------------------------------

  template <typename T, typename F> auto findIf(const T& data, F&& f) {
    auto it = data.begin();
    for (; it != data.end(); ++it)
      if (f(*it))
        break;
    return it;
  }

  //--------------------------------------------------------------------

  template <typename T, typename Q> auto find(const T& data, const Q& v) {
    return findIf(data, [&v] (const auto& d) { return d == v; });
  }

  //--------------------------------------------------------------------

  template <typename T, typename Q> auto sfind(span <T> data, Q&& v) -> typename span<T>::iterator {
    auto aux = [](auto f, span<T> data, const T& v) -> typename span<T>::iterator {
      if (data.size() == 0)
        return data.begin();

      const auto p = data.begin() + (data.end() - data.begin()) / 2;
      if (*p < v)
        return f(f, core::span(p + 1, data.end()), v);
      return f(f, core::span(data.begin(), p), v);
    };

    const auto res = aux(aux, data, v);
    return *res == v ? res : data.end();
  }

  //--------------------------------------------------------------------

  template <typename T, typename Q> auto sremove(span <T> data, span <Q> rem) -> typename span<T>::iterator {
    if (rem.begin() == rem.end())
      return data.size();

    auto remIt = rem.begin();
    auto readIt = core::sfind(data, get(remIt));
    auto writeIt = readIt;

    while (remIt != rem.end()) {
      Assert(readIt != data.end(), "rem must be a subset of data : \ndata : {}\nrem : {}", data, rem);
      if (*readIt < get(remIt)) {
        *writeIt = *readIt;
        ++writeIt;
        ++readIt;
      }
      else {
        Assert(get(remIt) == *readIt, "rem must be a subset of data : \ndata : {}\nrem : {}", data, rem);
        ++remIt;
        ++readIt;
      }
    }

    while (readIt != data.end()) {
      *writeIt = *readIt;
      ++writeIt;
      ++readIt;
    }

    return writeIt - data.begin();
  }
}
