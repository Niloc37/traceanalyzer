#pragma once

#include "Core/string.hpp"
#include "Core/uniquePointer.hpp"
#include "Core/vector.hpp"
#include "spdlog/fmt/fmt.h"


namespace ta::core {

  class Path final {
      struct Impl;
    public:
      Path();
      ~Path();
      Path(Path&&);
      Path(const Path&);
      auto operator=(Path&&) -> Path&;
      auto operator=(const Path&) -> Path&;
      explicit Path(const StringView&);
      auto operator=(const char *) -> Path&;
      Path(const char *str, size_t size = 0);
      auto operator=(const StringView&) -> Path&;

    public:
      [[nodiscard]] auto operator==(const Path&) const -> bool;
      [[nodiscard]] auto operator!=(const Path&) const -> bool;

    public:
      [[nodiscard]] auto c_str() const -> const char *;
      operator const char *() const { return c_str(); }
      [[nodiscard]] auto str() const -> StringView { return c_str(); }
      operator StringView() const { return str(); }

    public:
      [[nodiscard]] auto memory() const -> nlc::meta::bytes<unsigned long>;

    public:
      [[nodiscard]] auto operator/(const Path&) const -> Path;
      [[nodiscard]] auto operator/(const char *) const -> Path;

    public:
      [[nodiscard]] static auto invalid() -> Path;
      [[nodiscard]] auto isValid() const -> bool;
      [[nodiscard]] auto isInvalid() const -> bool;

    public:
      [[nodiscard]] auto parent() const -> Path;
      [[nodiscard]] auto basename() const -> Path;
      [[nodiscard]] auto extension() const -> StringView;

    private:
      [[nodiscard]] auto _basename() const -> StringView;
      auto _simplify() -> void;

    private:
      core::unique_ptr<Impl> _impl;
  };

  [[nodiscard]] auto currentDirectory() -> Path;
  auto setCurrentDirectory(const Path& path) -> void;
  [[nodiscard]] auto searchParent(const Path& path, const char *name) -> Path;
  [[nodiscard]] auto isFile(const Path& path) -> bool;
  [[nodiscard]] auto isDirectory(const Path& path) -> bool;
  [[nodiscard]] auto listEntries(const Path& path) -> Vector<Path>;
} // namespace ta::core

template <> struct fmt::formatter<ta::core::Path> {
  using Path = ta::core::Path;
  template <typename ParseContext> constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

  template <typename FormatContext> auto format(const Path& path, FormatContext& ctx) {
    return format_to(ctx.out(), path.c_str());
  }
};
