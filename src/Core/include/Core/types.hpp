#pragma once

namespace ta::core {

#define DEFINE_TYPE(name, type, expected_size) using name = type; static_assert(sizeof(name) == expected_size)
  DEFINE_TYPE(byte, unsigned char, 1);

  DEFINE_TYPE(uint8, unsigned char, 1);
  DEFINE_TYPE(uint16, unsigned short, 2);
  DEFINE_TYPE(uint32, unsigned int, 4);
  DEFINE_TYPE(uint64, unsigned long int, 8);

  DEFINE_TYPE(int8, char, 1);
  DEFINE_TYPE(int16, short, 2);
  DEFINE_TYPE(int32, int, 4);
  DEFINE_TYPE(int64, long int, 8);

  using size_t = uint64;
  using ssize_t = int64;
  using size32_t = uint32;
  using ssize32_t = int32;
#undef DEFINE_TYPE

}
