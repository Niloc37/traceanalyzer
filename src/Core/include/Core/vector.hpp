#pragma once

#include "Core/debug.hpp"
#include "Core/range.hpp"
#include "Core/uninitializedVector.hpp"
#include "nlc/meta/basics.hpp"
#include "nlc/meta/traits.hpp"

namespace ta::core {

  //----------------------------------------------------------------------

  namespace impl {

    template <typename T> auto dummyMake() -> T;

    template <typename Index> auto getIndex(Index id) {
      if constexpr (nlc::meta::is_integral<Index>)
        return id;
      else
        return id.value();
    }

    template <typename Index> using getIndex_t = decltype(getIndex(dummyMake<Index>()));

  }

  //----------------------------------------------------------------------

  template <typename T, size_t Size = 0u, typename Index = size_t> class Vector final {
    public:
      using size_t = impl::getIndex_t<Index>;
      using iterator = T *;
      using const_iterator = const T *;

    public:
      Vector() : _data(0) {}
      ~Vector() { clear(); }
      Vector(Vector&&) = default;
      Vector(Index n, const T& v);
      auto operator=(Vector&&) -> Vector& = default;
      Vector(const Vector&) = delete;
      auto operator=(const Vector&) -> Vector& = delete;

    public:
      [[nodiscard]] auto size() const { return _data.size(); }
      [[nodiscard]] auto empty() const { return _data.empty(); }
      [[nodiscard]] auto memory() const { return _data.memory(); }
      [[nodiscard]] auto capacity() const { return _data.capacity(); }

    public:
      [[nodiscard]] auto span() { return _data.span(); }
      [[nodiscard]] auto span() const { return _data.span(); }
      [[nodiscard]] auto ptr() { return _data.ptr(); }
      [[nodiscard]] auto ptr() const { return _data.ptr(); }
      [[nodiscard]] operator core::span<T>() { return span(); }
      [[nodiscard]] operator core::span<const T>() const { return span(); }

    public:
      [[nodiscard]] auto begin() { return _data.begin(); }
      [[nodiscard]] auto end() { return _data.end(); }
      [[nodiscard]] auto begin() const { return _data.begin(); }
      [[nodiscard]] auto end() const { return _data.end(); }

    public:
      [[nodiscard]] decltype(auto) front() { return _data.front(); }
      [[nodiscard]] decltype(auto) back() { return _data.back(); }
      [[nodiscard]] decltype(auto) front() const { return _data.front(); }
      [[nodiscard]] decltype(auto) back() const { return _data.back(); }
      [[nodiscard]] decltype(auto) operator[](Index i) { return _data[impl::getIndex(i)]; }
      [[nodiscard]] decltype(auto) operator[](Index i) const { return _data[impl::getIndex(i)]; }

    public:
      [[nodiscard]] auto indexOf(const_iterator it) const -> Index;

    public:
      auto resize(size_t size, const T& value = T());
      auto resize_no_extra_memory(size_t size, const T& value = T());
      auto reserve(size_t size) { _data.reserve(size); }
      auto reserve_exactly(size_t size) { _data.reserve_exactly(size); }
      auto shrink_to_fit() { _data.shrink_to_fit(); }

    public:
      template <typename...Args> auto emplace_back(Args&& ...args) -> Index;
      template <typename...Args> auto emplace_back_already_reserved(Args&& ...args) -> Index;
      template <typename...Args> auto insert(size_t index, Args&&...args) -> void;
      auto push_back(const T& v) { return emplace_back(v); }
      auto push_back_already_reserved(const T& v) { return emplace_back_already_reserved(v); }
      auto pop_back() -> void;
      auto clear() -> void;

    public:
      auto swap(Vector& rhs) { _data.swap(rhs._data); }

    public:
      auto indexes() const { return core::Range<typename Index::underlying_t, Index>(0u, size()); }

    private:
      UninitializedVector <T, Size, size_t> _data;
  };

  //----------------------------------------------------------------------

  /*
  template <typename T, size_t Size, typename Index> Vector<T, Size, Index>::Vector(const Vector& rhs)
      : _data(rhs.size()) {
    if constexpr (nlc::meta::is_trivially_copyable<T>) {
      core::memcpy(ptr(), rhs.ptr(), rhs.size() * sizeof(T));
    }
    else {
      for (const auto i : Range(0u, rhs.size()))
        _data.construct(i, rhs._data[i]);
    }
  }
  */

  //----------------------------------------------------------------------

  /*
  template <typename T, size_t Size, typename Index>
  auto Vector<T, Size, Index>::operator=(const Vector& rhs) -> Vector& {
    auto temp = rhs;
    swap(temp);
    return *this;
  }
  */

  //----------------------------------------------------------------------

  template <typename T, size_t Size, typename Index>
  Vector<T, Size, Index>::Vector(Index n, const T& v) : _data(n) {
    for (auto i = 0u; i < n; ++i)
      _data.construct(i, v);
  }

  //----------------------------------------------------------------------

  template <typename T, size_t Size, typename Index>
  template <typename...Args>
  auto Vector<T, Size, Index>::emplace_back(Args&& ... args) -> Index {
    const auto index = size();
    _data.resize(index + 1u);
    _data.construct(index, nlc::forward<Args>(args)...);
    return Index(index);
  }

  //----------------------------------------------------------------------

  template <typename T, size_t Size, typename Index>
  template <typename...Args>
  auto Vector<T, Size, Index>::emplace_back_already_reserved(Args&& ... args) -> Index {
    const auto index = size();
    _data.resize_no_grow(index + 1u);
    _data.construct(index, nlc::forward<Args>(args)...);
    return Index(index);
  }

  //----------------------------------------------------------------------

  template <typename T, size_t Size, typename Index>
  template <typename...Args>
  auto Vector<T, Size, Index>::insert(size_t index, Args&&...args) -> void {
    const auto movsize = size() - index;
    _data.resize(size() + 1u);
    const auto it = begin() + index;
    core::memmove(it + 1, it, movsize);
    _data.construct(index, nlc::forward<Args>(args)...);
  }

  //----------------------------------------------------------------------

  template <typename T, size_t Size, typename Index> auto Vector<T, Size, Index>::pop_back() -> void {
    if constexpr (!nlc::meta::is_trivially_destructible<T>)
      back().~T();
    _data.resize_no_grow(size() - 1);
  }

  //----------------------------------------------------------------------

  template <typename T, size_t Size, typename Index> auto Vector<T, Size, Index>::clear() -> void {
    if constexpr (!nlc::meta::is_trivially_destructible<T>)
      for (auto& v : _data)
        v.~T();
    _data.resize_no_grow(0u);
  }

  //----------------------------------------------------------------------

  template <typename T, size_t Size, typename Index> auto Vector<T, Size, Index>::indexOf(const_iterator it) const -> Index {
    Assert(it >= begin() && it < end(), "");
    return Index(it - begin());
  }

  //----------------------------------------------------------------------

  template <typename T, size_t Size, typename Index> auto Vector<T, Size, Index>::resize(size_t size_, const T& value) {
    const auto oldSize = size();
    if (size_ > oldSize) {
      _data.resize(size_);
      for (auto i = oldSize; i < size_; ++i)
        _data.construct(i, value);
    }
    else if (size_ < oldSize) {
      if constexpr (!nlc::meta::is_trivially_destructible<T>)
        for (auto i = size_; i < oldSize; ++i)
          _data.destruct(i);
      _data.resize_no_grow(size_);
    }
  }

  //----------------------------------------------------------------------

  template <typename T, size_t Size, typename Index> auto Vector<T, Size, Index>::resize_no_extra_memory(size_t size_, const T& value) {
    const auto oldSize = size();
    if (size_ > oldSize) {
      Assert(size_ < capacity(), "{} < {}", size_, capacity());
      _data.resize_no_extra_memory(size_);
      for (auto i = oldSize; i < size_; ++i)
        _data.construct(i, value);
    }
    else if (size_ < oldSize) {
      for (auto i = size_; i < oldSize; ++i)
        _data.desctruct(i);
      _data.resize_no_extra_memory(size_);
    }
  }

  //----------------------------------------------------------------------

  template <typename Iterable> auto vectorFromIterable(const Iterable& container) {
    using Type = nlc::meta::rm_const_ref<decltype(*container.begin())>;
    auto res = Vector<Type> {};
    for (const auto& value : container)
      res.emplace_back(value);
    return res;
  }

  //----------------------------------------------------------------------

}
