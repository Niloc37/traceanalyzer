#pragma once

#include "Core/tuple.hpp"
#include "Core/vector.hpp"
#include "nlc/meta/list.hpp"

namespace ta::core {

  namespace impl {
  }

  // ------------------------------------------------------------------------------

  template <typename KeysAndValueTypes, typename Index = size32_t>
  class StructOfArray {
      using size_t = impl::getIndex_t<Index>;
      using keysAndValueTypes = KeysAndValueTypes;
      using keys = nlc::meta::map<nlc::meta::first, keysAndValueTypes>;
      using types = nlc::meta::map<nlc::meta::second, keysAndValueTypes>;

      template <typename T> using toVector = core::Vector<T, 0u, Index>;
      using storage = tuple_from_type_list<types, toVector>;

      template <typename...Is> auto tuple_from_index(Index i, nlc::meta::list<Is...>) {
        return tuple_from_type_list<types, nlc::meta::add_lref> ( _data.template get<Is::value>()[i]... );
      }

      template <typename...Is> auto tuple_from_index(Index i, nlc::meta::list<Is...>) const {
        return tuple_from_type_list<types, nlc::meta::add_const_ref> ( _data.template get<Is::value>()[i]... );
      }

    public:
      class view {
        public:
          template <typename Key> [[nodiscard]] decltype(auto) get() {
            constexpr auto index = nlc::meta::index_of<Key, keys>;
            return core::get<index>(_data);
          }
          template <typename Key> [[nodiscard]] decltype(auto) get() const {
            constexpr auto index = nlc::meta::index_of<Key, keys>;
            return core::get<index>(_data);
          }

          view(StructOfArray& s, Index i) : _data(s.tuple_from_index(i, nlc::meta::range<0, types::size>())) {}

        private:
          tuple_from_type_list<types, nlc::meta::add_lref> _data;
      };

      class const_view {
        public:
          template <typename Key> [[nodiscard]] decltype(auto) get() const {
            constexpr auto index = nlc::meta::index_of<Key, keys>;
            return core::get<index>(_data);
          }

          const_view(const view& v) : _data(v._data) {}
          const_view(const StructOfArray& s, Index i) : _data(s.tuple_from_index(i, nlc::meta::range<0, types::size>())) {}

        private:
          const tuple_from_type_list<types, nlc::meta::add_const_ref> _data;
      };

    public:
      [[nodiscard]] auto size() const { return core::get<0>(_data).size(); }
      [[nodiscard]] auto empty() const { return size() == 0; }
      [[nodiscard]] auto capacity() const { core::get<0>(_data).capacity(); }
      [[nodiscard]] auto memory() const {
        auto res = decltype(core::get<0>(_data).memory())(0);
        forEach(_data, [&res] (const auto& v) { res += v.memory(); });
        return res;
      }

      template <typename...Args> auto emplace_back(Args&&...args) {
        return _emplace_back<0>(nlc::forward<Args>(args)...);
      }

    public:
      template <typename Key> [[nodiscard]] decltype(auto) get() {
        constexpr auto index = nlc::meta::index_of<Key, keys>;
        return core::get<index>(_data);
      }
      template <typename Key> [[nodiscard]] decltype(auto) get() const {
        constexpr auto index = nlc::meta::index_of<Key, keys>;
        return core::get<index>(_data);
      }

      template <typename Key> [[nodiscard]] decltype(auto) get(Index i) {
        constexpr auto index = nlc::meta::index_of<Key, keys>;
        return core::get<index>(_data)[i];
      }
      template <typename Key> [[nodiscard]] decltype(auto) get(Index i) const {
        constexpr auto index = nlc::meta::index_of<Key, keys>;
        return core::get<index>(_data)[i];
      }

      auto operator[] (Index i) { return view(*this, i); }
      auto operator[] (Index i) const { return const_view(*this, i); }

    private:
      template <int i, typename...Args, typename Arg> auto _emplace_back(Arg&& arg, Args&&...args) {
        if constexpr (i + 1 < types::size) {
          _emplace_back<i + 1>(nlc::forward<Args>(args)...);
        }
        return core::get<i>(_data).emplace_back(nlc::forward<Arg>(arg));
      }
      template <int i> auto _emplace_back() {
        if constexpr (i + 1 < types::size) {
          _emplace_back<i + 1>();
        }
        return core::get<i>(_data).emplace_back();
      }

      template <int i> auto _memory() const {
        if constexpr (i + 1 < types::size)
          return core::get<i>(_data).memory() + _memory<i + 1>();
        else
          return core::get<i>(_data).memory();
      }

    private:
      storage _data;
  };
}
