#pragma once

#include "spdlog/fmt/fmt.h"
#include "Core/uniquePointer.hpp"
#include "Core/string.hpp"
#include "Core/filesystem.hpp"

namespace ta::core {

  template <typename...T> [[nodiscard]] auto format(T&& ...args) -> core::String {
    return String(fmt::format(nlc::forward<T>(args)...).c_str());
  }

  class Output {
    public:
      virtual ~Output() = default;

    public:
      virtual auto operator()(StringView) -> void = 0;

      template <typename T, typename...Q> auto operator()(T&& format, Q&& ...args) -> void {
        using Format = nlc::meta::rm_const_ref<T>;

        if constexpr (nlc::meta::contains<nlc::meta::list<StringView, String>, Format>) {
          operator()(core::format(static_cast<const char*>(CString(format)), nlc::forward<Q>(args)...).view());
        }
        else if constexpr (nlc::meta::contains<nlc::meta::list<std::string, std::string_view>, Format>)
          operator()(core::format(format.c_str(), nlc::forward<Q>(args)...).view());
        else
          operator()(core::format(format, nlc::forward<Q>(args)...).view());
      }
  };

  class StringOutput final : public Output {
      struct Impl;

    public:
      StringOutput();
      ~StringOutput();

      using Output::operator();

      auto operator()(StringView) -> void override;

    public:
      auto str() const -> std::string;
      operator std::string() const { return str(); }

    private:
      core::unique_ptr<Impl> _impl;
  };

  class FileOutput final : public Output {
      struct Impl;

    public:
      FileOutput(const core::Path&);
      ~FileOutput();

      using Output::operator();

      auto operator()(StringView) -> void override;

    private:
      core::unique_ptr<Impl> _impl;
  };
}
