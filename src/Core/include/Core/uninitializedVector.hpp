#pragma once

#include "Core/buffer.hpp"
#include "Core/debug.hpp"
#include "nlc/meta/units.hpp"

namespace ta::core {

  template <typename T, size_t Size = 0u, typename Size_t = nlc::meta::Default> class UninitializedVector final {
    public:
      using size_t = nlc::meta::if_default<Size_t, size32_t>;

    private:
      using Buffer = nlc::meta::if_t<Size == 0, HeapBuffer < T, size_t>, MixedBuffer <T, Size, size_t>>;

    public:
      UninitializedVector() : UninitializedVector(0u) {}
      explicit UninitializedVector(size_t size) : _data(size), _size(size) {}
      UninitializedVector(UninitializedVector&& rhs) : _size(rhs._size) {
        memmove(_data, rhs._data);
        rhs._size = 0;
      }
      auto operator=(UninitializedVector&& rhs) -> UninitializedVector& {
        _size = rhs._size;
        memmove(_data, rhs._data);
        rhs._size = 0;
        return *this;
      }
      UninitializedVector(const UninitializedVector&) = delete;
      auto operator=(const UninitializedVector&) -> UninitializedVector& = delete;

    public:
      auto resize(size_t size) -> void {
        grow(size);
        _size = size;
      }
      auto resize_no_grow(size_t size) -> void {
        Assert(size <= _data.size(), "{} <= {}", _size, _data.size());
        _size = size;
      }
      auto resize_no_extra_memory(size_t size) -> void {
        if (_size != size) {
          _size = size;
          relocate(size);
        }
      }
      auto reserve(size_t size) -> void { if (size > capacity()) grow(size); }
      auto reserve_exactly(size_t size) -> void { if (size > capacity()) relocate(size); }
      auto shrink_to_fit() -> void { resize_no_extra_memory(_size); }

      [[nodiscard]] auto size() const { return _size; }
      [[nodiscard]] auto empty() const { return _size == 0u; }
      [[nodiscard]] auto capacity() const { return _data.size(); }
      [[nodiscard]] auto memory() const { return _data.memory(); }

      [[nodiscard]] auto ptr() { return _data.ptr(); }
      [[nodiscard]] auto ptr() const { return _data.ptr(); }

      [[nodiscard]] auto begin() { return ptr(); }
      [[nodiscard]] auto end() { return ptr() + _size; }
      [[nodiscard]] auto begin() const { return ptr(); }
      [[nodiscard]] auto end() const { return ptr() + _size; }

      [[nodiscard]] auto span() { return core::span(begin(), end()); }
      [[nodiscard]] auto span() const { return core::span(begin(), end()); }
      [[nodiscard]] operator core::span<T>() { return span(); }
      [[nodiscard]] operator core::span<const T>() const { return span(); }

      [[nodiscard]] decltype(auto) operator[](size_t index) { return _data[index]; }
      [[nodiscard]] decltype(auto) operator[](size_t index) const { return _data[index]; }
      [[nodiscard]] decltype(auto) front() { return _data[0u]; }
      [[nodiscard]] decltype(auto) front() const { return _data[0u]; }
      [[nodiscard]] decltype(auto) back() { return _data[_size - 1u]; }
      [[nodiscard]] decltype(auto) back() const { return _data[_size - 1u]; }

      template <typename...Args> auto construct(size_t index, Args&& ...args) -> void {
        new(ptr() + index) T(nlc::forward<Args>(args)...);
      }
      auto destruct(size_t index) -> void { _data[index].~T(); }

      auto swap(UninitializedVector& rhs) {
        memswap(_data, rhs._data);
        std::swap(rhs._size, _size);
      }

    private:
      auto grow(size_t size) -> void;
      auto relocate(size_t size) -> void;

    private:
      Buffer _data;
      size_t _size;
  };

  //--------------------------------------------------------------------

  template <typename T, size_t Size, typename Size_t>
  auto swap(UninitializedVector<T, Size, Size_t>& lhs, UninitializedVector<T, Size, Size_t>& rhs) { lhs.swap(rhs); }

  //--------------------------------------------------------------------

  template <typename T, size_t Size, typename Size_t> auto UninitializedVector<T, Size, Size_t>::grow(size_t size) -> void {
    if (size <= capacity())
      return;

    auto c = capacity() < 2u ? 2u : capacity();
    while (size > c)
      c += c / 2;
    relocate(c);
  }

  template <typename T, size_t Size, typename Size_t> auto UninitializedVector<T, Size, Size_t>::relocate(size_t size) -> void {
    Assert(_size <= size, "{} <= {}", _size, size);
    memresize<KeepValues>(_data, size);
  }
}
