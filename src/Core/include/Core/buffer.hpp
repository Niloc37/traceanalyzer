#pragma once

#include "Core/debug.hpp"
#include "Core/span.hpp"
#include "Core/types.hpp"
#include "nlc/meta/units.hpp"

namespace ta::core {

  auto memmove(void *dest, const void *from, size_t size) -> void;
  auto memcpy(void *dest, const void *from, size_t size) -> void;
  [[nodiscard]] auto malloc(size_t size) -> void *;
  auto free(void *) -> void;

  // StackBuffer /////////////////////////////////////////////////////////////

  template <typename T, size_t _size, typename Size_t = size32_t> class StackBuffer final {
    public:
      using size_t = Size_t;

    public:
      StackBuffer() = default;
      StackBuffer(StackBuffer&&) = delete;
      StackBuffer(const StackBuffer&) = delete;
      auto operator=(StackBuffer&&) -> StackBuffer& = delete;
      auto operator=(const StackBuffer&) -> StackBuffer& = delete;

    public:
      [[nodiscard]] auto size() const { return _size; }
      [[nodiscard]] auto memory() const { return nlc::meta::bytes<core::size_t>(0u); }

      [[nodiscard]] auto ptr() { return reinterpret_cast<T *>(_data); }
      [[nodiscard]] auto ptr() const { return reinterpret_cast<const T *>(_data); }

      [[nodiscard]] auto data() { return core::span(ptr(), _size); }
      [[nodiscard]] auto data() const { return core::span(ptr(), _size); }

      [[nodiscard]] auto operator[](size32_t i) -> T& {
        Assert(i < _size, "{} < {}", i, _size);
        return reinterpret_cast<T*>(&_data)[i];
      }
      [[nodiscard]] auto operator[](size32_t i) const -> const T& {
        Assert(i < _size, "{} < {}", i, _size);
        return reinterpret_cast<T*>(&_data)[i];
      }

    private:
      byte _data[_size * sizeof(T)];
  };

  // HeapBuffer //////////////////////////////////////////////////////////////

  template <typename T, typename Size_t = size32_t> class HeapBuffer final {
    public:
      using size_t = Size_t;

    public:
      HeapBuffer() : _data(nullptr), _size(0u) {}
      ~HeapBuffer() { free(); }
      explicit HeapBuffer(size_t size) : _data(static_cast<T *>(core::malloc(size * sizeof(T)))) , _size(size) {}
      HeapBuffer(HeapBuffer&& rhs) { swap(rhs); }
      HeapBuffer(const HeapBuffer&) = delete;
      auto operator=(HeapBuffer&& rhs) -> HeapBuffer& { free(); swap(rhs); return *this; }
      auto operator=(const HeapBuffer&) -> HeapBuffer& = delete;

    public:
      [[nodiscard]] auto size() const { return _size; }
      [[nodiscard]] auto memory() const { return nlc::meta::bytes<core::size_t>(_size * sizeof(T)); }

      [[nodiscard]] auto ptr() { return _data; }
      [[nodiscard]] auto ptr() const { return _data; }

      [[nodiscard]] auto data() { return core::span(ptr(), _size); }
      [[nodiscard]] auto data() const { return core::span(ptr(), _size); }

      [[nodiscard]] auto operator[](size_t i) -> T& {
        Assert(i < _size, "{} < {}", i, _size);
        return _data[i];
      }
      [[nodiscard]] auto operator[](size_t i) const -> const T& {
        Assert(i < _size, "{} < {}", i, _size);
        return _data[i];
      }

      auto swap(HeapBuffer& rhs) noexcept {
        auto data = _data; _data = rhs._data; rhs._data = data;
        auto size = _size; _size = rhs._size; rhs._size = size;
      }

      [[nodiscard]] auto steal() noexcept {
        _size = 0u;
        auto data = _data;
        return data;
      }

      auto give(T* data, size_t size) noexcept {
        free();
        _size = size;
        _data = data;
      }

      auto free() {
        if (_size > 0) {
          core::free(_data);
          _size = 0;
        }
      }

    private:
      T *_data = nullptr;
      Size_t _size = 0u;
  };


  // MixedBuffer /////////////////////////////////////////////////////////////

  template <typename T, size_t Size, typename Size_t = size32_t> class MixedBuffer final {
      static_assert(Size > 0, "A Heap buffer should be used");

    public:
      using size_t = Size_t;

    public:
      MixedBuffer() : _size(Size) {}
      ~MixedBuffer() { free(); }
      explicit MixedBuffer(size_t size) : _size(size > Size ? size : Size) {
        if (!isLocal()) _data.ptr = (T *) core::malloc(_size * sizeof(T));
      }
      MixedBuffer(MixedBuffer&&) = delete;
      MixedBuffer(const MixedBuffer&) = delete;
      auto operator=(MixedBuffer&&) -> MixedBuffer& = delete;
      auto operator=(const MixedBuffer&) -> MixedBuffer& = delete;

    public:
      [[nodiscard]] auto size() const { return _size; }
      [[nodiscard]] auto memory() const { return nlc::meta::bytes<core::size_t>(isLocal() ? 0u : _size * sizeof(T)); }

      [[nodiscard]] auto ptr() -> T * { return isLocal() ? (T *) _data.data : _data.ptr; }
      [[nodiscard]] auto ptr() const -> const T * { return isLocal() ? (const T *) _data.data : _data.ptr; }

      [[nodiscard]] auto data() { return core::span(ptr(), _size); }
      [[nodiscard]] auto data() const { return core::span(ptr(), _size); }

      [[nodiscard]] auto operator[](size_t i) -> T& {
        Assert(i < _size, "{} < {}", i, _size);
        return ptr()[i];
      }
      [[nodiscard]] auto operator[](size_t i) const -> const T& {
        Assert(i < _size, "{} < {}", i, _size);
        return ptr()[i];
      }

      [[nodiscard]] auto isLocal() const { return _size <= Size; }

      [[nodiscard]] auto steal() noexcept -> T* {
        Assert(!isLocal(), "");
        _size = Size; // no need to reset ptr, it is now used as buffer
        return _data.ptr;
      }

      auto give(T* ptr, size_t size) noexcept -> void {
        Assert(size > Size, "");
        free();
        _data.ptr = ptr;
        _size = size;
      }

      auto free() -> void {
        if (!isLocal()) core::free(_data.ptr);
      }

    private:
      union {
        byte data[Size * sizeof(T)];
        T *ptr;
      } _data;
      size_t _size;
  };


  //--------------------------------------------------------------------
  // resize

  enum KeepOrDiscardValues { KeepValues, DiscardValues };

  template <KeepOrDiscardValues politic, typename T, typename S> auto memresize(HeapBuffer<T, S>& buf, size_t size) -> void {
    if (size != buf.size()) {
      auto tmp = HeapBuffer<T, S>(size);
      if constexpr (politic == KeepValues)
        core::memcpy(tmp.ptr(), buf.ptr(), (buf.size() < size ? buf.size() : size) * sizeof(T));
      buf.swap(tmp);
    }
  }

  template <KeepOrDiscardValues politic, typename T, size_t s, typename S> auto memresize(MixedBuffer<T, s, S>& buf, size_t size) -> void {
    auto oldSize = buf.size();
    if (oldSize != size) {
      if (size > s) {
        auto tmp = static_cast<T*>(core::malloc(size * sizeof(T)));
        if constexpr (politic == KeepValues)
          core::memcpy(tmp, buf.ptr(), (oldSize < size ? oldSize : size) * sizeof(T));
        buf.give(tmp, size);
      }
      else if (oldSize > s){
        auto tmp = buf.steal();
        if constexpr (politic == KeepValues)
          core::memcpy(buf.ptr(), tmp, size * sizeof(T)); // we know that oldSize > s >= size
        core::free(tmp);
      }
    }
  }

  //--------------------------------------------------------------------
  // copy
#define IMPL_NO_SIZE {                                                          \
  Assert(dest.size() >= from.size(), "{} >= {}", dest.size(), from.size());     \
  core::memcpy(dest.ptr(), from.ptr(), from.size() * sizeof(T));                \
}
#define IMPL_SIZE {                                                                                                                     \
  Assert(size <= dest.size() && size <= from.size(), "size : {}, dest.size() : {}, from.size() : {}", size, dest.size(), from.size());  \
  core::memcpy(dest.ptr(), from.ptr(), size * sizeof(T));                                                                               \
}

  template <typename T, typename S1, size_t s, typename S2> auto memcpy(HeapBuffer<T, S1>& dest, const StackBuffer<T, s, S2>& from) IMPL_NO_SIZE
  template <typename T, typename S1, size_t s, typename S2> auto memcpy(HeapBuffer<T, S1>& dest, const StackBuffer<T, s, S2>& from, size_t size) IMPL_SIZE
  template <typename T, typename S1, size_t s, typename S2> auto memcpy(HeapBuffer<T, S1>& dest, const MixedBuffer<T, s, S2>& from) IMPL_NO_SIZE
  template <typename T, typename S1, size_t s, typename S2> auto memcpy(HeapBuffer<T, S1>& dest, const MixedBuffer<T, s, S2>& from, size_t size) IMPL_SIZE
  template <typename T, typename S1, typename S2> auto memcpy(HeapBuffer<T, S1>& dest, const HeapBuffer<T, S2>& from) IMPL_NO_SIZE
  template <typename T, typename S1, typename S2> auto memcpy(HeapBuffer<T, S1>& dest, const HeapBuffer<T, S2>& from, size_t size) IMPL_SIZE
  template <typename T, typename S1, size_t s1, typename S2, size_t s2> auto memcpy(MixedBuffer<T, s1, S1>& dest, const StackBuffer<T, s2, S2>& from) IMPL_NO_SIZE
  template <typename T, typename S1, size_t s1, typename S2, size_t s2> auto memcpy(MixedBuffer<T, s1, S1>& dest, const StackBuffer<T, s2, S2>& from, size_t size) IMPL_SIZE
  template <typename T, typename S1, size_t s1, typename S2, size_t s2> auto memcpy(MixedBuffer<T, s1, S1>& dest, const MixedBuffer<T, s2, S2>& from) IMPL_NO_SIZE
  template <typename T, typename S1, size_t s1, typename S2, size_t s2> auto memcpy(MixedBuffer<T, s1, S1>& dest, const MixedBuffer<T, s2, S2>& from, size_t size) IMPL_SIZE
  template <typename T, typename S1, size_t s, typename S2> auto memcpy(MixedBuffer<T, s, S1>& dest, const HeapBuffer<T, S2>& from) IMPL_NO_SIZE
  template <typename T, typename S1, size_t s, typename S2> auto memcpy(MixedBuffer<T, s, S1>& dest, const HeapBuffer<T, S2>& from, size_t size) IMPL_SIZE
  template <typename T, typename S1, size_t s1, typename S2, size_t s2> auto memcpy(StackBuffer<T, s1, S1>& dest, const StackBuffer<T, s2, S2>& from) IMPL_NO_SIZE
  template <typename T, typename S1, size_t s1, typename S2, size_t s2> auto memcpy(StackBuffer<T, s1, S1>& dest, const StackBuffer<T, s2, S2>& from, size_t size) IMPL_SIZE
  template <typename T, typename S1, size_t s1, typename S2, size_t s2> auto memcpy(StackBuffer<T, s1, S1>& dest, const MixedBuffer<T, s2, S2>& from) IMPL_NO_SIZE
  template <typename T, typename S1, size_t s1, typename S2, size_t s2> auto memcpy(StackBuffer<T, s1, S1>& dest, const MixedBuffer<T, s2, S2>& from, size_t size) IMPL_SIZE
  template <typename T, typename S1, size_t s, typename S2> auto memcpy(StackBuffer<T, s, S1>& dest, const HeapBuffer<T, S2>& from) IMPL_NO_SIZE
  template <typename T, typename S1, size_t s, typename S2> auto memcpy(StackBuffer<T, s, S1>& dest, const HeapBuffer<T, S2>& from, size_t size) IMPL_SIZE

  //--------------------------------------------------------------------
  // move

  template <typename T, typename S1, typename S2> auto memmove(HeapBuffer<T, S1>& dest, HeapBuffer<T, S2>& from) {
    const auto size = from.size();
    dest.give(from.steal(), size);
  }

  template <typename T, typename S1, typename S2> auto memmove(HeapBuffer<T, S1>& dest, HeapBuffer<T, S2>& from, size_t) {
    const auto size = from.size();
    dest.give(from.steal(), size);
  }

  template <typename T, size_t s, typename S1, typename S2> auto memmove(HeapBuffer<T, S1>& dest, MixedBuffer<T, s, S2>& from) {
    if (from.isLocal()) {
      memcpy(dest, from);
    }
    else {
      const auto size = from.size();
      dest.give(from.steal(), size);
    }
  }

  template <typename T, size_t s, typename S1, typename S2> auto memmove(HeapBuffer<T, S1>& dest, MixedBuffer<T, s, S2>& from, size_t usefulSize) {
    if (from.isLocal()) {
      memcpy(dest, from, usefulSize);
    }
    else {
      const auto size = from.size();
      dest.give(from.steal(), size);
    }
  }

  template <typename T, size_t s, typename S1, typename S2> auto memmove(MixedBuffer<T, s, S1>& dest, HeapBuffer<T, S2>& from) {
    const auto size = from.size();
    if (size > s) {
      dest.give(from.steal(), size);
    }
    else { // dest is garanted to be of size >= s >= size
      memcpy(dest, from);
    }
  }

  template <typename T, size_t s, typename S1, typename S2> auto memmove(MixedBuffer<T, s, S1>& dest, HeapBuffer<T, S2>& from, size_t usefulSize) {
    const auto size = from.size();
    if (size > s) {
      dest.give(from.steal(), size);
    }
    else { // dest is garanted to be of size >= s >= size
      memcpy(dest, from, usefulSize);
    }
  }

  template <typename T, size_t s1, size_t s2, typename S1, typename S2> auto memmove(MixedBuffer<T, s1, S1>& dest, MixedBuffer<T, s2, S2>& from) {
    const auto size = from.size();
    if (size > s1 && size > s2) { // size <= s1 => cant receive memory, size <= s2 => no memory to steal
      dest.give(from.steal(), size);
    }
    else {
      memcpy(dest, from);
    }
  }

  template <typename T, size_t s1, size_t s2, typename S1, typename S2> auto memmove(MixedBuffer<T, s1, S1>& dest, MixedBuffer<T, s2, S2>& from, size_t usefulSize) {
    const auto size = from.size();
    if (size > s1 && size > s2) { // size <= s1 => cant receive memory, size <= s2 => no memory to steal
      dest.give(from.steal(), size);
    }
    else {
      memcpy(dest, from, usefulSize);
    }
  }

  template <typename T, size_t s, typename S1, typename S2> auto memmove(HeapBuffer<T, S1>& dest,  StackBuffer<T, s, S2>& from) { memcpy(dest, from); }
  template <typename T, size_t s, typename S1, typename S2> auto memmove(HeapBuffer<T, S1>& dest,  StackBuffer<T, s, S2>& from, size_t size) { memcpy(dest, from, size); }
  template <typename T, size_t s, typename S1, typename S2> auto memmove(StackBuffer<T, s, S1>& dest,  HeapBuffer<T, S2>& from) { memcpy(dest, from); }
  template <typename T, size_t s, typename S1, typename S2> auto memmove(StackBuffer<T, s, S1>& dest,  HeapBuffer<T, S2>& from, size_t size) { memcpy(dest, from, size); }
  template <typename T, size_t s1, size_t s2, typename S1, typename S2> auto memmove(MixedBuffer<T, s1, S1>& dest,  StackBuffer<T, s2, S2>& from) { memcpy(dest, from); }
  template <typename T, size_t s1, size_t s2, typename S1, typename S2> auto memmove(MixedBuffer<T, s1, S1>& dest,  StackBuffer<T, s2, S2>& from, size_t size) { memcpy(dest, from, size); }
  template <typename T, size_t s1, size_t s2, typename S1, typename S2> auto memmove(StackBuffer<T, s1, S1>& dest,  MixedBuffer<T, s2, S2>& from) { memcpy(dest, from); }
  template <typename T, size_t s1, size_t s2, typename S1, typename S2> auto memmove(StackBuffer<T, s1, S1>& dest,  MixedBuffer<T, s2, S2>& from, size_t size) { memcpy(dest, from, size); }
  template <typename T, size_t s1, size_t s2, typename S1, typename S2> auto memmove(StackBuffer<T, s1, S1>& dest,  StackBuffer<T, s2, S2>& from) { memcpy(dest, from); }
  template <typename T, size_t s1, size_t s2, typename S1, typename S2> auto memmove(StackBuffer<T, s1, S1>& dest,  StackBuffer<T, s2, S2>& from, size_t size) { memcpy(dest, from, size); }

  //--------------------------------------------------------------------
  // swap

  // missing ?

  template <typename T, size_t s, typename S1, typename S2> auto memswap(StackBuffer<T, s, S1>& lhs, StackBuffer<T, s, S2>& rhs) {
    StackBuffer<T, s> tmp;
    memcpy(tmp, lhs);
    memcpy(lhs, rhs);
    memcpy(rhs, tmp);
  }

  template <typename T, size_t s, typename S1, typename S2> auto memswap(MixedBuffer<T, s, S1>& lhs, MixedBuffer<T, s, S2>& rhs) {
    constexpr auto size = sizeof(MixedBuffer<T, s, S1>);
    static_assert(size == sizeof(MixedBuffer<T, s, S2>));
    StackBuffer<char, size> tmp;
    core::memcpy(tmp.ptr(), &lhs, size);
    core::memcpy(&lhs, &rhs, size);
    core::memcpy(&rhs, tmp.ptr(), size);
  }

  template <typename T, typename S1, typename S2> auto memswap(HeapBuffer<T, S1>& lhs, HeapBuffer<T, S2>& rhs) {
    lhs.swap(rhs);
  }

  //--------------------------------------------------------------------


}
