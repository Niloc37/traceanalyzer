#pragma once

#include "nlc/meta/basics.hpp"
#include "nlc/meta/traits.hpp"

namespace ta::core {

  template <typename T> class unique_ptr final {
    public:
      unique_ptr() = default;
      ~unique_ptr() { delete _ptr; }

    public:
      unique_ptr(const unique_ptr&) = delete;
      auto operator=(const unique_ptr&) = delete;

    public: // constructors
      template <typename Q> explicit unique_ptr(Q *ptr) : _ptr { ptr } {
        static_assert(nlc::meta::disjonction<nlc::meta::is_same<T, Q>, nlc::meta::has_virtual_destructor<T>>);
      }

      template <typename Q> unique_ptr(unique_ptr<Q>&& rhs) : _ptr(rhs.steal()) {
        static_assert(nlc::meta::disjonction<nlc::meta::is_same<T, Q>, nlc::meta::has_virtual_destructor<T>>);
      }

    public: // assignations
      [[nodiscard]] auto substitute(T *ptr) {
        auto old = _ptr;
        _ptr = ptr;
        return old;
      }
      auto reset(T *ptr = nullptr) { delete substitute(ptr); }

      template <typename Q> auto operator=(Q *ptr) -> unique_ptr& {
        static_assert(nlc::meta::disjonction<nlc::meta::is_same<T, Q>, nlc::meta::has_virtual_destructor<T>>);
        reset(ptr);
        return *this;
      }

      template <typename Q> auto operator=(unique_ptr<Q>&& rhs) -> unique_ptr& {
        operator=(rhs.steal());
        return *this;
      }

    public:
      [[nodiscard]] auto operator*() -> T& { return *_ptr; }
      [[nodiscard]] auto operator*() const -> const T& { return *_ptr; }
      [[nodiscard]] auto operator->() -> T * { return _ptr; }
      [[nodiscard]] auto operator->() const -> const T * { return _ptr; }

    public:
      [[nodiscard]] auto get() const -> T * { return _ptr; }
      [[nodiscard]] auto steal() -> T * {
        auto res = _ptr;
        _ptr = nullptr;
        return res;
      }

    private:
      T *_ptr = nullptr;
  };

  template <typename T, typename...Args> [[nodiscard]] decltype(auto) make_unique(Args&& ...args) {
    return unique_ptr<T>(new T(nlc::forward<Args>(args)...));
  }
}
