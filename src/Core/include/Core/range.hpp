#pragma once

#include "nlc/meta/basics.hpp"
#include "nlc/meta/traits.hpp"

namespace ta::core {
  template <typename underlying_t, typename access_t = underlying_t>
  class integer_iterator final {
    public:
      integer_iterator(underlying_t v) : _v { v } {}
      integer_iterator(const integer_iterator&) = default;
      auto operator=(const integer_iterator&) -> integer_iterator& = default;
    public:
      [[nodiscard]] auto operator*() { return access_t { _v }; }
      [[nodiscard]] auto operator!=(const integer_iterator& it) { return _v != it._v; }
      auto operator++() -> integer_iterator { return _v++; }
      auto operator++(int) -> integer_iterator& {
        ++_v;
        return this;
      }
    private:
      underlying_t _v;
  };

  /**
   * Range is syntactic sugar in order to iterate over integers using for-range syntax
   *
   * usage :
   * for (const auto i : Range(0u, 5u)) {
   *   std::cout << i << " ";
   * }
   * // displays "0 1 2 3 4 " on output
   *
   * @tparam T : Type of the index of the loop
   */
  template <typename T, typename Access = T> class Range final {
    public:
      using iterator = integer_iterator<T, Access>;

    public:
      [[nodiscard]] auto begin() { return _begin; }
      [[nodiscard]] auto end() { return _end; }

    public:
      template <typename Q> Range(Q begin, T end)
          : _begin { begin < end ? begin : end }
          , _end { end } {
      }

    private:
      iterator _begin;
      iterator _end;
  };
  template <typename T, typename Q> Range(T&&, Q&&) -> Range<nlc::meta::rm_const_ref<Q>>;
} // ta::core
