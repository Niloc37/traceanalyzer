#pragma once

#include "Core/buffer.hpp"
#include "Core/span.hpp"
#include "Core/uniquePointer.hpp"
#include "nlc/meta/units.hpp"

namespace ta::core {

  using size_t = unsigned long int;

  auto isValidUtf8(const char *begin, const char *end) -> bool;

  //-----------------------------------------------------------

  class Char final {
    public:
      using underlying_t = int;

    public:
      Char() = delete;
      Char(Char&&) = default;
      Char(const Char&) = default;
      auto operator=(Char&&) -> Char& = default;
      auto operator=(const Char&) -> Char& = default;
      explicit Char(underlying_t data) : _data(data) {}
      Char(char c) : _data(c) {}

    public:
      auto codepoint() const { return _data; }
      auto size() const -> size_t;

    public:
      auto _catToString(char *) const -> char*;

    private:
      underlying_t _data;
  };

#define OP(op)                                                                                \
  inline auto operator op(Char lhs, Char rhs) { return lhs.codepoint() op rhs.codepoint(); }  \
  inline auto operator op(Char lhs, char rhs) { return lhs.codepoint() op rhs; }              \
  inline auto operator op(char lhs, Char rhs) { return lhs op rhs.codepoint(); }

  OP(==) OP(!=) OP(<) OP(>) OP(<=) OP(>=)
#undef OP

  //-----------------------------------------------------------

  class StringIterator final {
    public:
      explicit StringIterator(const char* ptr) : _ptr(ptr) {}

    public:
      auto operator++() -> StringIterator&;
      auto operator++(int) -> StringIterator;
      [[nodiscard]] auto operator*() -> Char;
      [[nodiscard]] auto operator!=(const StringIterator&) -> bool;
      [[nodiscard]] auto operator==(const StringIterator&) -> bool;
      auto ptr() const -> const char* { return _ptr; }
    private:
      const char* _ptr;
  };

  //-----------------------------------------------------------

  inline auto operator-(StringIterator lhs, StringIterator rhs) -> size_t { return lhs.ptr() - rhs.ptr(); }

  //-----------------------------------------------------------

  class StringView final {
    public:
      using iterator = StringIterator;

    public:
      StringView() = default;
      StringView(StringView&&) = default;
      StringView(const StringView&) = default;
      auto operator=(StringView&&) -> StringView& = default;
      auto operator=(const StringView&) -> StringView& = default;

    public:
      StringView(const char*);
      StringView(const char*, size_t size);
      StringView(iterator _begin, iterator _end) : StringView(_begin.ptr(), _end - _begin) {}
      auto operator=(const char*) -> StringView&;
      explicit StringView(const core::span<const char>& span) : StringView(span.ptr(), span.size()) {}

    public: // TODO delete
      StringView(std::string_view str) : StringView(str.data(), str.size()) {}
      StringView(const std::string& str) : StringView(str.c_str()) {}

    public:
      [[nodiscard]] auto size() const { return _size; }
      [[nodiscard]] auto empty() const -> bool { return size() == 0u; }
      [[nodiscard]] auto len() const -> size_t;
      [[nodiscard]] auto data() const { return core::span<const char>(_data, _size); }
      [[nodiscard]] auto ptr() const { return _data; }

    public:
      [[nodiscard]] auto begin() const { return iterator(_data); }
      [[nodiscard]] auto end() const { return iterator(_data + _size); }

    private:
      size_t _size = 0;
      const char* _data = "";
  };

  //-----------------------------------------------------------

  [[nodiscard]] auto compare(const StringView&, const StringView&) -> int;
  [[nodiscard]] auto operator==(const StringView&, const StringView&) -> bool;
  [[nodiscard]] auto operator<(const StringView&, const StringView&) -> bool;
  [[nodiscard]] auto operator>(const StringView&, const StringView&) -> bool;
  [[nodiscard]] inline auto operator!=(const StringView& lhs, const StringView& rhs) -> bool { return !(lhs == rhs); }
  [[nodiscard]] inline auto operator<=(const StringView& lhs, const StringView& rhs) -> bool { return !(lhs > rhs); }
  [[nodiscard]] inline auto operator>=(const StringView& lhs, const StringView& rhs) -> bool { return !(lhs < rhs); }
  [[nodiscard]] auto isPrefix(const StringView& prefix, const StringView& str) -> bool;

  //-----------------------------------------------------------

  struct SplitOptions {
    StringView breakers = "";
    StringView ignored = " \t";
    StringView grouper = "\"\"";
  };

  [[nodiscard]] auto find(StringView, Char) -> StringIterator;
  [[nodiscard]] auto findLast(StringView, Char) -> StringIterator;
  [[nodiscard]] auto contains(StringView, Char) -> bool;
  auto extractFront(StringView& str, SplitOptions options = SplitOptions()) -> StringView;
  [[nodiscard]] auto getFront(StringView str, SplitOptions options = SplitOptions()) -> StringView;
  [[nodiscard]] auto frontChar(StringView) -> Char;
  [[nodiscard]] auto backChar(StringView) -> Char;
  [[nodiscard]] auto popFrontChar(StringView) -> StringView;

  class SplitIterator final {
      friend class Split;
      class EndedSplitIterator final {};
    private:
      SplitIterator(StringView str, const SplitOptions& options) : _str(str), _options(options) { if (_str != "") operator++(); }

    public:
      auto operator++() -> SplitIterator& { _cur = extractFront(_str, _options); return *this; }
      [[nodiscard]] auto operator++(int) -> SplitIterator { auto res = *this; operator++(); return res; }
      [[nodiscard]] auto operator*() const -> StringView { return _cur; }
      [[nodiscard]] auto operator->() const -> const StringView* { return &_cur; }
      [[nodiscard]] auto operator!=(const EndedSplitIterator&) -> bool { return _cur != ""; }
    private:
      StringView _str;
      StringView _cur;
      const SplitOptions& _options;
  };

  class Split final {
    public:
      Split(const StringView& str, const SplitOptions& options = SplitOptions()) : _str(str), _options(options) {}

      [[nodiscard]] auto begin() const { return SplitIterator(_str, _options); }
      [[nodiscard]] auto end() const { return SplitIterator::EndedSplitIterator {}; }

    private:
      StringView _str;
      SplitOptions _options;
  };

  [[nodiscard]] auto removeQuotes(StringView str, StringView quotes) -> StringView;
  [[nodiscard]] auto toInt(StringView, int base = 10) -> int;
  [[nodiscard]] auto toFloat(StringView) -> float;
  [[nodiscard]] auto toDouble(StringView) -> double;

  //-----------------------------------------------------------

  class String final {
    public:
      using iterator = StringIterator;

    public:
      String();
      ~String() = default;
      String(String&& rhs) { memmove(_data, rhs._data); }
      String(const String& rhs) : String(rhs.view()) {}
      auto operator=(String&& rhs) -> String& { memmove(_data, rhs._data); return *this; }
      auto operator=(const String& rhs) -> String& { return operator=(rhs.view()); }

    public:
      String(const StringView&);
      auto operator=(const StringView&) -> String&;
      auto view() const -> StringView { return StringView(_data.ptr(), size()); }
      operator StringView() const { return view(); }

    public:
      String(const char* str) : String(StringView(str)) {}
      auto operator=(const char * str) -> String& { return operator=(StringView(str)); }

    public:
      [[nodiscard]] auto size() const -> size_t { return _data.size(); }
      [[nodiscard]] auto empty() const -> bool { return size() == 0u; }
      [[nodiscard]] auto len() const -> size_t;
      [[nodiscard]] auto memory() const -> nlc::meta::bytes<size_t> { return _data.memory(); }
      [[nodiscard]] auto ptr() const { return _data.ptr(); }

    public:
      [[nodiscard]] auto begin() const { return iterator(_data.ptr()); }
      [[nodiscard]] auto end() const { return iterator(_data.ptr() + size()); }

    private:
      core::HeapBuffer<char> _data;
  };

  //-----------------------------------------------------------

  template <size_t capacity>
  class StackString final {
    public:
      using iterator = StringIterator;

    public:
      StackString() : _size(0) {}
      ~StackString() = default;
      StackString(StackString&& rhs) : _size(rhs._size) { memmove(_data, rhs._data); _size = 0; }
      StackString(const StackString& rhs) : _size(rhs._size) { memcpy(_data, rhs._data, _size); }
      auto operator=(StackString&& rhs) -> StackString& { _size = rhs._size; memmove(_data, rhs._data); _size=0; }
      auto operator=(const StackString& rhs) -> StackString& { _size = rhs._size; memcpy(_data, rhs._data, _size); }

    public:
      StackString(const StringView&);
      auto operator=(const StringView&) -> StackString&;
      auto view() const { return StringView(_data.ptr(), size()); }
      operator StringView() const { return view(); }

    public:
      StackString(const char* str) : StackString(StringView(str)) {}
      auto operator=(const char * str) -> StackString& { return operator=(StringView(str)); }

    public:
      [[nodiscard]] auto size() const -> size_t { return _size; }
      [[nodiscard]] auto empty() const -> bool { return size() == 0u; }
      [[nodiscard]] auto len() const -> size_t { return view().len(); };
      [[nodiscard]] auto memory() const -> nlc::meta::bytes<size_t> { return _data.memory(); }
      [[nodiscard]] auto ptr() const { return _data.ptr(); }

    public:
      [[nodiscard]] auto begin() const { return iterator(_data.ptr()); }
      [[nodiscard]] auto end() const { return iterator(_data.ptr() + size()); }

    private:
      core::StackBuffer<char, capacity> _data;
      size_t _size;
  };

  template <size_t capacity> StackString<capacity>::StackString(const StringView& str) : _size(str.size()) {
    Assert(str.size() <= capacity, "{} <= {}", str.size(), capacity);
    memcpy((void*)_data.ptr(), (void*)str.ptr(), _size);
  }

  template <size_t capacity> auto StackString<capacity>::operator=(const StringView& str) -> StackString& {
    Assert(str.size() <= capacity, "{} <= {}", str.size(), capacity);
    _size = str.size();
    memcpy((void*)_data.ptr(), (void*)str.ptr(), _size);
  }

  //-----------------------------------------------------------

  auto operator+(StringView lhs, StringView rhs) -> String;

  //-----------------------------------------------------------

  class CString final {
    public:
      CString(StringView str) : _buf(str.size() + 1) {
        core::memcpy(_buf.ptr(), str.ptr(), str.size());
        _buf[str.size()] = '\0';
      }

      operator const char* () { return _buf.ptr(); }

    private:
      HeapBuffer<char> _buf;
  };

  //-----------------------------------------------------------

}

template <> struct fmt::formatter<ta::core::Char> : formatter<string_view> {
  template <typename FormatContext> auto format(const ta::core::Char& c, FormatContext& ctx) {
    ta::core::StackBuffer<char, sizeof(ta::core::Char::underlying_t)> buf;
    auto end = c._catToString(buf.ptr());
    return formatter<string_view>::format(string_view(buf.ptr(), end - buf.ptr()), ctx);
  }
};

template <> struct fmt::formatter<ta::core::StringView> : formatter<string_view> {
  template <typename FormatContext> auto format(const ta::core::StringView& v, FormatContext& ctx) {
    return formatter<string_view>::format(string_view(v.ptr(), v.size()), ctx);
  }
};

template <> struct fmt::formatter<ta::core::String> : formatter<ta::core::StringView> {
  template <typename FormatContext> auto format(const ta::core::String& v, FormatContext& ctx) {
    return formatter<ta::core::StringView>::format(v, ctx);
  }
};

template <size_t capacity> struct fmt::formatter<ta::core::StackString<capacity>> : formatter<ta::core::StringView> {
  template <typename FormatContext> auto format(const ta::core::StackString<capacity>& v, FormatContext& ctx) {
    return formatter<ta::core::StringView>::format(v, ctx);
  }
};

