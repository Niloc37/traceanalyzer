#pragma once

namespace ta::core {

  enum class CompareResult {
      Less, More, Equal
  };

  auto compare() { return CompareResult::Equal; }

#define DEFINE_COMPARE(type)                        \
  auto compare(const type& lhs, const type& rhs) {  \
    if (lhs < rhs) return CompareResult::Less;      \
    if (rhs<lhs) return CompareResult::More;        \
    return CompareResult::Equal;                    \
  }

  DEFINE_COMPARE(char)
  DEFINE_COMPARE(unsigned char)
  DEFINE_COMPARE(short)
  DEFINE_COMPARE(unsigned short)
  DEFINE_COMPARE(int)
  DEFINE_COMPARE(unsigned int)
  DEFINE_COMPARE(long int)
  DEFINE_COMPARE(unsigned long int)
  DEFINE_COMPARE(float)
  DEFINE_COMPARE(double)
  DEFINE_COMPARE(long double)
#undef DEFINE_COMPARE

  template <typename T, typename...Q> constexpr auto compare(const T& lhs, const T& rhs, const Q& ...others) {
    if (const auto c = compare(lhs, rhs); c != CompareResult::Equal)
      return c;
    return compare(others...);
  }

} // namespace ta::core
