#pragma once

#include "nlc/meta/id.hpp"
#include "nlc/meta/units.hpp"

template <typename IdDescr, typename Type>
struct fmt::formatter<nlc::meta::id<IdDescr, Type>> : formatter<string_view> {
  using Id = nlc::meta::id<IdDescr, Type>;

  template <typename FormatContext> auto format(const Id& id, FormatContext& ctx) {
    if constexpr (nlc::meta::impl::is_invalid_value_defined<typename Id::descriptor_t>())
      if (id.isInvalid())
        return formatter<string_view>::format("invalid", ctx);
    return formatter<string_view>::format(fmt::format("{}", id.value()), ctx);
  }
};

template <typename T, typename Dim>
struct fmt::formatter<nlc::meta::unit<T, Dim>> : formatter<T> {
  template <typename FormatContext> auto format(const nlc::meta::unit<T, Dim>& v, FormatContext& ctx) {
    return formatter<T>::format(v.v, ctx);
  }
};

template <typename T1, typename T2> struct fmt::formatter<std::pair<T1, T2>> : formatter<string_view> {
  template <typename FormatContext> auto format(const std::pair<T1, T2>& p, FormatContext& ctx) {
    return formatter<string_view>::format(fmt::format("({}, {})", p.first, p.second), ctx);
  }
};

