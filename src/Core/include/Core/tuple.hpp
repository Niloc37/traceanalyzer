#pragma once

#include "Core/types.hpp"
#include "nlc/meta/basics.hpp"
#include "nlc/meta/list.hpp"

namespace ta::core {

  // tuple ------------------------------------------------------------------------

  template <typename...Ts>
  class tuple final {
    public:
      static constexpr auto size = 0;
      using types = nlc::meta::list<>;
    public:
      template <size_t i> auto get() const { static_assert(i < size); }
      template <size_t i> auto get() { static_assert(i < 0); }
  };

  // -------------------------------------------

  template <typename T, typename...Ts>
  class tuple<T, Ts...> final {
    public:
      static constexpr auto size = 1 + sizeof...(Ts);
      using types = nlc::meta::list<T, Ts...>;

    public:
      tuple() = default;
      template <typename Arg, typename...Args> tuple(Arg&& first, Args&&...next)
          : _first(nlc::forward<Arg>(first))
          , _next(nlc::forward<Args>(next)...) {
      }

    public:
      template <size_t i> decltype(auto) get() const {
        static_assert(i < size);
        if constexpr (i == 0)
          return (_first);
        else
          return _next.template get<i - 1>();
      }

      template <size_t i> decltype(auto) get() {
        static_assert(i < size);
        if constexpr (i == 0)
          return (_first);
        else
          return _next.template get<i - 1>();
      }

    private:
      T _first;
      tuple<Ts...> _next;
  };

  // -------------------------------------------

  template <typename T>
  class tuple<T> final {
    public:
      static constexpr auto size = 1;
      using types = nlc::meta::list<>;

    public:
      tuple() = default;
      template <typename Arg> explicit tuple(Arg&& value) : _data(nlc::forward<Arg>(value)) {}

      template <size_t i> decltype(auto) get() {
        static_assert(i < size);
        return (_data);
      }
      template <size_t i> decltype(auto) get() const {
        static_assert(i < size);
        return (_data);
      }

    private:
      T _data;
  };

  // -------------------------------------------

  template <typename... Ts> tuple(Ts...) -> tuple<Ts...>;

  namespace impl {
    template <typename T, template <typename...> typename f> struct tuple_from_type_list {};
    template <typename...Ts, template<typename...> typename f> struct tuple_from_type_list<nlc::meta::list<Ts...>, f> { using type = tuple<f<Ts>...>; };
  }
  template <typename T, template<typename...> typename f = nlc::meta::identity> using tuple_from_type_list = typename impl::tuple_from_type_list<T, f>::type;

  // pair -------------------------------------------------------------------------

  template <typename T1, typename T2>
  struct pair final {
    T1 first;
    T2 second;

    pair() = default;
    pair(pair&&) = default;
    pair(const pair&) = default;
    template <typename Q1, typename Q2> pair(Q1&& arg1, Q2&& arg2)
        : first(nlc::forward<Q1>(arg1)), second(nlc::forward<Q2>(arg2)) {
    }
    auto operator=(pair&&) -> pair& = default;
    auto operator=(const pair&) -> pair& = default;
  };

  // get --------------------------------------------------------------------------

  template <size_t index, typename...Ts> decltype(auto) get(tuple<Ts...>& t) { return t.template get<index>(); }
  template <size_t index, typename...Ts> decltype(auto) get(tuple<Ts...>&& t) { return nlc::move(t.template get<index>()); }
  template <size_t index, typename...Ts> decltype(auto) get(const tuple<Ts...>& t) { return t.template get<index>(); }

  // apply ------------------------------------------------------------------------

  namespace impl {
    template <typename F, typename T, auto...vs> auto apply_on_tuple_impl(F&& f, T&& t, nlc::meta::list<nlc::meta::Value<vs>...>) {
      return f(core::get<vs>(nlc::forward<T>(t))...);
    }
  }

  template <typename F, typename...Ts> constexpr auto apply(F&& f, tuple<Ts...>& t) {
    return impl::apply_on_tuple_impl(f, t, nlc::meta::range<0, tuple<Ts...>::size>());
  }
  template <typename F, typename...Ts> constexpr auto apply(F&& f, tuple<Ts...>&& t) {
    return impl::apply_on_tuple_impl(nlc::move(f), nlc::move(t), nlc::meta::range<0, tuple<Ts...>::size>());
  }
  template <typename F, typename...Ts> constexpr auto apply(F&& f, const tuple<Ts...>& t) {
    return impl::apply_on_tuple_impl(f, t, nlc::meta::range<0, tuple<Ts...>::size>());
  }

  // forEach ----------------------------------------------------------------------

  template <typename F, typename...T> constexpr auto forEach(tuple<T...>& t, F&& f) {
    nlc::meta::for_each<nlc::meta::range<0, tuple<T...>::size>>([&](auto i) { f(core::get<i>(t)); });
  }
  template <typename F, typename...T> constexpr auto forEach(tuple<T...>&& t, F&& f) {
    nlc::meta::for_each<nlc::meta::range<0, tuple<T...>::size>>([&](auto i) { f(nlc::move(core::get<i>(t))); });
  }
  template <typename F, typename...T> constexpr auto forEach(const tuple<T...>& t, F&& f) {
    nlc::meta::for_each<nlc::meta::range<0, tuple<T...>::size>>([&](auto i) { f(core::get<i>(t)); });
  }
}
