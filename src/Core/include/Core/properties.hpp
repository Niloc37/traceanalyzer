#pragma once

#include "Core/string.hpp"
#include "Core/vector.hpp"

#include <map>

namespace ta::core {

  struct Property {
    String name;
    String defaultValue;
    String customValue;
  };

  class PropertyBank {
    public:
      auto set(StringView name, StringView value) -> void;
      auto setDefault(StringView name, StringView value) -> void;
      auto setDefaultAndReset(StringView name, StringView value) -> void;
      auto reset(StringView name) -> void;

    public:
      [[nodiscard]] auto isSet(StringView name) const -> bool;
      [[nodiscard]] auto get(StringView name) const -> StringView;
      [[nodiscard]] auto getDefault(StringView name) const -> StringView;
      [[nodiscard]] auto list() const -> Vector<Property>;

    private:
      auto _get(StringView name) -> Property&;

    private:
      std::map<String, Property> _data;
  };

}
