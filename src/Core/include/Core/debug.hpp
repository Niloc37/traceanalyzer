#pragma once

#include "nlc/meta/basics.hpp"
#include "spdlog/spdlog.h"

#include <string_view>

namespace ta::core {
#define DO_NOTHING ((void)0)

  template <typename...T>
  auto do_nothing(T&&...args) { (((void)nlc::forward<T>(args), ...)); }

#define STRINGIFY_IMPL(arg) #arg
#define STRINGIFY(arg) STRINGIFY_IMPL(arg)

// Debug only shortcut

#ifndef TA_DEBUG
#define ONLY_IN_DEBUG(arg) DO_NOTHING
#else
#define ONLY_IN_DEBUG(arg) arg
#endif

// LOGGING ///////////////////////////////////////

  using verbose_level = spdlog::level::level_enum;

  [[nodiscard]] auto get_stderr() -> spdlog::logger&;

  auto set_verbose_level(verbose_level lvl) -> void;
  auto set_verbose_level(std::string_view lvl) -> void;

  auto str_to_verbose_level(std::string_view str) -> verbose_level;

#ifndef TA_DEBUG
#define LOG_DEBUG(...) ::ta::core::do_nothing(__VA_ARGS__)
#define LOG_INFO(...) ::ta::core::do_nothing(__VA_ARGS__)
#define LOG_WARNING(...) ::ta::core::do_nothing(__VA_ARGS__)
#else
#define LOG_DEBUG(...) ::ta::core::get_stderr().debug(__VA_ARGS__)
#define LOG_INFO(...) ::ta::core::get_stderr().info(__VA_ARGS__)
#define LOG_WARNING(...) ::ta::core::get_stderr().warn(__VA_ARGS__)
#endif
#define LOG_ERROR(...) ::ta::core::get_stderr().error(__VA_ARGS__)
#define LOG_CRITICAL(...) ::ta::core::get_stderr().critical(__VA_ARGS__)

// ASSERTS ///////////////////////////////////////

  enum class debug_level : char {
    parano,
    debug,
    critical,
  };

#ifndef TA_DEBUG
  inline auto set_debug_level(debug_level) {}
  inline auto get_debug_level() { return debug_level::critical; }
#else
  auto set_debug_level(debug_level) -> void;
  auto get_debug_level() -> debug_level;
#endif

  inline auto operator <(debug_level lhs, debug_level rhs) -> bool {
    return static_cast<char>(lhs) < static_cast<char>(rhs);
  }

#ifdef TA_DEBUG
  [[nodiscard]] auto new_assert_id() -> int;
  auto assert_occured(int) -> void;
#endif

#ifndef TA_DEBUG
#define AssertParano(cond, ...) DO_NOTHING
#define Assert(cond, ...) DO_NOTHING
#define AssertWarning(cond, ...) DO_NOTHING
#define AssertNotReached(...) DO_NOTHING

#define CriticalAssert(cond, ...)                                                         \
if (cond) {}                                                                              \
else {                                                                                    \
  LOG_CRITICAL("Assert " __FILE__ "(" STRINGIFY(__LINE__) ") : \"" #cond "\" "            \
      __VA_ARGS__);                                                                       \
  abort();                                                                                \
}

#else

#define AssertParano(cond, ...)                                                           \
if (ta::core::debug_level::parano < ta::core::get_debug_level() || (cond)) {}             \
else {                                                                                    \
  LOG_ERROR("Assert at " __FILE__ ":" STRINGIFY(__LINE__) " : \"" #cond "\" " __VA_ARGS__); \
  static auto id = ::ta::core::new_assert_id();                                           \
  ta::core::assert_occured(id);                                                           \
}

#define Assert(cond, ...)                                                                 \
if (::ta::core::debug_level::debug < ::ta::core::get_debug_level() || (cond)) {}          \
else {                                                                                    \
  LOG_ERROR("Assert at " __FILE__ ":" STRINGIFY(__LINE__) " : \"" #cond "\" " __VA_ARGS__); \
  static auto id = ::ta::core::new_assert_id();                                           \
  ::ta::core::assert_occured(id);                                                         \
}

#define AssertWarning(cond, ...)                                                          \
if (::ta::core::debug_level::debug < ::ta::core::get_debug_level() || (cond)) {}          \
else { LOG_WARNING("AssertWarning at " __FILE__ ":" STRINGIFY(__LINE__) " : \"" #cond "\" " \
    __VA_ARGS__);                                                                         \
}

#define AssertNotReached(...)                                                             \
if (::ta::core::debug_level::debug < ::ta::core::get_debug_level()) {}                    \
else {                                                                                    \
  LOG_ERROR("AssertNotReached at " __FILE__ ":" STRINGIFY(__LINE__) " : " __VA_ARGS__);     \
  static auto id = ::ta::core::new_assert_id();                                           \
  ::ta::core::assert_occured(id);                                                         \
}

#define CriticalAssert(cond, ...)                                                         \
if (cond) {}                                                                              \
else {                                                                                    \
  LOG_CRITICAL("Assert at " __FILE__ ":" STRINGIFY(__LINE__) " : \"" #cond "\" "            \
      __VA_ARGS__);                                                                       \
  static auto id = ::ta::core::new_assert_id();                                           \
  ::ta::core::assert_occured(id);                                                         \
}

#endif

}
