#include "Core/buffer.hpp"

namespace ta::core {

  auto memmove(void *dest, const void *from, unsigned long int size) -> void {
    if (size > 0) {
      Assert(dest != nullptr && from != nullptr, "{}, {}", dest, from);
      std::memmove(dest, from, size);
    }
  }

  auto memcpy(void *dest, const void *from, unsigned long int size) -> void {
    if (size > 0) {
      Assert(dest != nullptr && from != nullptr, "{}, {}", dest, from);
      std::memcpy(dest, from, size);
    }
  }

  auto malloc(size_t size) -> void * {
    if (size == 0u)
      return nullptr;
    const auto res = std::malloc(size);
    Assert(res != nullptr, "");
    return res;
  }

  auto free(void *ptr) -> void {
    return std::free(ptr);
  }
}
