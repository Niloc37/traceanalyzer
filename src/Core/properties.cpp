#include "Core/properties.hpp"

namespace ta::core {

  //---------------------------------------------------

  auto PropertyBank::set(StringView name, StringView value) -> void {
    _get(name).customValue = value;
  }

  //---------------------------------------------------

  auto PropertyBank::setDefault(StringView name, StringView value) -> void {
    _get(name).defaultValue = value;
  }

  //---------------------------------------------------

  auto PropertyBank::setDefaultAndReset(StringView name, StringView value) -> void {
    auto& p = _get(name);
    p.defaultValue = value;
    p.customValue = value;
  }

  //---------------------------------------------------

  auto PropertyBank::reset(StringView name) -> void {
    auto& p = _get(name);
    p.customValue = p.defaultValue;
  }

  //---------------------------------------------------

  auto PropertyBank::isSet(StringView name) const -> bool {
    const auto it = _data.find(name);
    if (it == _data.end())
      return false;
    return it->second.defaultValue != it->second.customValue;
  }

  //---------------------------------------------------

  auto PropertyBank::get(StringView name) const -> StringView {
    const auto it = _data.find(name);
    if (it == _data.end())
      return "";
    return it->second.customValue;
  }

  //---------------------------------------------------

  auto PropertyBank::getDefault(StringView name) const -> StringView {
    const auto it = _data.find(name);
    if (it == _data.end())
      return "";
    return it->second.defaultValue;
  }

  //---------------------------------------------------

  auto PropertyBank::list() const -> Vector<Property> {
    auto res = Vector<Property> {};
    res.reserve(_data.size());

    for (const auto& it : _data)
      res.emplace_back(it.second);

    return res;
  }

  //---------------------------------------------------

  auto PropertyBank::_get(StringView name) -> Property& {
    auto [it, inserted] = _data.emplace(name, Property {});
    if (inserted)
      it->second.name = name;
    return it->second;
  }

  //---------------------------------------------------

}
