#include "Core/debug.hpp"

#include <atomic>
#include <csignal>
#include <set>
#include "spdlog/sinks/stdout_color_sinks.h"

namespace ta::core {

  struct SetLoggerFormat {
    SetLoggerFormat() {
      spdlog::set_pattern("[%^%L %t %H:%M:%S%$] %v");
    }
  };

  auto get_stderr() -> spdlog::logger& {
    static auto logger = spdlog::stderr_color_mt("stderr");
    static SetLoggerFormat format;
    return *logger;
  }

  auto console = spdlog::stderr_color_mt("console");

  auto str_to_verbose_level(std::string_view str) -> verbose_level {
    if (str == "trace")
      return verbose_level::trace;
    else if (str == "debug")
      return verbose_level::debug;
    else if (str == "info")
      return verbose_level::info;
    else if (str == "warning")
      return verbose_level::warn;
    else if (str == "error")
      return verbose_level::err;
    else if (str == "critical")
      return verbose_level::critical;
    else if (str == "off")
      return verbose_level::off;
    else
      return static_cast<verbose_level>(-1);
  }

  auto set_verbose_level(verbose_level lvl) -> void {
    if (lvl < 1 || lvl > 5)
      spdlog::error("Verbose level {} is not supported\n", static_cast<int>(lvl));
#ifndef TA_DEBUG
      else if (lvl < 5)
        spdlog::error("Verbose level {} is not supported without debug mode\n", static_cast<int>(lvl));
#endif
    else
      spdlog::set_level(lvl);
  }

  auto set_verbose_level(std::string_view lvl_str) -> void {
    auto lvl = str_to_verbose_level(lvl_str);
    if (lvl < 1 || lvl > 5)
      spdlog::error("Verbose level {} is not supported", lvl_str);
#ifndef TA_DEBUG
      else if (lvl < 5)
        spdlog::error("Verbose level {} is not supported without debug mode", lvl_str);
#endif
    else
      spdlog::set_level(lvl);
  }

#ifdef TA_DEBUG
  static auto debug_lvl = debug_level::debug;

  auto set_debug_level(debug_level lvl) -> void {
    debug_lvl = lvl;
  }

  auto get_debug_level() -> debug_level {
    return debug_lvl;
  }

  auto new_assert_id() -> int {
    static std::atomic<int> next = 0;
    return next++;
  }

  auto assert_occured(int id) -> void {
    static std::set<int> ignored;
    static std::mutex mtx;

    auto lck = std::unique_lock(mtx);
    if (ignored.find(id) == ignored.end()) {
      volatile auto ignore = false;
      std::raise(SIGINT);
      if (ignore)
        ignored.insert(id);
    }
  }

#endif
}
