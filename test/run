#!/bin/bash

src_dir=$(realpath $(dirname $0)/..) # Assume we are in test/run
bin_dir="$src_dir/bin/test"

mkdir -p $bin_dir

# Load function print_info, print_warning, print_error and print_ok
source $src_dir/utils/misc.sh

available_options="
  cmake
  build_for_tests
  compilation
"
# module_generator performance pptrace static

function usage {
  >&2 echo "test/run [[opts...] test] ..."
  >&2 echo "Available options are :"
  >&2 echo "  -l : dump to logs"
  >&2 echo "  -s : disable verbose (by default)"
  >&2 echo "  -v : enable verbose"
  >&2 echo "Available tests are : $available_options"
  >&2 echo '"test/run list" print all the available tests on standard output'
  >&2 echo '"test/run ALL" is a shortcut for "test/run $(test/run list)" and run all the tests'
}

#---------------------------------------------
# nothing and list override global comportement

if [[ "$@" == "" ]] ; then
  usage
  exit 0
fi

if [[ "$@" == "list" ]] ; then
  echo "$available_options"
  exit 0
fi

#---------------------------------------------
# Check parameters

invalid_option=false
invalid_test=""
at_least_one_test=false

for arg in $@ ; do
  case $arg in
    -*) # Option
      for ((i = 1; i < ${#arg}; i++)) ; do # For each parameter
        opt=${arg:$i:1}
        case $opt in
          l) ;;
          s) ;;
          v) ;;
          *) print_error "Unknown option '$opt'"
        esac
      done;;
    ALL) at_least_one_test=true;;
    *)
      found=false
      for avail in $available_options ; do
        if [[ $arg == $avail ]] ; then
          found=true
        fi
      done
      if [[ $found == false ]]; then
        invalid_test+=$arg
      else
        at_least_one_test=true
      fi;;
  esac
done

#---------------------------------------------
# Print error if necessary

if [[ $invalid_option == true || $invalid_test != "" || $at_least_one_test == false ]] ; then
  if [[ $invalid_test != "" ]]; then
    print_error "Unknown tests : $invalid_test"
  fi
  usage
  exit 1
fi

#---------------------------------------------
# Execute tests

export LOG_OUTPUT="/dev/null"

success=0
for arg in $@ ; do
  tests_to_execute=""
  case $arg in
    -*) # Option
      for ((i = 1; i < ${#arg}; i++)) ; do # For each parameter
        opt=${arg:$i:1}
        case $opt in
          l) export LOG_OUTPUT="$bin_dir/logs.txt";;
          v) export LOG_OUTPUT="/dev/stdout";;
          s) export LOG_OUTPUT="/dev/null";;
        esac
      done;;
    ALL) tests_to_execute="$available_options";;
    *) tests_to_execute="$arg";;
  esac
  for t in $tests_to_execute ; do
    print_info $t
    test/$t/run.sh "$src_dir" "$bin_dir/$t" "$bin_dir/install"
    if [[ $? == 0 ]] ; then
      print_ok "Test $t succeded"
    else
      print_error "Test $t failed"
      success=1
    fi
  done
done
asked_options="$@"

#---------------------------------------------

exit $success
