#!/bin/bash

src_dir=$1
bin_dir=$2
install_dir=$3
current_dir=$(realpath $(dirname $0))

source $src_dir/utils/misc.sh

forbiden_tokens="
                 file(GLOB_RECURSE
                 add_compile_options
                 add_definitions
                 include_directories
                 link_directories
                 link_libraries
                 set(C_FLAGS
                 file(GLOB
                 TODO
                 '--unresolved-symbols'
                 "

warning_token="
              CMAKE_CXX_FLAGS
              "

#-----------------------------------

for CMakeLists in $src_dir/CMakeLists.txt $(find $src_dir/src -name CMakeLists.txt) ; do
  error=false
  for token in $forbiden_tokens ; do
    res=$(grep -iown $token $CMakeLists)
    if [[ $res != "" ]] ; then
      if [[ $error == true ]] ; then
        print_simple $res "in" $CMakeLists
      else
        print_error $res "in" $CMakeLists
        error=true
      fi
    fi
  done
  warning=false
  for token in $warning_token ; do
    res=$(grep -iown $token $CMakeLists)
    if [[ $res != "" ]] ; then
      if [[ $warning == true ]] ; then
        print_simple $res "in" $CMakeLists
      else
        print_warning $res "in" $CMakeLists
        warning=true
      fi
    fi
  done
  if [[ $error == false && $warning == false ]] ; then
    print_ok $CMakeLists
  fi
done

