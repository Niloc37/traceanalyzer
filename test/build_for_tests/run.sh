#!/bin/bash

src_dir=$1
bin_dir=$2
install_dir=$3

source $src_dir/utils/misc.sh

mkdir -p $bin_dir && cd $bin_dir
cmake $src_dir -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX=$install_dir &> $LOG_OUTPUT
if [[ $? != 0 ]]; then
  print_error "Failed to execute cmake"
  exit 1
fi

make -j VERBOSE=1 &> $LOG_OUTPUT
if [[ $? != 0 ]]; then
  print_error "Failed to execute make"
  exit 1
fi

make install > $LOG_OUTPUT
if [[ $? != 0 ]]; then
  print_error "Failed to execute make install"
  exit 1
fi

