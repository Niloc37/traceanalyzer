#!/bin/bash

src_dir=$1
bin_dir=$2
install_dir=$3
current_dir=$(realpath $(dirname $0))

source $src_dir/utils/misc.sh

log_file=$bin_dir/logs.txt
rm -f $src_dir/$log_file

function _mkdir {
  if [[ -d $@ ]]; then
    rm -rf $@
  fi
  mkdir -p $@
}

_mkdir $bin_dir
touch $log_file

run_test=true
error_occured=false

test_name=""

function run_test {
  if [[ $run_test == true ]]; then
    # print_info "Run test : $@"
    # print_info "Run test : $@" >> $log_file 2>&1
    eval "$@" >> $log_file 2>&1
    if [[ $? == 0 ]]; then
      print_ok $test_name : $@
    else
      print_error $test_name $@
      cat $log_file
      run_test=false
      error_occured=true
    fi
  fi
}

function test_compilation {
  local build_dir=$1
  local config="$2"
  local params="$3"

  test_name="Compilation $(basename $build_dir)"

  mkdir -p $build_dir
  cd $build_dir
  # print_info "Test compilation \"$config\" with $params"
  run_test=true
  run_test "cmake $src_dir -DCMAKE_BUILD_TYPE=$config -G \"Unix Makefiles\" -DCMAKE_INSTALL_PREFIX=$build_dir/install $params"
  run_test make VERBOSE=1 -j
  run_test make install
  cd $current_dir
}

configs="Release RelWithDebInfo Debug"
for config in $configs; do
  test_compilation $bin_dir/${config} $config
done

if [[ $error_occured == true ]]; then
  exit -1
fi
exit 0
