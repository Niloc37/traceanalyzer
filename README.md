# TraceAnalyzer

## Quick start

### Building

* utils/init :
  - download or update external dependencies in "extlib" (using utils/updateDependencies)
  - Create three cmake target in "bin" folder :
    - Release : for release, usage checks and optimization
    - Debug : for debugging, more assert and verbosity, debugging symbols and no optimization
    - RelWithDebInfo : for developpers and tests, similar to Debug with optimization
  - install prefix is "install" subdirectory in project directory
* utils/build [target]
  - by default, use RelWithDebInfo as target, or the last target built
  - same effet as "cd bin/target && make -j && make install"

### Usage

* for usage information, use install/TraceAnalyzer -?
* utils/run [target]
  - build and run selected target (or last target built) with option -i (interractive mode)
  - In Debug and RelWithDebInformation, use gdb for quick debugging and add "-v debug" to options
